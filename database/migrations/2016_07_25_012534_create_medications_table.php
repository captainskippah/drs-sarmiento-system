<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medications', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('medicine');
            $table->integer('frequency');
            $table->integer('days');
            $table->string('instruction');
            $table->integer('prescription_id')->unsigned();

            $table->foreign('prescription_id')->references('id')->on('prescriptions');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medications');
    }
}
