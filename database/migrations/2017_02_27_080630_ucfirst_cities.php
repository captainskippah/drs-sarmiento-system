<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UcfirstCities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('UPDATE cities SET citymunDesc = LOWER(citymunDesc)');
        DB::unprepared(
            'UPDATE cities SET citymunDesc = CONCAT(UPPER(SUBSTR(citymunDesc,1,1)), LOWER(SUBSTR(citymunDesc,2)))
        ');

        DB::unprepared("
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' a',' A');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' b',' B');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' c',' C');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' d',' D');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' e',' E');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' f',' F');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' g',' G');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' h',' H');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' i',' I');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' j',' J');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' k',' K');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' l',' L');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' m',' M');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' n',' N');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' o',' O');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' p',' P');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' q',' Q');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' r',' R');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' s',' S');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' t',' T');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' u',' U');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' v',' V');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' w',' W');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' x',' X');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' y',' Y');
            UPDATE cities SET citymunDesc = REPLACE(citymunDesc,' z',' Z');
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
