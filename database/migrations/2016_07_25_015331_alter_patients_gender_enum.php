<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPatientsGenderEnum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->string('gender', 6)->change();
        });

        DB::table('patients')->where('gender', 'M')->update(['gender' => 'male']);
        DB::table('patients')->where('gender', 'F')->update(['gender' => 'female']);
        DB::statement( "ALTER TABLE `patients` MODIFY `gender` ENUM('male', 'female')" );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('patients')->where('gender', 'male')->update(['gender' => 'M']);
        DB::table('patients')->where('gender', 'female')->update(['gender' => 'F']);
        DB::statement( "ALTER TABLE `patients` MODIFY `gender` char(1)" );
    }
}
