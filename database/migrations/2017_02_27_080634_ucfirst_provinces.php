<?php

use App\Models\Province;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UcfirstProvinces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('UPDATE provinces SET provDesc = LOWER(provDesc);');
        DB::unprepared('UPDATE provinces SET provDesc = CONCAT(UPPER(SUBSTR(provDesc,1,1)), LOWER(SUBSTR(provDesc,2)));');
        DB::unprepared("
            UPDATE provinces SET provDesc = REPLACE(provDesc,' a',' A');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' b',' B');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' c',' C');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' d',' D');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' e',' E');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' f',' F');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' g',' G');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' h',' H');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' i',' I');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' j',' J');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' k',' K');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' l',' L');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' m',' M');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' n',' N');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' o',' O');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' p',' P');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' q',' Q');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' r',' R');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' s',' S');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' t',' T');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' u',' U');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' v',' V');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' w',' W');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' x',' X');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' y',' Y');
            UPDATE provinces SET provDesc = REPLACE(provDesc,' z',' Z');
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
