<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UcfirstBarangays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('UPDATE barangays SET brgyDesc = LOWER(brgyDesc)');
        
        DB::unprepared('
            UPDATE barangays SET brgyDesc = CONCAT(UPPER(SUBSTR(brgyDesc,1,1)), LOWER(SUBSTR(brgyDesc,2)))
        ');

        DB::unprepared("
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' a',' A');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' b',' B');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' c',' C');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' d',' D');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' e',' E');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' f',' F');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' g',' G');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' h',' H');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' i',' I');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' j',' J');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' k',' K');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' l',' L');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' m',' M');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' n',' N');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' o',' O');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' p',' P');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' q',' Q');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' r',' R');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' s',' S');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' t',' T');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' u',' U');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' v',' V');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' w',' W');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' x',' X');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' y',' Y');
            UPDATE barangays SET brgyDesc = REPLACE(brgyDesc,' z',' Z');
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
