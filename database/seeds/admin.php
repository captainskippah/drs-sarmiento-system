<?php

use Illuminate\Database\Seeder;

class admin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'firstname' => 'Lemuel',
            'lastname' => 'Flores',
            'username' => 'xxRockOnxx',
            'email' => 'is101.lemuel@gmail.com',
            'password' => bcrypt('secret'),
            'created_at' => time()
        ]);
    }
}
