<?php

use Illuminate\Database\Seeder;

class PhonePrefixesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('phone_prefixes')->insert([
        	['prefix' => '0905'],
			['prefix' => '0906'],
			['prefix' => '0907'],
			['prefix' => '0908'],
			['prefix' => '0909'],
			['prefix' => '0910'],
			['prefix' => '0912'],
			['prefix' => '0915'],
			['prefix' => '0916'],
			['prefix' => '0917'],
			['prefix' => '0918'],
			['prefix' => '0919'],
			['prefix' => '0920'],
			['prefix' => '0921'],
			['prefix' => '0922'],
			['prefix' => '0923'],
			['prefix' => '0924'],
			['prefix' => '0925'],
			['prefix' => '0926'],
			['prefix' => '0927'],
			['prefix' => '0928'],
			['prefix' => '0929'],
			['prefix' => '0930'],
			['prefix' => '0932'],
			['prefix' => '0933'],
			['prefix' => '0935'],
			['prefix' => '0936'],
			['prefix' => '0937'],
			['prefix' => '0938'],
			['prefix' => '0939'],
			['prefix' => '0942'],
			['prefix' => '0943'],
			['prefix' => '0946'],
			['prefix' => '0947'],
			['prefix' => '0948'],
			['prefix' => '0949'],
			['prefix' => '0950'],
			['prefix' => '0975'],
			['prefix' => '0977'],
			['prefix' => '0989'],
			['prefix' => '0996'],
			['prefix' => '0997'],
			['prefix' => '0998'],
			['prefix' => '0999']
        ]);
    }
}
