<?php

use App\User;
use Illuminate\Database\Seeder;

class DefaultAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'username' => 'finn',
        	'password' => bcrypt('secret'),
        	'firstname' => 'Sample',
        	'lastname' => 'Admin'
        ]);
    }
}
