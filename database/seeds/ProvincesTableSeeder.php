<?php

use Illuminate\Database\Seeder;

class ProvincesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        \DB::table('provinces')->delete();
        
        \DB::table('provinces')->insert(array (
            0 => 
            array (
                'provCode' => 128,
                'provDesc' => 'ILOCOS NORTE',
                'regCode' => 1,
            ),
            1 => 
            array (
                'provCode' => 129,
                'provDesc' => 'ILOCOS SUR',
                'regCode' => 1,
            ),
            2 => 
            array (
                'provCode' => 133,
                'provDesc' => 'LA UNION',
                'regCode' => 1,
            ),
            3 => 
            array (
                'provCode' => 155,
                'provDesc' => 'PANGASINAN',
                'regCode' => 1,
            ),
            4 => 
            array (
                'provCode' => 209,
                'provDesc' => 'BATANES',
                'regCode' => 2,
            ),
            5 => 
            array (
                'provCode' => 215,
                'provDesc' => 'CAGAYAN',
                'regCode' => 2,
            ),
            6 => 
            array (
                'provCode' => 231,
                'provDesc' => 'ISABELA',
                'regCode' => 2,
            ),
            7 => 
            array (
                'provCode' => 250,
                'provDesc' => 'NUEVA VIZCAYA',
                'regCode' => 2,
            ),
            8 => 
            array (
                'provCode' => 257,
                'provDesc' => 'QUIRINO',
                'regCode' => 2,
            ),
            9 => 
            array (
                'provCode' => 308,
                'provDesc' => 'BATAAN',
                'regCode' => 3,
            ),
            10 => 
            array (
                'provCode' => 314,
                'provDesc' => 'BULACAN',
                'regCode' => 3,
            ),
            11 => 
            array (
                'provCode' => 349,
                'provDesc' => 'NUEVA ECIJA',
                'regCode' => 3,
            ),
            12 => 
            array (
                'provCode' => 354,
                'provDesc' => 'PAMPANGA',
                'regCode' => 3,
            ),
            13 => 
            array (
                'provCode' => 369,
                'provDesc' => 'TARLAC',
                'regCode' => 3,
            ),
            14 => 
            array (
                'provCode' => 371,
                'provDesc' => 'ZAMBALES',
                'regCode' => 3,
            ),
            15 => 
            array (
                'provCode' => 377,
                'provDesc' => 'AURORA',
                'regCode' => 3,
            ),
            16 => 
            array (
                'provCode' => 410,
                'provDesc' => 'BATANGAS',
                'regCode' => 4,
            ),
            17 => 
            array (
                'provCode' => 421,
                'provDesc' => 'CAVITE',
                'regCode' => 4,
            ),
            18 => 
            array (
                'provCode' => 434,
                'provDesc' => 'LAGUNA',
                'regCode' => 4,
            ),
            19 => 
            array (
                'provCode' => 456,
                'provDesc' => 'QUEZON',
                'regCode' => 4,
            ),
            20 => 
            array (
                'provCode' => 458,
                'provDesc' => 'RIZAL',
                'regCode' => 4,
            ),
            21 => 
            array (
                'provCode' => 505,
                'provDesc' => 'ALBAY',
                'regCode' => 5,
            ),
            22 => 
            array (
                'provCode' => 516,
                'provDesc' => 'CAMARINES NORTE',
                'regCode' => 5,
            ),
            23 => 
            array (
                'provCode' => 517,
                'provDesc' => 'CAMARINES SUR',
                'regCode' => 5,
            ),
            24 => 
            array (
                'provCode' => 520,
                'provDesc' => 'CATANDUANES',
                'regCode' => 5,
            ),
            25 => 
            array (
                'provCode' => 541,
                'provDesc' => 'MASBATE',
                'regCode' => 5,
            ),
            26 => 
            array (
                'provCode' => 562,
                'provDesc' => 'SORSOGON',
                'regCode' => 5,
            ),
            27 => 
            array (
                'provCode' => 604,
                'provDesc' => 'AKLAN',
                'regCode' => 6,
            ),
            28 => 
            array (
                'provCode' => 606,
                'provDesc' => 'ANTIQUE',
                'regCode' => 6,
            ),
            29 => 
            array (
                'provCode' => 619,
                'provDesc' => 'CAPIZ',
                'regCode' => 6,
            ),
            30 => 
            array (
                'provCode' => 630,
                'provDesc' => 'ILOILO',
                'regCode' => 6,
            ),
            31 => 
            array (
                'provCode' => 645,
                'provDesc' => 'NEGROS OCCIDENTAL',
                'regCode' => 6,
            ),
            32 => 
            array (
                'provCode' => 679,
                'provDesc' => 'GUIMARAS',
                'regCode' => 6,
            ),
            33 => 
            array (
                'provCode' => 712,
                'provDesc' => 'BOHOL',
                'regCode' => 7,
            ),
            34 => 
            array (
                'provCode' => 722,
                'provDesc' => 'CEBU',
                'regCode' => 7,
            ),
            35 => 
            array (
                'provCode' => 746,
                'provDesc' => 'NEGROS ORIENTAL',
                'regCode' => 7,
            ),
            36 => 
            array (
                'provCode' => 761,
                'provDesc' => 'SIQUIJOR',
                'regCode' => 7,
            ),
            37 => 
            array (
                'provCode' => 826,
                'provDesc' => 'EASTERN SAMAR',
                'regCode' => 8,
            ),
            38 => 
            array (
                'provCode' => 837,
                'provDesc' => 'LEYTE',
                'regCode' => 8,
            ),
            39 => 
            array (
                'provCode' => 848,
                'provDesc' => 'NORTHERN SAMAR',
                'regCode' => 8,
            ),
            40 => 
            array (
                'provCode' => 860,
            'provDesc' => 'SAMAR (WESTERN SAMAR)',
                'regCode' => 8,
            ),
            41 => 
            array (
                'provCode' => 864,
                'provDesc' => 'SOUTHERN LEYTE',
                'regCode' => 8,
            ),
            42 => 
            array (
                'provCode' => 878,
                'provDesc' => 'BILIRAN',
                'regCode' => 8,
            ),
            43 => 
            array (
                'provCode' => 972,
                'provDesc' => 'ZAMBOANGA DEL NORTE',
                'regCode' => 9,
            ),
            44 => 
            array (
                'provCode' => 973,
                'provDesc' => 'ZAMBOANGA DEL SUR',
                'regCode' => 9,
            ),
            45 => 
            array (
                'provCode' => 983,
                'provDesc' => 'ZAMBOANGA SIBUGAY',
                'regCode' => 9,
            ),
            46 => 
            array (
                'provCode' => 997,
                'provDesc' => 'CITY OF ISABELA',
                'regCode' => 9,
            ),
            47 => 
            array (
                'provCode' => 1013,
                'provDesc' => 'BUKIDNON',
                'regCode' => 10,
            ),
            48 => 
            array (
                'provCode' => 1018,
                'provDesc' => 'CAMIGUIN',
                'regCode' => 10,
            ),
            49 => 
            array (
                'provCode' => 1035,
                'provDesc' => 'LANAO DEL NORTE',
                'regCode' => 10,
            ),
            50 => 
            array (
                'provCode' => 1042,
                'provDesc' => 'MISAMIS OCCIDENTAL',
                'regCode' => 10,
            ),
            51 => 
            array (
                'provCode' => 1043,
                'provDesc' => 'MISAMIS ORIENTAL',
                'regCode' => 10,
            ),
            52 => 
            array (
                'provCode' => 1123,
                'provDesc' => 'DAVAO DEL NORTE',
                'regCode' => 11,
            ),
            53 => 
            array (
                'provCode' => 1124,
                'provDesc' => 'DAVAO DEL SUR',
                'regCode' => 11,
            ),
            54 => 
            array (
                'provCode' => 1125,
                'provDesc' => 'DAVAO ORIENTAL',
                'regCode' => 11,
            ),
            55 => 
            array (
                'provCode' => 1182,
                'provDesc' => 'COMPOSTELA VALLEY',
                'regCode' => 11,
            ),
            56 => 
            array (
                'provCode' => 1186,
                'provDesc' => 'DAVAO OCCIDENTAL',
                'regCode' => 11,
            ),
            57 => 
            array (
                'provCode' => 1247,
            'provDesc' => 'COTABATO (NORTH COTABATO)',
                'regCode' => 12,
            ),
            58 => 
            array (
                'provCode' => 1263,
                'provDesc' => 'SOUTH COTABATO',
                'regCode' => 12,
            ),
            59 => 
            array (
                'provCode' => 1265,
                'provDesc' => 'SULTAN KUDARAT',
                'regCode' => 12,
            ),
            60 => 
            array (
                'provCode' => 1280,
                'provDesc' => 'SARANGANI',
                'regCode' => 12,
            ),
            61 => 
            array (
                'provCode' => 1298,
                'provDesc' => 'COTABATO CITY',
                'regCode' => 12,
            ),
            62 => 
            array (
                'provCode' => 1339,
                'provDesc' => 'CITY OF MANILA',
                'regCode' => 13,
            ),
            63 => 
            array (
                'provCode' => 1374,
                'provDesc' => 'NCR, SECOND DISTRICT',
                'regCode' => 13,
            ),
            64 => 
            array (
                'provCode' => 1375,
                'provDesc' => 'NCR, THIRD DISTRICT',
                'regCode' => 13,
            ),
            65 => 
            array (
                'provCode' => 1376,
                'provDesc' => 'NCR, FOURTH DISTRICT',
                'regCode' => 13,
            ),
            66 => 
            array (
                'provCode' => 1401,
                'provDesc' => 'ABRA',
                'regCode' => 14,
            ),
            67 => 
            array (
                'provCode' => 1411,
                'provDesc' => 'BENGUET',
                'regCode' => 14,
            ),
            68 => 
            array (
                'provCode' => 1427,
                'provDesc' => 'IFUGAO',
                'regCode' => 14,
            ),
            69 => 
            array (
                'provCode' => 1432,
                'provDesc' => 'KALINGA',
                'regCode' => 14,
            ),
            70 => 
            array (
                'provCode' => 1444,
                'provDesc' => 'MOUNTAIN PROVINCE',
                'regCode' => 14,
            ),
            71 => 
            array (
                'provCode' => 1481,
                'provDesc' => 'APAYAO',
                'regCode' => 14,
            ),
            72 => 
            array (
                'provCode' => 1507,
                'provDesc' => 'BASILAN',
                'regCode' => 15,
            ),
            73 => 
            array (
                'provCode' => 1536,
                'provDesc' => 'LANAO DEL SUR',
                'regCode' => 15,
            ),
            74 => 
            array (
                'provCode' => 1538,
                'provDesc' => 'MAGUINDANAO',
                'regCode' => 15,
            ),
            75 => 
            array (
                'provCode' => 1566,
                'provDesc' => 'SULU',
                'regCode' => 15,
            ),
            76 => 
            array (
                'provCode' => 1570,
                'provDesc' => 'TAWI-TAWI',
                'regCode' => 15,
            ),
            77 => 
            array (
                'provCode' => 1602,
                'provDesc' => 'AGUSAN DEL NORTE',
                'regCode' => 16,
            ),
            78 => 
            array (
                'provCode' => 1603,
                'provDesc' => 'AGUSAN DEL SUR',
                'regCode' => 16,
            ),
            79 => 
            array (
                'provCode' => 1667,
                'provDesc' => 'SURIGAO DEL NORTE',
                'regCode' => 16,
            ),
            80 => 
            array (
                'provCode' => 1668,
                'provDesc' => 'SURIGAO DEL SUR',
                'regCode' => 16,
            ),
            81 => 
            array (
                'provCode' => 1685,
                'provDesc' => 'DINAGAT ISLANDS',
                'regCode' => 16,
            ),
            82 => 
            array (
                'provCode' => 1740,
                'provDesc' => 'MARINDUQUE',
                'regCode' => 17,
            ),
            83 => 
            array (
                'provCode' => 1751,
                'provDesc' => 'OCCIDENTAL MINDORO',
                'regCode' => 17,
            ),
            84 => 
            array (
                'provCode' => 1752,
                'provDesc' => 'ORIENTAL MINDORO',
                'regCode' => 17,
            ),
            85 => 
            array (
                'provCode' => 1753,
                'provDesc' => 'PALAWAN',
                'regCode' => 17,
            ),
            86 => 
            array (
                'provCode' => 1759,
                'provDesc' => 'ROMBLON',
                'regCode' => 17,
            ),
        ));
        
        
    }
}
