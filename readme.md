# Online Record Management and Scheduling with Stock Monitoring System for Drs. Sarmiento Pharmacy and Skin Clinic

## Get Started

**Step 1:** Install composer dependencies

`composer install`

**Step 2:** Install npm dependencies

`npm install`

**Step 3:** Compile javascript assets

`npm run production`

**Step 4:** Configure your environment

Create .env file or copy .env.example and configure according to your needs.
Most likely you only need to configure the DB settings.

For the `TINIFY_APIKEY` please get yours here https://tinypng.com/developers

**Step 5:** Migrate tables

`php artisan migrate`

**Step 6:** Seed the default admin

`php artisan db:seed`

**Step 7:** Start laravel server

`php artisan serve`



# For more information please just see the laravel docs... seriously