;(function ( $, window, document, undefined ) {

	// Cache selectors
	var
		$tablePatients       = $('#table-patients'),
		$modalPatientView    = $('#modal-patient-view'),
		$modalPatientArchive = $('#modal-patient-archive'),
		$modalPatientRestore = $('#modal-patient-restore'),
		
		$btnPatientArchive  = $('#btn-patient-archive'),
		$btnPatientRestore  = $('#btn-patient-restore')

	var $dataTable = $tablePatients.DataTable({
		ajax: '/admin/patients/table',
		processing: true,
		serverSide: true,
		columns: [
			{
				data: 'id',
				name: 'id'
			},
			{
				data: 'firstname',
				name: 'firstname'
			},
			{
				data: 'middlename',
				name: 'middlename',
				render: function(data) {
					return !data ? '<i>Not Available</i>' : data
				}
			},
			{
				data: 'lastname',
				name: 'lastname'
			},
			{
				data: 'age',
				name: 'age'
			},
			{
				data: 'phone',
				name: 'phone',
				sortable: false
			},
			{
				data: 'id',
				sortable: false,
				searchable: false,
				render: function(data) {
					const view = '<a href="/admin/patients/' + data + '" class="item js-view-row"><i class="search icon"></i> View</a>';
                    const edit = '<a href="/admin/patients/' + data + '/edit" class="item"><i class="pencil icon"></i> Edit</a>';
                    const archive = '<div class="item js-archive-row" data-id="' + data + '"><i class="archive icon"></i> Archive</div>';
                    const restore = '<div class="item js-restore-row" data-id="' + data + '"><i class="upload icon"></i> Restore</div>';
                    
                    if ($tablePatients.data('mode') == 'active') {
                    	return '<div class="ui fluid selection dropdown"><div class="text">Actions</div><i class="dropdown icon"></i><div class="menu">' + view + edit + archive +'</div></div>';
                    } else {
                    	return '<div class="ui fluid selection dropdown"><div class="text">Actions</div><i class="dropdown icon"></i><div class="menu">' + view + edit + restore +'</div></div>';
                    }
				}
			}
		]
	});

	// Re-iniatilize dropdowns everytime table re-draw
	$dataTable.on('draw', function(){
		$tablePatients
			.find('.ui.dropdown')
			.dropdown({
				action: 'hide'
			});
	});

	// Close icon inside message
	$('.close', '.ui.message').on('click', function(event) {
		$(this)
			.closest('.ui.message')
			.slideUp(200)
	})

	// Table Mode Handler: Active and Archived
	$('.js-table-mode').on('click', function(event){
		if ($(this).data('mode') == 'archived') {
			$dataTable.ajax.url('/admin/patients/table?status=archived').load();
		} else {
			$dataTable.ajax.url('/admin/patients/table?status=active').load();
		}

		$(this)
			.prop('disabled', true)
				.siblings()
					.prop('disabled', false)

		console.log($(this).siblings())

		$tablePatients.data('mode', $(this).data('mode'))
	})

	$tablePatients.on('click', '.js-view-row', function(event) {
		event.preventDefault()
		const data = $dataTable.row($(this).closest('tr')).data()

		$modalPatientView
			.find('.js-prescriptions')
				.prop('href', '/admin/prescriptions/?patient=' + data.id)
				.end()
			.find('.js-appointments')
				.prop('href', '/admin/appointments/?patient=' + data.id)
				.end()
			.find('.js-name')
				.html(data.firstname + ' ' + data.middlename + ' ' + data.lastname)
				.end()
			.find('.js-age')
				.html(data.age)
				.end()
			.find('.js-phone')
				.html(data.phone)
				.end()
			.find('.js-address')
				.html(
					data.address_2 + ' ' +data.address_1 + '<br>' +
					data.brgyDesc + ', ' + data.citymunDesc + '<br>' +
					data.provDesc
				)
				.end()
			.find('.js-edit')
				.prop('href', '/admin/patients/' + data.id + '/edit')

		$modalPatientView.modal('show')
	});

	// Archive Prompt
	$tablePatients.on('click', '.js-archive-row', function(event){
		$btnPatientArchive
			.data('id', $(this).data('id'))
			.removeProp('disabled')
			.removeClass('loading');

		$modalPatientArchive
			.modal('show');
	});

	// Send Archive Request
	$btnPatientArchive.on('click', function(event) {
		$(this)
			.prop('disabled', true)
			.addClass('loading')

		$.ajax({
			url: '/admin/patients/' + $(this).data('id') + '/archive',
			type: 'POST',
			data: {'_method': 'DELETE'},
		})
		.done(function() {
			$modalPatientArchive.modal('hide')
			$dataTable.ajax.reload()
		})
	})

	// Restore prompt
	$tablePatients.on('click', '.js-restore-row', function(event){
		$btnPatientRestore
			.data('id', $(this).data('id'))
			.removeProp('disabled')
			.removeClass('loading');

		$modalPatientRestore.modal('show');
	});

	// Send Restore Request
	$btnPatientRestore.on('click', function(event) {
		$(this)
			.prop('disabled', true)
			.addClass('loading')

		$.ajax({
			url: '/admin/patients/' + $(this).data('id') + '/restore',
			type: 'POST',
			data: {'_method': 'PUT'}
		})
		.done(function() {
			$modalPatientRestore.modal('hide')
			$dataTable.ajax.reload()
		})
	})

})( jQuery, window, document );