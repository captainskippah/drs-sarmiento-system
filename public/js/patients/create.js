(function ($) {
	$('#dropdown-prefix')
		.dropdown()

	$('#btn-genders').on('focus change', 'input', function(event) {
		$(this)
			.closest('label')
				.addClass('active')
			.siblings('label')
				.removeClass('active')
	})

	// Will only fire when tab is pressed and no selection was made
	$('#btn-genders').on('blur', 'input', function(event) {
		if (!$(this).is(':checked')) {
			$(this)
				.closest('label')
					.removeClass('active')
		}
	})

	var Location = {
		init: function() {
			this.$province = $('#dropdown-province')
			this.$city     = $('#dropdown-city')
			this.$barangay = $('#dropdown-barangay')

			this.$province.dropdown()
			this.$city.dropdown()
			this.$barangay.dropdown()
			
			this.loadDefaults()
		},
		loadDefaults: function() {
			const self     = this
			const provCode = this.$province.find('.value').val()
			const cityCode = this.$city.find('.value').val()
			const brgyCode = this.$barangay.find('.value').val()

			this.fetchProvinces().done(function(){
				self.$province.dropdown('set selected', provCode)

				self.fetchCities(provCode).done(function(){
					self.$city.dropdown('set selected', cityCode)

					self.fetchBarangays(cityCode).done(function(){
						self.$barangay.dropdown('set selected', brgyCode)

						// All dropdown is set. Bind events now
						self.bindEvents()
					})
				})
			})
		},
		bindEvents: function() {
			this.$province
				.dropdown('setting', 'onChange', this.onProvinceChange.bind(this))

			this.$city
				.dropdown('setting', 'onChange', this.onCityChange.bind(this))

			this.$barangay
				.dropdown('setting', 'onChange', this.onBarangayChange.bind(this))
		},

		/* Dropdown Change Handlers */

		onProvinceChange: function(provCode) {
			// Will get enabled after city finish fetching
			this.disableDropdown(this.$city)

			// Will get enabled when a city has been selected
			this.disableDropdown(this.$barangay)

			// Fetch cities for this province
			this.fetchCities()
		},
		onCityChange: function(citymunCode) {
			// Will get enabled after barangays finish fetching
			this.disableDropdown(this.$barangay)

			if (citymunCode) {
				this.fetchBarangays()
			}
		},
		onBarangayChange: function(brgyCode) {
			if (!brgyCode) {
				this.disableDropdown(this.$barangay)
			}
			this.$barangay.find('[name="address[barangay]"]').valid()
		},

		/* Dropdown Fetching Methods */

		fetchProvinces: function() {
			const self = this

			this.disableDropdown(this.$province)
			this.loadingDropdown(this.$province)

			return $.ajax({
				url: '/api/v1/provinces'
			})
			.done(function(provinces) {
				var items = ''
				for (province of provinces) {
					items += '<div class="item" data-value="' + province.provCode + '">' + province.provDesc + '</div>'
				}
				self.updateDropdown(self.$province, items)
				self.enableDropdown(self.$province)
			})
		},
		fetchCities: function() {
			const self     = this
			const provCode = this.$province.find('.value').val()

			this.disableDropdown(this.$city)
			this.loadingDropdown(this.$city)

			return $.ajax({
				url: '/api/v1/provinces/' + provCode + '/cities'
			})
			.done(function(cities) {
				var items = ''
				for (city of cities) {
					items += '<div class="item" data-value="' + city.citymunCode + '">' + city.citymunDesc + '</div>'
				}
				self.updateDropdown(self.$city, items)
				self.enableDropdown(self.$city)
			})
		},
		fetchBarangays: function(citymunCode) {
			const self     = this
			const provCode = this.$province.find('.value').val()
			const cityCode = this.$city.find('.value').val()

			this.disableDropdown(this.$barangay)
			this.loadingDropdown(this.$barangay)

			return $.ajax({
				url: '/api/v1/provinces/' + provCode + '/cities/' + cityCode + '/barangays'
			})
			.done(function(barangays) {
				var items = ''
				for (barangay of barangays) {
					items += '<div class="item" data-value="' + barangay.brgyCode + '">' + barangay.brgyDesc + '</div>'
				}
				self.updateDropdown(self.$barangay, items)
				self.enableDropdown(self.$barangay)
			})
		},

		/* Dropdown Helper Methods */
		updateDropdown: function(element, items) {
			element
				.find('.menu')
				.html(items)

			element
				.dropdown('refresh')
		},
		loadingDropdown: function(element) {
			element
				.addClass('loading')
		},
		disableDropdown: function(element) {
			element
				.addClass('disabled')

			// Alternative for: .dropdown('clear') without trigger onChange
			// Reason: barangay's .valid() will fire even when submit is not fired yet
			element
				.dropdown('set text', '')
				.find('.value')
					.val('')
		},
		enableDropdown: function(element) {
			element
				.removeClass('disabled loading')
		}
	}

	Location.init()

	$('#form-patient').validate({
		ignore: '',
		rules: {
			'personal[firstname]': {
				required: true
			},
			'personal[lastname]': {
				required: true
			},
			'personal[gender]': {
				required: true
			},
			'personal[age]': {
				required: true
			},
			'personal[phone]': {
				required: true
			},
			'address[line1]': 'required',
			'address[barangay]': 'required'
		}
	})

	$('#form-patient').on('submit', function() {
		if (this.valid()) {
			$(this)
				.addClass('loading')
				.find(':submit').
					prop('disabled', true)
		}
	})
}(jQuery));