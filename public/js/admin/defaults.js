jQuery(function($){
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json'
    })

    $.validator.setDefaults({
        errorClass: 'error',
        errorElement: 'span',
        errorPlacement: function (error, element) {
            $(element)
                .closest('.field')
                    .append($(error).addClass('message'))
        },
        highlight: function(element, errorClass) {
            $(element)
                .closest('.field')
                    .addClass(errorClass)
        },
        unhighlight: function(element, errorClass) {
            $(element)
                .closest('.field')
                    .removeClass(errorClass)
        }
    })

    $.validator.addMethod('price', function(value, element, params){
        return this.optional(element) || /^$|^([1-9][0-9]{,2}(,[0-9]{3})*|[0-9]+)(\.[0-9]{2,2})?$/.test(value)
    }, 'Please enter valid price')

    $.fn.dropdown.settings.forceSelection = false

    $('#dropdownProfile').dropdown();

    // Generalize .ui.message close handler.
    // In case there will be more .ui.message in page
    // 'slide down' will do the opposite when closing
    $('#page-wrapper').on('click', '.ui.message .close.icon', function(e) {
        $(this).closest('.ui.message').transition('fade')
    })
})