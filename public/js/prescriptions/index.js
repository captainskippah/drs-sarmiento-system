jQuery(function($){
	$('#table-prescriptions').DataTable({
		ajax: '/api/v1/admin/prescriptions/table',
		processing: true,
		serverSide: true,
		columns:[
			{
				data: 'patient.name',
				name: 'patient.name'
			},
			{
				data: 'case',
				name: 'case'
			},
			{
				data: 'treatment.name',
				name: 'treatment.name',
				defaultContent: '<i>None</i>'
			},
			{
				data: 'created_at',
				name: 'created_at'
			},
			{
				data: 'action',
				sortable: false,
				orderable: false,
				searchable: false
			}
		]
	});
})