jQuery(function($){

	var onMedicationChange = {
		get: function(target, property) {
			return target[property]
		},
		set: function(target, property, value, receiver) {
			target[property] = value
			$('#table-medication').trigger('update')
			return true
		}
	}

	var medications = new Proxy([], onMedicationChange)

	var
		$formPrescription = $('#form-prescription'),
		$formMedication   = $('#form-medication'),
		$tableMedication  = $('#table-medication'),
		$modalMedication  = $('#modal-medication')

	const validatorPrescription = $formPrescription.validate({
		ignore: '',
		rules: {
			patient: 'required',
			case: 'required'
		}
	})

	const validatorMedication = $formMedication.validate({
		rules: {
			name: 'required',
			frequency: {
				required: true,
				digits: true
			},
			days: {
				required: true,
				digits: true
			},
			instructions: 'required'
		}
	})

	$('#dropdown-patient').dropdown()
	$('#dropdown-treatment').dropdown()

	$modalMedication.modal({
		onHidden: function() {
			$formMedication.trigger('reset')
		}
	})

	$formPrescription.on('submit', function(e) {
		e.preventDefault()
		
		if ($(this).valid() && validateMedication()) {

		}
	})

	$formMedication.on('submit', function(e) {
		e.preventDefault()

		if ($(this).valid()) {

			medications.push({
				name: $(this).find('[name="name"]').val(),
				frequency: $(this).find('[name="frequency"]').val(),
				days: $(this).find('[name="days"]').val(),
				instructions: $(this).find('[name="instructions"]').val()
			})

			$modalMedication.modal('hide')
		}
	})

	$formMedication.on('reset', function(e) {
		validatorMedication.resetForm()
		$(this).find('.field.error').removeClass('error')
	})

	$tableMedication.on('update', function(e){
		if (medications.length > 0) {
			$('.js-empty-medication').hide()

			let rows = ''

			medications.forEach(function(medication, index) {
				let columns = [
					'<td>' + medication['name'] + '<input type="hidden" name="medicine[][name]" value="' + medication['name'] +'"></td>',
					'<td>' + medication['frequency'] + '<input type="hidden" name="medicine[][frequency]" value="' + medication['frequency'] +'"></td>',
					'<td>' + medication['days'] + '<input type="hidden" name="medicine[][days]" value="' + medication['days'] +'"></td>',
					'<td>' + medication['instructions'] + '<input type="hidden" name="medicine[][instructions]" value="' + medication['instructions'] +'"></td>',
					'<td><button type="button" class="ui red button js-delete-medication" data-id="' + index + '">Delete</button></td>'
				].join('')

				rows += '<tr>' + columns + '</tr>'
			})

			$(this).find('tbody').html(rows)

		} else {
			$(this).find('tbody').html('<tr><td colspan="5" style="text-align: center;">No Medications Available</td></tr>')
		}
	})

	$tableMedication.on('click', '.js-delete-medication', function(e) {
		medications.splice($(this).data('id'), 1)
	})

	$('.js-modal-close').on('click', function(e) {
		$modalMedication.modal('hide')
	})

	$('.js-modal-submit').on('click', function(e) {
		$formMedication.submit()
	})

	$('#btn-add-medication').on('click', function(e) {
		$modalMedication.modal('show')
	})

	// Generalize .ui.message close handler.
	// In case there will be more .ui.message in page
	// 'slide down' will do the opposite when closing
	$('.ui.message').on('click', '.close.icon', function(e) {
		$(this).closest('.ui.message').transition('slide down')
	})

	function validateMedication() {
		if (medications.length > 0) {
			return true
		} else {
			$('#error-medication').transition('shake')
			return false
		}
	}
})