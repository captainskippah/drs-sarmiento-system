;jQuery(function($){

	var
		$formTreatment = $('#form-treatment')

	var validator = $formTreatment.validate({
		rules: {
			name: 'required',
			price: {
				required: true,
				price: true
			}
		}
	})

	$formTreatment.on('submit', function(event){
		if ($(this).valid()) {
			$(this)
				.addClass('loading')
				.find(':submit')
					.prop('disabled', true)
		}
	})
})