;jQuery(function($){
	var
		$tableTreatments = $('#table-treatments'),
		$message = Handlebars.compile($('#message').html()),
		$modalTreatmentDelete = Handlebars.compile($('#modal-treatment-delete').html())

	var methods = {
		sendDelete: function(treatmentID) {
			return $.ajax({
				url: '/admin/treatments/' + treatmentID,
				type: 'DELETE'
			})
		}
	}

	var $dataTable = $tableTreatments.DataTable({
		ajax: '/admin/treatments/table',
		serverSide: true,
		processing: true,
		columns: [
			{ data: 'name' },
			{ data: 'price', className: 'right aligned' },
			{
				data: 'id',
				searchable: false,
				sortable: false,
				render: function(data){
                    const edit = '<a href="/admin/treatments/' + data + '/edit" class="item"><i class="pencil icon"></i> Edit</a>';
                    const del = '<div class="item js-delete-row" data-id="' + data + '"><i class="trash icon"></i> Delete</div>';
                    
                	return '\
                	<div class="ui fluid selection dropdown">\
                		<div class="text">Actions</div>\
                		<i class="dropdown icon"></i>\
                		<div class="menu">' + edit + del +'</div>\
                	</div>';
				}
			}
		]
	})

	// Re-iniatilize dropdowns everytime table re-draw
	$dataTable.on('draw', function(){
		$tableTreatments
			.find('.ui.dropdown')
			.dropdown({
				action: 'hide'
			})
	})


	$tableTreatments.on('click', '.js-delete-row', function(event){
		const row  = $(this).closest('tr')
		const data = $dataTable.row(row).data()

		$( $modalTreatmentDelete({ name: data.name }) )
		.modal({
			onApprove: function() {
				methods.sendDelete(data.id)
				.done(function(response){
					$dataTable.ajax.reload(function() {
						$('#page-message')
							.html($message({
								type: 'success',
								header: 'Success',
								message: '<b>' + data.name + '</b> is deleted'
							}))
								.find('.ui.message')
									.transition('fade')
					})
				})
			}
		})
		.modal('show')
	})
})