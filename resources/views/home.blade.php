@extends('layouts.default')

@push('custom-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.6/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.6/assets/owl.theme.default.min.css">
@endpush

@section('content')

	<div id="home">
		<div class="owl-carousel" id="slider">
			<div class="item">
				<img src="/img/public/carousel1.png">
			</div>
			<div class="item">
				<img src="/img/public/carousel2.png">
			</div>
			<div class="item">
				<img src="/img/public/carousel3.png">
			</div>
		</div>
	</div>

	<div id="main-container">
		<section id="segment-products">
			<header>
				<h1 class="ui horizontal divider header">Browse Our Products</h1>
			</header>
			<div class="ui grid container">
				<div class="row">
					<div class="three wide column" id="column-menu-products">
						<div class="ui small header">Product Categories</div>
						<div class="ui secondary vertical fluid pointing menu" id="menu-products">
							<?php $first = true; ?>
							@foreach($categories as $category)

								@if($category->products()->count() !== 0)
									<a
										href="#"
										class="{{ $first ? 'active item' : 'item' }}"
										data-content="{{ $category->id }}"
									>
										<span>{{ $category->name }}</span>
										<small>{{ $category->parent ? '('.$category->parent->name.')' : '' }}</small>
									</a>

									<?php
										if($first){
											$first = false;
										}
									?>
								@endif

							@endforeach
						</div>		
					</div>
					<div class="thirteen wide column" id="column-products">
						<?php $first = true; ?>
						@foreach($categories as $category)
							<div
								class="ui four product cards {{ $first ? 'active transition visible' : 'transition hidden' }}"
								data-content="{{ $category->id }}"
								@if($first) style="display: flex !important;" @endif
							>

								@foreach($products as $product)

									@if($product->category->id == $category->id)
									
										<div class="ui card">
											<a href="#" class="image"><img src="/img/products/{{ $product->image }}"></a>
											<div class="content">
												<a href="#" class="header">{{ $product->name }}</a>
												<div class="description">
													@if($product->details)
														<p>{{ $product->details }}</p>
													@else
														<i>None</i>
													@endif
												</div>
											</div>
										</div>

									@endif

								@endforeach

								<?php $first = false; ?>
							</div>
						@endforeach
					</div>
				</div>
			</div>
		</section> {{-- end of products segment --}}

		<section id="segment-facilities">
			<header>
				<h1 class="ui horizontal divider header">Our Clinic</h1>
			</header>

			<div class="ui three column grid container">
				<div class="column">
					<img class="ui fluid image" src="/img/public/facilities1.jpg">
				</div>
				<div class="column">
					<img class="ui fluid image" src="/img/public/facilities2.jpg">
				</div>
				<div class="column">
					<img class="ui fluid image" src="/img/public/facilities3.jpg">
				</div>
			</div>
		</section>

		<section id="segment-aboutus">
			<header>
				<h1 class="ui horizontal divider header">About Us</h1>
			</header>

			<div class="ui two column grid container">
				<div class="column">
					<img class="ui fluid image" src="/img/public/front.jpg">
				</div>
				<div class="column" id="column-aboutus">
					<div>
						<h2 class="ui header">Mission</h2>
						<ul>
							<li>Provide world-class dermatological services and care for our patients</li>
							<li>Sustain and develop the Clinic.</li>
						</ul>
					</div>

					<div>
						<h2 class="ui header">Vision</h2>
						<p>A centre of excellence in skin health and a home for dermatology.</p>
					</div>

					<div>
						<h2 class="ui header">History</h2>
						<p>
							The Drs. Sarmiento Pharmacy and Skin Clinic was built on March 30, 1990 and located at 466 Rizal Street, Angeles City. Mr. Luciano Sarmiento and Mrs. Remedios Sarmiento are the owner of the clinic. In 1990s, the skin clinic only offers consultation, checkup for skin diseases, medicines and skin products. Few years after, they added new services like facials, diamond peel, skin peeling or whitening, pimple or acne treatment, foot and body scrub, warts removal, underarm whitening, Botox for wrinkle and fat dissolve or Mesotherapy.
						</p>
					</div>
				</div>
			</div>
		</section>
	</div>

@stop

@section('js-plugins')

{{-- The Component's JS files --}}
@stack('modules')
<script type="text/javascript" src="https://cdn.rawgit.com/thesmart/jquery-scrollspy/master/scrollspy.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/kswedberg/jquery-smooth-scroll/master/jquery.smooth-scroll.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.6/owl.carousel.min.js"></script>
<script type="text/javascript">
	
	// Cache common selectors
	var 
		$navMenu          = $('#nav'),
		$slider           = $('#slider'),
		$sectionContainer = $('#main-container'),
		$productMenu      = $('#menu-products'),
		$products         = $('#column-products').find('.ui.product.cards'),
		$btnGender        = $('#btn-gender'),
		$btnPatientForm   = $('#btn-patient-form');

	$slider.owlCarousel({
		loop: true,
		items: 1,
		autoplay: true,
	});

	$navMenu
		.find('.item')
		.smoothScroll();

	$sectionContainer
		.children('section')
		.add('#home')
		.each(function(index, element) {

			let sectionID = $(element).attr('id');
			let $navItem   = $navMenu.find('.item[href="#'+sectionID+'"]');

			$(element)
				.visibility({
					once: false,
					onTopPassed: function(){
						$navItem.addClass('active')
					},
					onBottomPassedReverse: function(){
						$navItem.addClass('active')
					},
					onBottomPassed: function(){
						$navItem.removeClass('active')
					},
					onTopPassedReverse: function(){
						$navItem.removeClass('active')
					}
				});
		});

	$productMenu.on('click', '.item[data-content]', function(event) {

		event.preventDefault();

		var
			menuID = $(this).data('content'),
			cardID = $products.filter('.active').data('content');

		if(menuID == cardID){
			return false;
		}

		// Add active class on selected .item ( in productMenu )
		$(this)
			.addClass('active')
				.siblings('.item.active')
				.removeClass('active');

		// Hide current active cards.
		$products
			.filter('.active')
			.removeClass('active')
			.transition({
				animation: 'fade right',
				duration: '0.15s',
				onComplete: function(){
					// Show new active cards after hiding the previous one.
					$products
						.filter(function(){
							return $(this).data('content') == menuID
						})
						.addClass('active')
						.transition({
							animation: 'fade right',
							duration: '0.15s'
						});
				}
			});	
	});

</script>
@stop