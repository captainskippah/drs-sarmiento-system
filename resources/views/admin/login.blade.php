@section('title')
Clinic - Login
@stop

@push('css-plugins')
<style type="text/css">
    body {
      background-color: #DADADA;
      margin-top: 0;
    }
    body > .grid {
      height: 100%;
    }
    .column {
      max-width: 450px;
    }
      .field {
        text-align: left;
      }

     .field.error .message {
        color: #9F3A38;
        display: inline-block;
     }  
</style>
@endpush

@include('includes.admin.head')

<div class="ui middle aligned center aligned grid">
    <div class="column">
        <h2 class="ui teal header">
            <div class="content">
                Log-in to your account
            </div>
        </h2>
        <form action="{{ url('/admin/login') }}" role="form" method="POST" class="ui large form">
            {{ csrf_field() }}
            <div class="ui stacked segment">
                <div class="field">
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="text" name="username" placeholder="Username" autofocus>
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input type="password" name="password" placeholder="Password">
                    </div>
                </div>
                <button type="submit" class="ui fluid large teal submit button">Login</button>
            </div>

            @if (count($errors) > 0)
            <div class="ui error message">
                <ul class="list">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

        </form>
      </div>
</div>

@section('js-plugins')
<script type="text/javascript">
    $('.ui.form')
      .validate({
        rules: {
          'username': 'required',
          'password': 'required'
        }
      })
</script>
@stop

@include('includes.admin.footer')