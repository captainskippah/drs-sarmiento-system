@extends('layouts.admin.default')

@section('page-header')
Dashboard
@stop

@section('content')
<div class="ui message">
    <div class="header">Welcome {{ auth()->user()->firstname.' '.auth()->user()->lastname }}!</div>
</div>

@stop

@section('js-plugins')

@stack('modules')
@stop