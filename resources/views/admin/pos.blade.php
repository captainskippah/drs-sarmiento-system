<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta id="_token" name="csrf-token" content="{{ csrf_token() }}">
        <meta name="author" content="Lemuel Flores">
        <title>POS - Clinic</title>

        <!-- Semantic UI -->
        <link href="/semantic/dist/semantic.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="/font-awesome/css/font-awesome.min.css" />
        <!-- POS style -->
        <link rel="stylesheet" type="text/css" href="/css/admin/pos.css">
    </head>

    <body class="ui dimmable">

        <div class="ui dimmer">
            <div class="ui large indeterminate text loader">Loading POS System. Please wait...</div>
        </div>

        <nav class="ui huge top fixed menu" id="nav-main">
            <a href="#" class="header item">Drs. Sarmiento POS</a>

            <a class="item" href="{{ url('admin/dashboard') }}">
                <i class="fa fa-fw fa-dashboard"></i> Dashboard
            </a>
            <a class="item" href="{{ url('admin/inventory') }}">
                <i class="fa fa-fw fa-barcode"></i> Inventory
            </a>
            <a class="item" href="{{ url('admin/brands') }}">
                <i class="fa fa-fw fa-th-list"></i> Brands
            </a>
            <a class="item" href="{{ url('admin/categories') }}">
                <i class="fa fa-fw fa-folder"></i> Categories
            </a>

            <div class="right menu">
                <div class="ui dropdown item" id="dropdownProfile">
                    <i class="user icon"></i> Finn The Human <i class="dropdown icon"></i>
                    <div class="inverted menu">
                        <a href="#" class="item"><i class="user icon"></i> Profile</a>
                        <a href="#" class="item"><i class="mail icon"></i> Inbox</a>
                        <a href="#" class="item"><i class="setting icon"></i> Settings</a>
                        <a href="#" class="item"><i class="power icon"></i> Logout</a>
                    </div>
                </div>
            </div>
        </nav>

        <main id="app">
            <app></app>
        </main>
        
        <!-- jQuery -->
        <script src="/js/jquery.min.js"></script>

        <!-- Semantic UI Core Javascript -->
        <script type="text/javascript" src="/semantic/dist/semantic.min.js"></script>

        <script type="text/javascript" src="/js/pos.js"></script>

        <script type="text/javascript">

        /*
        *   jQuery stuffs. Don't touch
        */
        $('#dropdownProfile').dropdown();

        </script>
    </body>

</html>