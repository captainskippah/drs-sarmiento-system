@extends('layouts.admin.default')

@push('css-plugins')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.semanticui.min.css" integrity="sha256-xApYlmdj+dPTG+P7NRNeaxVKwhSuTsa4xQlTKo3V9Vw="crossorigin="anonymous">
@endpush

@section('page-header')
Brands
@stop

@section('content-top')
<div class="row">
    <div class="four wide column">
        <button type="button" class="ui green button" id="btn-new-brand"><i class="plus icon"></i> Add New Brand</button>
    </div>
</div>
@stop

@section('content')

<div class="ui small modal" id="modal-brand-form">
	<div class="header">
		New Brand
	</div>

	<div class="content">
		<form class="ui form" id="form-brand">
			<div class="field">
				<label for="input-name">Brand Name</label>
				<div class="ui input">
					<input type="text" id="input-name" name="name">
					{{ method_field('POST') }}
				</div>
			</div>
		</form>
	</div>

	<div class="actions">
		<button type="button" class="ui red cancel button">
			Cancel
		</button>
		<button type="submit" class="ui green ok button" form="form-brand">
			<i class="checkmark icon"></i> Save
		</button>
	</div>
</div><!-- end of modal-brand-form -->

<div class="ui small modal" id="modal-brand-delete">
	<div class="header">
		Delete Brand?
	</div>

	<div class="content">
		Delete this brand?
	</div>

	<form id="form-brand-delete"></form>

	<div class="actions">
		<button type="button" class="ui red cancel button">
			Cancel
		</button>
		<button type="submit" class="ui green ok button">
			<i class="checkmark icon"></i> Yes
		</button>
	</div>
</div>

<table class="ui selectable sortable table" id="table-brands">
	<thead>
		<tr>
			<th>Brand Name</th>
			<th class="four wide column">Actions</th>
		</tr>
	</thead>
</table>

@stop

@push('js-plugins')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.semanticui.min.js"></script>
<script type="text/javascript" src="/js/jquery.form.min.js"></script>

<script type="text/javascript">
$(document).ready(function($) {

	// Cache commonly used selectors
	var
		$btnNewBrand      = $('#btn-new-brand'),
		
		$modalBrandForm   = $('#modal-brand-form'),
		$modalBrandDelete = $('#modal-brand-delete'),

		$formBrand        = $('#form-brand'),
		$formBrandDelete  = $('#form-brand-delete'),

		$tableBrands      = $('#table-brands');

	var $dataTable = $tableBrands.DataTable({
		ajax: '/admin/brands/table',
		serverSide: true,
		processing: true,
		columns: [
			{
				data: 'name',
				name: 'name'
			},
			{
				data:'action',
				searchable: false,
				orderable: false,
				sortable: false
			}

		]
	});

	const validator = $formBrand.validate()

	$('#input-name').rules('add', {
		required: true
	})

	/***************************************************/

	/*
	*	Open New Brand Form
	*/
	$btnNewBrand.on('click', function(){
		$formBrand[0].reset()

		$formBrand
			.prop('action', '/admin/brands');

		$formBrand
			.find('[name="_method"]')
				.val('POST')

		$modalBrandForm
			.find('.header')
			.text('New Brand');

		$modalBrandForm.modal('show');
	});

	/*
	*	Open Brand Editing Form
	*/
	$tableBrands.on('click', 'button[data-action="edit brand"]', function(){
		let brandData = $dataTable.row($(this).closest('tr')).data();

		$formBrand
			.prop('action', '/admin/brands/'+brandData.id)

		$formBrand[0].reset()

		$formBrand
			.find('[name="name"]')
				.val(brandData.name)
				.end()
			.find('[name="_method"]')
				.val('PUT')

		$modalBrandForm
			.find('.header')
			.text('Edit Brand');

		$modalBrandForm.modal('show');
	})

	/*
	*	Open Brand Deleting Modal Confirmation
	*/
	$tableBrands.on('click', 'button[data-action="delete brand"]', function(){
		let brandData = $dataTable.row($(this).closest('tr')).data();

		$formBrandDelete
			.prop('action', '/admin/brands/'+brandData.id);

		$modalBrandDelete
			.find('.content')
			.text('Delete "'+brandData.name+'" ?');

		$modalBrandDelete.modal('show');
	});

	/*************************************************/

	function submitBrand(){
		if($formBrand.valid()){
			disableForm();

			submitForm({
				url    : $formBrand.prop('action'),
				data   : $formBrand.serializeArray()
			})
			.fail(enableForm)
			.done(function(){
				$modalBrandForm.modal('hide');

				successNoty(
					$formBrand.form('get value', '_method') == 'POST' ?
				 	'New brand was added' :
				 	'Brand information was saved'
				 );
				
				$dataTable.ajax.reload();
			});
		}

		return false;
	}

	function enableForm(){
		$modalBrandForm
			.find('.ok')
				.removeClass('loading')
				.removeProp('disabled');

		$formBrand
			.find('.input')
			.removeClass('disabled');
	}

	function disableForm(){
		$modalBrandForm
			.find('.ok')
				.addClass('loading')
				.prop('disabled', true);

		$formBrand
			.find('.input')
			.addClass('disabled');
	}

	$modalBrandForm.modal({
		onShow: enableForm,
		onApprove: () => false, // avoid closing the modal
		onDeny: () => $.noty.closeAll()
	});

	$formBrand.on('submit', submitBrand);

	$formBrand.on('reset', function(event) {
		validator.resetForm()

		$(this)
			.find('.error')
				.removeClass('error')
	})

	/****************************************/

	function enableFormDelete(){
		$modalBrandDelete
			.find('.ok')
				.removeClass('loading')
				.removeProp('disabled');
	}

	function deleteBrand(){

		$modalBrandDelete
			.find('.ok')
				.addClass('loading')
				.prop('disabled', true);

		submitForm({ url: $formBrandDelete.prop('action') }, 'DELETE')
			.fail(enableFormDelete)
			.done(function(){
				$modalBrandDelete.modal('hide');
				successNoty('Successfully deleted brand');
				$dataTable.ajax.reload();
			});

		return false;
	}

	$modalBrandDelete.modal({
		onShow: enableFormDelete,
		onApprove: deleteBrand
	});
});
</script>
@endpush