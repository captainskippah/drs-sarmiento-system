@extends('layouts.admin.default')

@push('css-plugins')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.semanticui.min.css" integrity="sha256-xApYlmdj+dPTG+P7NRNeaxVKwhSuTsa4xQlTKo3V9Vw="crossorigin="anonymous">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush

@section('page-header', 'Prescriptions')

@section('content-top')
    <a href="/admin/prescriptions/create" class="ui green button">
    	<i class="fa fw fa-pencil-square-o"></i> New Prescription
    </a>
@stop

@section('content')
    <table class="ui selectable sortable table" id="table-prescriptions">
		<thead>
			<tr>
				<th>Case</th>
				<th>Patient Name</th>
				<th>Treatment</th>
				<th>Date Prescribed</th>
				<th class="three wide column">Action</th>
			</tr>
		</thead>

		<tbody>
		</tbody>
		
		<tfoot>
			<tr>
				<th>Case</th>
				<th>Patient Name</th>
				<th>Treatment</th>
				<th>Date Prescribed</th>
				<th class="three wide column">Action</th>
			</tr>
		</tfoot>
	</table>
@stop

@push('js-plugins')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.semanticui.min.js"></script>
<script src="/js/prescriptions/index.js"></script>
@endpush