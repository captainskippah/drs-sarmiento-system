@extends('layouts.admin.default')
@push('css-plugins')<link rel="stylesheet" type="text/css" href="/css/admin/prescriptions/create.css">@endpush
@section('page-header', 'Prescriptions')

@section('content')

<form action="#" class="ui form" id="form-prescription">
    <div class="ui fluid card">
        <div class="content">
            <section class="section">
                <div class="ui dividing header">Prescription Details</div>
                <div class="ui centered grid">
                    <div class="six wide column">
                        <div class="field">
                            <label>Patient</label>
                            <div class="ui fluid selection search dropdown" id="dropdown-patient">
                                <input class="value" type="hidden" name="patient">
                                <i class="dropdown icon"></i>
                                <input class="search" autocomplete="off" tabindex="0">
                                <div class="default text">Type to search for patient</div>
                                <div class="menu" tabindex="-1"></div>
                            </div>
                        </div>

                        <div class="field">
                            <label>Case</label>
                            <input type="text" name="case">
                        </div>

                        <div class="field">
                            <label>Treatment</label>
                            <div class="ui fluid selection search dropdown" id="dropdown-treatment">
                                <input class="value" type="hidden" name="treatment">
                                <i class="dropdown icon"></i>
                                <input class="search" autocomplete="off" tabindex="0">
                                <div class="default text">Type to search for treatment (optional)</div>
                                <div class="menu" tabindex="-1"></div>
                            </div>
                        </div>

                        <div class="field">
                            <label>Extra Notes</label>
                            <textarea name="notes" placeholder="(optional)" rows="2"></textarea>
                        </div>
                    </div>
                </div>
            </section><!--./ prescription section -->

            <section class="section">
                <header>
                    <div class="ui dividing header">Medications</div>
                </header>
                
                <button type="button" class="ui blue button" id="btn-add-medication">Add Medicine</button>

                <div class="ui error message" id="error-medication">
                    <i class="close icon"></i>
                    <div class="header">Please provide at least one medication</div>
                </div>

                <table class="ui fixed striped table" id="table-medication">
                    <thead>
                        <tr>
                            <th class="five wide column">Medicine</th>
                            <th>Times per day</th>
                            <th>Days</th>
                            <th class="five wide column">Instructions</th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr class="js-empty-medication">
                            <td colspan="5" style="text-align: center;">No Medications Available</td>
                        </tr>
                    </tbody>
                </table>
            </section><!--./ medication section -->

            <section class="section">
                <button type="submit" class="ui green button">Create Prescription</button>
                <a href="/admin/prescriptions" class="ui basic button">Cancel</a>
            </section><!--./ submit section -->
        </div>
    </div>
</form>

<!-- Modal Medication -->
<div class="ui small modal" id="modal-medication">
    <div class="close icon"></div>
    <div class="header">Add New Medication</div>
    <div class="content">
        <form action="#" class="ui form" id="form-medication">
            <div class="field">
                <label>Medicine Name</label>
                <input type="text" name="name">
            </div>

            <div class="field">
                <label>Times per day</label>
                <input type="tel" name="frequency" maxlength="3" style="width: 15%;">
            </div>

            <div class="field">
                <label>Days</label>
                <input type="tel" name="days" maxlength="3" style="width: 15%">
            </div>

            <div class="field">
                <label>Instructions</label>
                <textarea rows="2" name="instructions"></textarea>
            </div>
            
            {{-- Added invisible submit to submit on Enter key --}}
            <button type="submit" style="display: none;"></button>
        </form>
    </div><!--/.modal content -->
    <div class="actions">
        <button type="button" class="ui button js-modal-close">Cancel</button>
        <button type="button" class="ui green button js-modal-submit">Add</button>
    </div>
</div><!-- Modal Medication -->

@stop

@push('js-plugins')
<script src="/js/jquery.validate-additional-methods.min.js"></script>
<script src="/js/prescriptions/create.js"></script>
@endpush