@extends('layouts.admin.default')

@push('custom-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.semanticui.min.css" integrity="sha256-xApYlmdj+dPTG+P7NRNeaxVKwhSuTsa4xQlTKo3V9Vw="crossorigin="anonymous">
@endpush

@section('page-header')
Categories
@stop


@section('content-top')
<div class="row">
    <div class="four wide column">
        <button type="button" class="ui green button" id="btn-new-category"><i class="plus icon"></i> Add New Category</button>
    </div>
</div>
@stop

@section('content')

<div class="ui small modal" id="modal-category-form">
	<div class="header">
		New Category
	</div>
	
	<div class="content">

		<form class="ui form" id="form-category" action="#">
			<div class="field">
				<label for="input-name">Category Name</label>
				<div class="ui input">
					<input type="text" id="input-name" name="name" placeholder="Category Name"> 					
				</div>
			</div>

			<div class="field">
				<label>Parent Category</label>
				<div class="ui multiple selection search dropdown" id="category-dropdown">
					<input type="hidden" id="input-parent" name="parent">
					<i class="dropdown icon"></i>
					<div class="default text">Select Parent Category</div>
					<div class="menu"></div>
				</div>
			</div>

			{{ method_field('POST') }}
		</form>

	</div>

	<div class="actions">
		<button type="button" class="ui red cancel button">
			Cancel
		</button>
		<button type="submit" form="form-category" class="ui green ok button">
			<i class="checkmark icon"></i> Save
		</button>
	</div>
</div> <!-- end of modal-form-category -->

<div class="ui small modal" id="modal-category-delete">
	<div class="header">
		Delete Category
	</div>

	<div class="content">
		Delete this category and all its subcategories?
	</div>

	<div class="actions">
		<form action="#" id="form-category-delete">{{ method_field('DELETE') }}</form>
		<button type="button" class="ui red cancel button">Cancel</button>
		<button type="submit" class="ui green ok button" form="form-category-delete">
			<i class="check icon"></i> Yes
		</button>
	</div>
</div>

<table class="ui selectable sortable table" id="table-categories">
	<thead>
		<tr>
			<th>Category Name</th>
			<th>Parent Category</th>
			<th class="four wide column">Actions</th>
		</tr>
	</thead>
</table>


@stop

@section('js-plugins')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.semanticui.min.js"></script>
<script type="text/javascript" src="/js/jquery.form.min.js"></script>
<script type="text/javascript">
$(document).ready(function($) {

	// Cache commonly used selectors
	var
		$modalCategoryForm   = $('#modal-category-form'),
		$modalCategoryDelete = $('#modal-category-delete'),
		$formCategory        = $('#form-category'),
		$formCategoryDelete  = $('#form-category-delete'),
		$btnNewCategory      = $('#btn-new-category'),
		$tableCategories     = $('#table-categories'),
		$btnSubmit           = $('#btn-submit'),
		$btnSubmitDelete     = $('#btn-submit-delete'),
		$categoryDropdown    = $('#category-dropdown');

	var $dataTable = $tableCategories.DataTable({
		ajax: '/admin/categories/table',
		serverSide: true,
		processing: true,
		columns:[
			{
				data: 'name',
				name: 'name'
			},
			{
				data: 'parent.name',
				defaultContent: '<i>None</i>'
			},
			{
				data: 'action',
				sortable: false,
				searchable: false,
				orderable: false
			}
		]
	})

	const validator = $formCategory.validate()

	$('#input-name').rules('add', {
		required: true
	})

	/****************************************************/

	$btnNewCategory.on('click', function(){
		$formCategory[0].reset()

		$formCategory
			.prop('action', '/admin/categories');

		$formCategory
			.find('[name="_method"]')
				.val('POST')

		$modalCategoryForm
			.find('.header')
			.text('New Category');

		$modalCategoryForm
			.modal('show');
	})

	$tableCategories.on('click', 'button[data-action="edit category"]', function(){
        let categoryData = $dataTable.row($(this).closest('tr')).data();

        $formCategory
			.prop('action', '/admin/categories/'+categoryData.id);

		$formCategory[0].reset()

		$formCategory
			.find('[name="name"]')
				.val(categoryData.name)
				.end()
			.find('[name="_method"]')
				.val('PUT')

		if(categoryData.parent){
			$categoryDropdown
				.dropdown('set value', categoryData.parent.id)
				.dropdown('set text', categoryData.parent.name);
		}

		$modalCategoryForm
			.find('.header')
			.text('Edit Category');

		$modalCategoryForm
			.modal('show');
	});

	$tableCategories.on('click', 'button[data-action="delete category"]', function(){
        let categoryData = $dataTable.row($(this).closest('tr')).data();

		$formCategoryDelete
			.prop('action', '/admin/categories/'+categoryData.id);

		$modalCategoryDelete
			.find('.content')
			.text('Delete "'+categoryData.name+'" and all its subcategories ?');

		$modalCategoryDelete.modal('show');
	});

	$categoryDropdown.dropdown({
		apiSettings:{
			url: '/admin/categories/search?q={query}'
		},
		maxSelection: 1,
		forceSelection: false,
		fullTextSearch: true,
	});

	/*****************************************************/

	function enableForm(){
		$modalCategoryForm
			.find('.ok')
				.removeClass('loading')
				.removeProp('disabled');

		$formCategory
			.find('.input')
				.removeProp('readonly')
				.removeClass('disabled');
	}

	function disableForm(){
		$formCategory
			.find('.input')
				.prop('readonly', true)
				.addClass('disabled');

		$modalCategoryForm
			.find('.ok')
				.addClass('loading')
				.prop('disabled', true);
	}

	function submitCategory(){
		if($formCategory.valid()){
			disableForm();

			submitForm({
				url : $formCategory.prop('action'),
				data: $formCategory.serializeArray()
			})
			.fail(enableForm)
			.done(function(){
				$modalCategoryForm.modal('hide');

				successNoty(
					$formCategory.form('get value', '_method') == 'POST' ?
					'New category was added' :
					'Category information was saved'
				);

				$dataTable.ajax.reload();
			});
		}

		return false;
	}

	$modalCategoryForm.modal({
		onShow: enableForm,
		onApprove: () => false, // Avoid closing the modal
		onDeny: () => $.noty.closeAll()
	});

	$formCategory.on('submit', submitCategory);

	$formCategory.on('reset', function(event) {
		validator.resetForm()

		$(this)
			.find('.error')
				.removeClass('error')
	})

	/**********************************************/

	function enableFormDelete(){
		$modalCategoryDelete
			.find('.ok')
				.removeClass('loading')
				.removeProp('disabled');
	}

	function disableFormDelete(){
		$modalCategoryDelete
			.find('.ok')
				.addClass('loading')
				.prop('disabled', true);
	}

	function deleteCategory(){

		disableFormDelete();

		submitForm({
			url : $formCategoryDelete.prop('action'),
			data: $formCategoryDelete.serializeArray()
		})
		.fail(enableFormDelete)
		.done(function(){
			$modalCategoryDelete.modal('hide');

			successNoty('Successfully deleted category');

			$dataTable.ajax.reload();
		});

		return false;
	}

	$modalCategoryDelete.modal({
		onShow: enableFormDelete,
		onApprove: () => false, // Avoid closing the modal
		onDeny   : () => $.noty.closeAll()
	});

	$formCategoryDelete.on('submit', deleteCategory);
});
</script>

@stop