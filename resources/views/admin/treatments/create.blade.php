@extends('layouts.admin.default')
@section('page-header', 'Treatments')

@section('content')

@if(count($errors) > 0)
	@include('includes.admin.message_error_list', ['errors' => $errors->all()])
@endif

{{ Form::open([
	'route' => empty($treatment) ? 'treatments.store' : ['treatments.update', $treatment->id],
	'class' => 'ui form',
	'id'    => 'form-treatment',
	'method' => empty($treatment) ? 'POST' : 'PUT'
]) }}

<div class="ui centered card">
	<div class="content">
		<div class="field">
			<label>Treatment Name</label>
			<input
				type="text"
				name="name"
				value="{{ empty($treatment) ? '' : $treatment->name }}">
		</div>

		<div class="field">
			<label>Price [&#8369;]</label>
			<input
				type="tel"
				name="price"
				value="{{ empty($treatment) ? '' : $treatment->price }}"
				style="width: 90px;">
		</div>
	</div>

	<div class="extra content">
		<button type="submit" class="ui green button">
			{{ empty($treatment) ? 'Create Treatment' : 'Update Treatment' }}
		</button>
	</div>
</div>


{{ Form::close() }}

@stop

@push('js-plugins')
<script src="/js/treatments/create.js"></script>
@endpush