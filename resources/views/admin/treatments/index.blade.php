@extends('layouts.admin.default')

@push('css-plugins')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.semanticui.min.css" integrity="sha256-xApYlmdj+dPTG+P7NRNeaxVKwhSuTsa4xQlTKo3V9Vw="crossorigin="anonymous">
@endpush

@section('page-header', 'Treatments')


@section('content-top')
    <a href="/admin/treatments/create" class="ui green button">
    	<i class="add icon"></i> New Treatment
    </a>
@stop

@section('content')
	<div id="page-message"></div>

	<table class="ui selectable sortable table" id="table-treatments">
		<thead>
			<tr>
				<th>Treatment Name</th>
				<th class="three wide right aligned">Price</th>
				<th class="three wide">Actions</th>
			</tr>
		</thead>

		<tbody></tbody>

		<tfoot>
			<tr>
				<th>Treatment Name</th>
				<th>Price</th>
				<th>Actions</th>
			</tr>
		</tfoot>
	</table>
@stop

@push('js-plugins')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.semanticui.min.js"></script>

<script type="text/handlebars-x-template" id="message">
	<div class="ui @{{ type }} message transition hidden">
		<i class="close icon"></i>
		<div class="header">@{{ header }}</div>
		<p>@{{{ message }}}</p>
	</div>
</script>

<script type="text/handlebars-x-template" id="modal-treatment-delete">
	<div class="ui small modal">
			<div class="header">Confirm</div>

			<div class="content">
				<p>This will delete "<b>@{{ name }}</b>". Continue?</p>
			</div>

			<div class="actions">
				<button type="button" class="ui cancel button">Cancel</button>
				<button type="button" id="btn-patient-archive" class="ui positive ok button">Delete</button>
			</div>
		</div>
</script>

<script src="/js/treatments/index.js"></script>
@endpush