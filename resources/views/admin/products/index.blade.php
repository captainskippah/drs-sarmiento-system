@extends('layouts.admin.default')

@push('custom-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.semanticui.min.css" integrity="sha256-xApYlmdj+dPTG+P7NRNeaxVKwhSuTsa4xQlTKo3V9Vw="crossorigin="anonymous">
@endpush

@section('page-header')
Inventory
@stop


@section('content-top')

@if(session('status'))
	<div class="ui success message">
		<i class="close icon"></i>
		<div class="header">
			<i class="check icon"></i>
			{{ session('status') == 'success' ?
				'Successfully added new product' : 'Successfully edited product' 
			}}
		</div>
		<p>{{ session('status') == 'success' ?
				'New product was added to the inventory' : 'Product information was saved'
			}}
		</p>
	</div>
@endif

<div class="row">
	<div class="four wide column">
		<a href="/admin/products/create" class="ui green button" id="btn-new-product">
			<i class="fa fa-plus"></i> New product
		</a>

		<button type="button" class="ui red button" data-mode="active" id="btn-table-mode">
			<i class="archive icon"></i> View archived products
		</button>
	</div>
</div>

@stop

@section('content')

<table class="ui compact striped selectable sortable table" id="table-products">

	<thead>
		<tr>
			<th>Image</th>
			<th class="three wide column">Name</th>
			<th>Price</th>
			<th>Cost</th>
			<th class="three wide column">Category</th>
			<th class="three wide column">Brand</th>
			<th>Current Stocks</th>
			<th>Alert Quantity</th>
			<th class="three wide column">Actions</th>
		</tr>
	</thead>

	<tbody></tbody>
	
</table>

<div class="ui small modal" id="modal-product">
	<div class="header">
		Restore Product?
	</div>

	<div class="content">
		Restore this product?
	</div>

	<div class="actions">
		<form action="#" method="POST" id="form-product">{{ method_field('PUT') }}</form>
		<button type="button" class="ui red cancel button">Cancel</button>
		<button type="submit" form="form-product" class="ui green ok button">
			<i class="check icon"></i>
			Yes
		</button>
	</div>
</div>

@stop

@section('js-plugins')
<script type="text/javascript" src="/js/jquery.form.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.semanticui.min.js"></script>
<script type="text/javascript">

	@if(session('status'))
		$('.ui.message').on('click', '.close', function(){
			$(this)
				.closest('.message')
				.transition('slide down', '700ms');
		});
	@endif

	// Cache commonly used selectors
	var
		$tableProducts      = $('#table-products'),

		$modalProduct = $('#modal-product'),
		$formProduct  = $('#form-product'),
		$btnSubmit    = $('#btn-submit'),

		$btnTableMode = $('#btn-table-mode');


	var $dataTable = $tableProducts.DataTable({
		ajax: {
			url: '/admin/products/table',
			data: function(data){
				return $.extend( {}, data, {
					archived: $btnTableMode.data('mode') == 'archived'
				});
			}
		},
		serverSide: true,
		processing: true,
		columnDefs:[
			{
				targets: [0,-1],
				searchable: false,
				orderable: false,
				sortable: false,
			}
		],
		order:[
			['1', 'asc']
		],
		columns:[
			{
				data: 'image'
			},
			{
				data:'name',
				name: 'name'
			},
			{
				data: 'price',
				name: 'price'
			},
			{
				data:'cost',
				name:'cost'
			},
			{
				data:'category',
				name:'category.name'
			},
			{
				data:'brand',
				name:'brand.name'
			},
			{
				data:'stocks.quantity',
				name:'stocks.quantity'
			},
			{
				data:'alert_qty',
				name:'alert_qty'
			},
			{
				data: 'action'
			}
		]
	});

	// Table Draw Handler
	$dataTable.on('draw', function(){
		$tableProducts
			.find('.ui.dropdown')
			.dropdown({
				action: 'hide'
			});
	});

	// Table Mode Handler (Active or Archived)
	$btnTableMode.on('click', function(){
		if($(this).data('mode') == 'active'){

			$(this)
				.data('mode', 'archived')
				.removeClass('red')
				.addClass('teal')
				.html('<i class="upload icon"></i> View active products');
		}else if($(this).data('mode') == 'archived'){

			$(this)
				.data('mode', 'active')
				.removeClass('teal')
				.addClass('red')
				.html('<i class="archive icon"></i> View archived products');
		}

		$dataTable.ajax.reload();
		return true;
	});

	// Delete Button Handler
	$tableProducts.on('click', 'div[data-action="delete product"]', function(){
        let productData = $dataTable.row($(this).closest('tr')).data();
		
        $formProduct
				.prop('action', '/admin/products/'+productData.id)
				.form('set value', '_method', 'DELETE');

		$modalProduct
			.find('.header')
				.text('Archiving Product')
				.end()
			.find('.content')
				.text('Archive "'+productData.name+'" ?');

		$modalProduct.modal('show');
	});

	// Restore Button Handler
	$tableProducts.on('click', 'div[data-action="restore product"]', function(){
        let productData = $dataTable.row($(this).closest('tr')).data();
		
        $formProduct
				.prop('action', '/admin/products/'+productData.id+'/restore')
				.form('set value', '_method', 'PUT');

		$modalProduct
			.find('.header')
				.text('Restoring Product')
				.end()
			.find('.content')
				.text('Restore "'+productData.name+'" ?');

		$modalProduct.modal('show');
	});

	// Make modal un-closable
	$modalProduct.modal({
		closable : false,
		onShow   : enableForm,
		onApprove: () => false,
		onDeny   : () => $.noty.closeAll()
	});

	// Form Handler ( Archive / Restore )
	$formProduct.on('submit', function(event){
		event.preventDefault();

		disableForm();
		
		submitForm({
			url : $formProduct.prop('action'),
			data: $formProduct.serializeArray()
		})
		.done(function(){
			$modalProduct.modal('hide');

			successNoty(
				$formProduct.form('get value', '_method') == 'DELETE' ?
				'Product is now archived' :
				'Product is now restored'
			);

			$dataTable.ajax.reload();
		});
	});

	function enableForm(){
		$modalProduct
			.find('.ok')
				.removeProp('disabled')
				.removeClass('disabled')
				.removeClass('loading');
	}

	function disableForm(){
		$modalProduct
			.find('.ok')
				.prop('disabled', true)
				.addClass('disabled')
				.addClass('loading');

	}

</script>
@stop