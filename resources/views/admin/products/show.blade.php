@extends('layouts.admin.default')

@section('page-header')
Inventory
@stop


@section('content')

<div class="ui top attached tabular menu" id="tab-menu-product">
	<a data-tab="details" class="active item"><i class="fa fa-fw fa-info-circle"></i> Product Details</a>
	<a data-tab="history" class="item"><i class="fa fa-fw fa-history"></i> History</a>
	<a href="/admin/products/{{ $product->id }}/edit" class="item"><i class="fa fa-fw fa-pencil"></i> Edit</a>
</div>
<div class="ui bottom attached active product tab segment" data-tab="details">
	<div class="ui centered grid">
		<div class="row">
			<div class="six wide column">
				<img class="ui image" src="/img/products/{{ $product->image }}">
			</div>	

			<div class="eight wide column">
				<table class="ui basic striped table">
					<tbody>
						<tr>
							<td class="right aligned four column wide">Name</td>
							<td><strong>{{ $product->name }}</strong></td>
						</tr>

						<tr>
							<td class="right aligned four column wide">Brand</td>
							<td><strong>{{ empty($product->brand) ? 'None' : $product->brand->name }}</strong></td>
						</tr>

						<tr>
							<td class="right aligned four column wide">Category</td>
							<td><strong>{{ $product->category->name }}</strong></td>
						</tr>

						<tr>
							<td class="right aligned four column wide">Cost</td>
							<td><strong>{{ $product->cost }}</strong></td>
						</tr>

						<tr>
							<td class="right aligned four column wide">Price</td>
							<td><strong>{{ $product->price }}</strong></td>
						</tr>

						<tr>
							<td class="right aligned four column wide">Alert Quantity</td>
							<td><strong>{{ $product->alert_qty }}</strong></td>
						</tr>

						<tr>
							<td class="right aligned four column wide">Unit Of Measure</td>
							<td><strong>{{ $product->unit }}</strong></td>
						</tr>
					</tbody>
				</table>

				<table class="ui center aligned striped table" style="margin-top: 2em;">
					<thead>
						<tr>
							<th>Stock ID</th>
							<th>Quantity</th>
							<th>Manufacturing Date</th>
							<th>Expiration Date</th>
						</tr>
					</thead>

					<tbody>
						@foreach($product->stocks as $stock)
							<tr>
								<td>{{ $stock->id }}</td>
								<td>{{ $stock->quantity }}</td>
								<td>{{ date('F d, Y', strtotime($stock->manufactured)) }}</td>
								<td>{{ date('F d, Y', strtotime($stock->expiration)) }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>	

			<div class="fourteen wide column" style="margin-top: 3em;">
				<h4 class="ui top attached blue message">Product Details</h4>
	          	<div class="ui bottom attached segment">{{ $product->details }}</div>
			</div>
		</div>
	</div>
</div>

<div class="ui bottom attached product tab segment" data-tab="history">
	<div class="ui centered grid">
		<div class="row">
			<div class="fourteen wide column">
				<table class="ui center aligned striped table">
					<thead>
						<tr>
							<th>Stock ID</th>
							<th>Value</th>
							<th>Description</th>
							<th>Transaction Date</th>
						</tr>
					</thead>

					<tbody>
						@foreach($product->stocks as $stock)
							@foreach($stock->transactions as $transaction)
								<tr>
									<td>{{ $stock->id }}</td>
									<td>{{ sprintf("%+d", $transaction->value) }}</td>
									<td>{{ $transaction->description }}</td>
									<td>{{ date('F d, Y', strtotime($transaction->updated_at)) }}</td>
								</tr>
							@endforeach
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@stop

@section('js-plugins')
<script type="text/javascript">

	// Cache commonly used selectors
	var
		$tabMenu = $('#tab-menu-product');


		$tabMenu.find('.item').tab();

</script>
@stop