@extends('layouts.admin.default')

@push('custom-css')
<link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/BreadMaker/semantic-ui-daterangepicker/master/daterangepicker.min.css">
@endpush

@section('page-header')
Inventory
@stop

@section('content-top')

	@if(session('status') == 'error')
	<div class="ui error message transition hidden">
		<i class="close icon"></i>
		<div class="header">Server Error</div>
		<p>There was a problem while adding new item. Please contact the Administrator/Developer</p>
	</div>
	@endif

	@if(count($errors) > 0)
	<div class="ui error message transition hidden" id="form-error">
		<i class="close icon"></i>
		<div class="header">There was some errors with your submission</div>
		<ul class="list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
		</ul>
	</div>
	@endif
@stop

@section('content')
	{{ Form::open([
		'class'  => 'ui form',
		'id'     => 'form-product',
		'route'  => empty($product) ? 'products.create' : ['products.update', $product->id], 
		'method' => empty($product) ? 'POST' : 'PUT',
		'files'  => true
	]) }}

    <div class="ui centered grid">
		<div id="page-products" class="row">
			<div class="eight wide column">
				<div class="ui fluid card">
					<div class="content">
						<div class="header">Product Information</div>
					</div>
					<div class="content" style="background-color: #FAFAFA;">
						<div class="field">
							<label>Brand</label>
							<div class="ui fluid multiple selection search dropdown" id="dropdown-brand">
			                	<input
			                		type="hidden"
			                		id="input-brand"
			                		value="{{ old('brand', !empty($product->brand) ? $product->brand->id : '') }}"
			                		name="brand" />
			                	<i class="dropdown icon"></i>
			            		<div class="default text">Type to search for brand</div>
			                	<div class="menu">
			                		@if(!empty($product->brand))
			                			<div class="active item" data-value="{{ $product->brand->id }}">{{ $product->brand->name }}</div>
			                		@endif
			                	</div>
			                </div>
						</div>

						<div class="field">
			                <label>Product Name (required)</label>
			                <input
			                	type="text"
			                	id="input-name"
			                	name="name"
			                	placeholder="Paracetamol"
			                	value="{{ old('name', !empty($product) ? $product->name  : '') }}" />
			            </div>

			            <div class="field">
			                <label>Category (required)</label>
			                <div class="ui fluid selection search dropdown" id="dropdown-category">
			                	<input
			                		type="hidden"
			                		id="input-category"
			                		value="{{ old('category', !empty($product) ? $product->category->id : '') }}"
			                		name="category" />
			                	<i class="dropdown icon"></i>

			                	@if(!empty($product))
			                		<div class="text">{{ $product->category->name }}</div>
			                		@else
			                		<div class="default text">Type to search for category</div>
			                	@endif

			                	<div class="menu"></div>
			                </div>
			            </div>

			            <div class="field">
			                <label>Price [&#8369;] (required)</label>
			                <input
			                	type="text"
			                	id="input-price"
			                	name="price"
			                	value="{{ old('price', !empty($product) ? ($product->price == '0.00' ? '' : $product->price) : '') }}"
			                	placeholder="15.00"
			                	style="width: 20%;" />
			            </div>

			            <div class="field">
			                <label>Cost [&#8369;]</label>
			                <input
			                	type="text"
			                	id="input-cost"
			                	name="cost"
			                	value="{{ old('cost', !empty($product) ? ($product->cost == '0.00' ? '' : $product->cost) : '') }}"
			                	placeholder="10.00"
			                	style="width: 20%;" />
			            </div>

						<div class="field">
			            	<label>Unit Of Measure (required)</label>
			            	<input
			            		type="text"
			            		id="input-units"
			            		name="unit"
			            		value="{{ old('unit', !empty($product) ? $product->unit : '') }}"
			            		placeholder="kg"
			            		style="width: 20%;" />
			            </div>

			            <div class="field">
			                <label>Alert Qty</label>
			                <input
			                	type="number"
			                	min="1"
			                	name="quantity-alert"
			                	value="{{ old('quantity-alert', !empty($product) ? ($product->alert_qty == 0 ? '' : $product->alert_qty) : '') }}"
			                	placeholder="10"
			                	style="width: 20%;" />
			            </div>

			            <div class="field" id="field-product-details">
			                <label>Product Details</label>
			                <textarea
			                	rows="5"
			                	name="details">{{ old('details', !empty($product) ? $product->details : '') }}</textarea>
			            </div>

			            <div class="field">
			            	<label>Product Image</label>
			            	<div class="ui fluid card" id="card-img-preview">
		                        <div class="image">
		                            <img id="img-preview" src="https://placehold.it/250" />
		                        </div>
		                        <div class="content">
		                            <div class="header" id="img-name">No Image Selected</div>
		                            <div class="meta" id="img-size">
		                                (0.0kB)
		                            </div>
		                        </div>
		                        <div class="extra content">
		                        	<label class="ui fluid blue browse button">
		                                <i class="fa fa-folder-open"></i> Browse
		                				<input accept="image/png, image/jpeg" type="file" id="input-image" name="image" style="display: none;">
		                            </label>
		                        </div>
		                    </div>
			            </div>
					</div><!-- Card Content -->
					<div class="extra content" style="background-color: #FAFAFA;">
						<button type="button" class="js-next ui right floated blue button">Next <i class="fa fa-chevron-right"></i></button>
					</div>
				</div><!-- Card -->
			</div><!-- eight wide column -->
		</div><!-- row -->

        <div id="page-stocks" class="row" style="display: none;">
            <div class="fifteen wide column">
				<div class="ui fluid card">
					<div class="content">
						<div class="header">Product Stocks</div>
					</div>
					<div class="content">
						<div class="ui stock info message">
			            	<p><i class="fa fa-exclamation-circle"></i> At least one stock is required</p>
			            </div>

						<table class="ui center aligned compact celled striped table" id="table-stocks">
		            		<thead>
		            			<tr>
		            				<th class="two wide column">Quantity (required)</th>
		            				<th>Manufacturing Date</th>
		            				<th>Expiration Date</th>
		            				<th class="one wide column"></th>
		            			</tr>
		            		</thead>

		            		<tbody>
		            			@if(!empty($product))
		            				@foreach($product->stocks as $stock)
		            				<input type="hidden" name="stock_id[]" value="{{ $stock->id }}">
		            				<tr>
			            				<td>
			            					<div class="ui input">
				            					<input
				            						value="{{ $stock->quantity }}"
				            						class="center aligned"
				            						type="number"
				            						name="quantity-stock[]"
				            						placeholder="100"
				            						min="1" />
				            				</div>
			            				</td>
			            				<td>
			            					<input value="{{ !empty($stock->manufactured) ? date('m/d/Y', strtotime($stock->manufactured)) : '' }}" type="text" class="manufactured" name="manufactured[]" placeholder="MM/DD/YYYY">
			            				</td>
			            				<td>
			            					<input value="{{ !empty($stock->expiration) ? date('m/d/Y', strtotime($stock->expiration)) : '' }}" type="text" class="expiration" name="expiration[]" placeholder="MM/DD/YYYY">
			            				</td>
			            				<td>
			            					<button type="button" class="ui tiny red delete icon button">
			            						<i class="trash icon"></i>
			            					</button>
			            				</td>
			            			</tr>
		            				@endforeach
		            				@else
			            			<tr>
			            				<td>
			            					<div class="ui input">
				            					<input
				            						class="center aligned"
				            						type="number"
				            						name="quantity-stock[]"
				            						placeholder="100"
				            						min="1" />
				            				</div>
			            				</td>
			            				<td>
			            					<input type="text" class="manufactured" name="manufactured[]" placeholder="MM/DD/YYYY">
			            				</td>
			            				<td>
			            					<input type="text" class="expiration" name="expiration[]" placeholder="MM/DD/YYYY">
			            				</td>
			            				<td>
			            					<button type="button" class="ui tiny red delete icon button">
			            						<i class="trash icon"></i>
			            					</button>
			            				</td>
			            			</tr>
			            		@endif

		            			<tr>
		            				<td></td>
		            				<td></td>
		            				<td></td>
		            				<td>
		            					<button type="button" class="ui tiny blue icon button" id="btn-add-stocks">
		            						<i class="plus icon"></i>
		            					</button>
		            				</td>
		            			</tr>
		            		</tbody>
		            	</table>
					</div><!-- Card Content -->
					<div class="extra content">
						<button type="button" class="ui button js-previous">Previous</button>
						<button type="submit" class="ui right floated blue button">Submit</button>
					</div>
				</div><!-- Card -->
	        </div><!-- Column -->
	    </div><!-- Row -->
    </div><!-- ui centered grid -->
</form>



@stop

@section('js-plugins')
<script src="http://momentjs.com/downloads/moment.min.js"></script>
<script src="https://cdn.rawgit.com/BreadMaker/semantic-ui-daterangepicker/master/daterangepicker.min.js"></script>
<script src="https://cdn.rawgit.com/brianreavis/sifter.js/master/sifter.min.js"></script>
<script type="text/javascript">
	{{-- Should remove this in the future --}}
	@if(count($errors) > 0)
		$('#form-error').transition('slide down', '700ms');

		$('#form-error').on('click', '.close', function(){
			$(this)
				.closest('.message')
				.transition('slide down', '700ms');
		})
	@endif

	// Cache commonly used selectors
	var
		$dropdownType        = $('#dropdown-type'),
		$dropdownBrand       = $('#dropdown-brand'),
		$dropdownCategory    = $('#dropdown-category'),

		$formProduct         = $('#form-product'),
		$tableStocks         = $('#table-stocks'),

		$btnAddStocks        = $('#btn-add-stocks'),
		
		$inputCategory       = $('#input-category'),
		$imagePreview        = $('#img-preview'),
		$inputImage          = $('#input-image'),
		$inputUnits          = $('#input-units'),
		$inputPrice          = $('#input-price'),
		$inputCost           = $('#input-cost'),
		$inputName           = $('#input-name'),
		$imageName           = $('#img-name'),
		$imageSize           = $('#img-size');

		$.validator.addMethod('regex', function (value, element, regexp) {
			var re = new RegExp(regexp)
			return this.optional(element) || re.test(value)
		}, "Please check your format")

		$formProduct.validate({
			ignore: "",
			highlight: function(element, errorClass) {
				if ($(element).parent('.ui.input').length > 0) {
					$(element)
						.parent('.ui.input')
							.addClass('error')
				} else {
					$.validator.defaults.highlight(element, errorClass)
				}
			},
			unhighlight: function(element, errorClass) {
				if ($(element).parent('.ui.input').length > 0) {
					$(element)
						.parent('.ui.input')
						.removeClass('error')
							.find('.ui.label')
							.remove()
				} else {
					$.validator.defaults.unhighlight(element, errorClass)
				}
			}
		})

		$inputName.rules('add', {
			required: true
		})

		$inputCategory.rules('add', {
			required: false
		})

		$inputCost.rules('add', {
			regex: '^$|^([1-9][0-9]{,2}(,[0-9]{3})*|[0-9]+)(\.[0-9]{2,2})?$'
		})

		$inputPrice.rules('add', {
			required: true,
			regex: '^$|^([1-9][0-9]{,2}(,[0-9]{3})*|[0-9]+)(\.[0-9]{2,2})?$'
		})

		$inputUnits.rules('add', {
			required: true,
			regex: '[A-Za-z]',
			messages: {
				regex: 'Unit of measure must be letters only'
			}
		})

		$inputImage.rules('add', {
			required: $('[name="_method"]').val() === 'POST'
		})

		$('.js-next').on('click', function(event) {
			if ($formProduct.valid()) {
				$('#page-products').hide()
				$('#page-stocks').show()
			}
		})

		$('.js-previous').on('click', function(event) {
			$('#page-stocks').hide()
			$('#page-products').show()
		})

		$formProduct.on('submit', function(event){
			if (!$formProduct.valid() || !hasStock()) {
				return false
			}
			
			$(this)
				.addClass('loading');

			$(this)
				.find(':submit')
					.prop('disabled', true)
					.addClass('disabled')
					.addClass('loading');
		})

		function hasStock() {
			const hasStock = $tableStocks
				.find('[name="quantity-stock[]"]')
				.filter(function () {
					return $(this).val() > 0
				}).length > 0

			if (!hasStock) {
				$('.ui.stock.message')
					.removeClass('info')
					.addClass('error')
					.show()
					.transition('shake')
			}

			return hasStock
		}

		$inputImage.on('change', function(){
			let files  = $(this)[0].files;
			let reader = new FileReader;

			reader.onloadend = function(){
				$imageName.text(files[0].name);
				$imageSize.text( Math.round(files[0].size / 1000 * 100) / 100 + 'kB' );
				$imagePreview.prop('src', reader.result);
			}

			reader.readAsDataURL(files[0]);
		});

		$inputImage.on('change', function(){
			if( $(this).closest('.field').hasClass('error') ){
				$(this).closest('.error').removeClass('error');
			}
		})

		$tableStocks.on('click', 'button.delete', function(){
			$(this).closest('tr').remove();
		});

		$btnAddStocks.on('click', function(){
			let lastRow = $tableStocks.find('tbody tr').eq(-1);

			lastRow.before(
				'<tr>'+
    				'<td>'+
    					'<input class="center aligned" type="number" name="quantity-stock[]" placeholder="100" min="1">'+
    				'</td>'+
    				'<td>'+
    					'<input readonly type="text" class="manufactured" name="manufactured[]" placeholder="MM/DD/YYYY">'+
    				'</td>'+
    				'<td>'+
    					'<input readonly type="text" class="expiration" name="expiration[]" placeholder="MM/DD/YYYY">'+
    				'</td>'+
    				'<td>'+
    					'<button type="button" class="ui tiny red delete icon button">'+
    						'<i class="trash icon"></i>'+
    					'</button>'+
    				'</td>'+
				'</tr>'
			);

			lastRow.next().find('.expiration').daterangepicker({
				singleDatePicker: true,
				drops: 'up',
				minDate: moment()
			});

			lastRow.next().find('.manufactured').daterangepicker({
				singleDatePicker: true,
				drops: 'up',
				maxDate: moment()
			});

		})

		$('.expiration').daterangepicker({
			singleDatePicker: true,
			drops: 'up',
			minDate: moment()
		});

		$('.manufactured').daterangepicker({
			singleDatePicker: true,
			drops: 'up',
			maxDate: moment()
		});

		let request = null;

		let brands      = [];
		let brandSifter = new Sifter([]);

		$dropdownBrand.dropdown({
			forceSelection: false,
			maxSelections: 1,
			placeholder: 'Type to search for brand',
			apiSettings:{
				responseAsync: function(settings, callback){

					let query = $dropdownBrand.find('.search').val();
					
					// No query and has stored results. Show them.
					if(query.length == 0 && brands.length > 0){
						return callback({
							sucess: true,
							results: brands
						});
					}

					let results = brandSifter.search(query,{
						fields: ['text'],
						sort: [{field: 'text', direction: 'asc'}]
					});

					// Not available locally. Fetch from server.
					if(results.total == 0){
						request = $.ajax({
							url: '/admin/brands/search/'+query,
							beforeSend: function(){
								if(request !== null){
									request.abort();
									request = null;
								}
							}
						})
						.done(function(response){

							// Update local copy.
							if(response.success){
								brands      = brands.concat(response.results);
								brandSifter = new Sifter(brands);
							}

							return callback(response);
						});						
					}else{
						let localResults = [];

						// for each item in results, push it to localResults.
						results.items.map(function(value, index){
							localResults.push(brands[value.id])
						})

						return callback({
							success: true,
							results: localResults
						})
					}
				}
			}
		});

		let categories = [];
		let categoriesSifter = new Sifter([]);

		$dropdownCategory.dropdown({
			apiSettings:{
				responseAsync: function(settings, callback){
					let query = $dropdownCategory.find('.search').val();

					// No query and has stored results. Show them.
					if(query.length == 0 && categories.length > 0){
						return callback({
							sucess: true,
							results: categories
						});
					}

					let results = categoriesSifter.search(query,{
						fields: ['text'],
						sort: [{field: 'text', direction: 'asc'}]
					});

					// Not available locally
					if(results.total == 0){
						request = $.ajax({
							url: '/admin/categories/search/?q='+query,
							beforeSend: function(){
								if(request !== null){
									request.abort();
									request = null
								}
							}
						})
						.done(function(response){
							if(response.success){
								categories = categories.concat(response.results);
								categoriesSifter = new Sifter(categories);
							}

							return callback(response);
						});						
					}else{
						let localResults = [];

						// for each item in results, push it to localResults.
						results.items.map(function(value, index){
							localResults.push(categories[value.id])
						})

						return callback({
							success: true,
							results: localResults
						})
					}

				}
			}
		});
</script>
@stop