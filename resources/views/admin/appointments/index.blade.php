@extends('layouts.admin.default')

@push('custom-css')
<link rel="stylesheet" type="text/css" href="/fullcalendar/dist/fullcalendar.min.css">
@endpush

@section('page-header')
Appointments
@stop


@section('content-top')

<div class="ui info message">
	<div class="header">Future Enhancement Plans:</div>
	<ul class="list">
		<li>Integrate System itself to Google Calendar for better experience like in-site editing of events, etc.</li>
	</ul>
</div>

<div class="row">
	<div class="four wide column">
		<button type="button" class="ui green button" id="btn-new-appointment">
			<i class="fa fa-plus"></i> New Appointment
		</button>
		<a href="https://calendar.google.com/calendar" class="ui basic green button">
			<i class="external icon"></i> Open Google Calendar
		</a>
	</div>
</div>

@stop

@section('content')

<div class="ui small modal" id="modal-new-appointment">
	<div class="header">New Appointment (W.I.P)</div>

	<div class="content">
		<form class="ui form" id="form-appointment">
			<div class="field">
				<label>Title</label>
				<input type="text" name="">
			</div>

			<div class="fields">
				<div class="field">
					<label>Start Date</label>
					<input type="text">
				</div>

				<div class="field">
					<label>Start Time</label>
					<input type="text" name="" id="start-time">
				</div>

				<div class="field">
					<label>End Date</label>
					<input type="text">
				</div>

				<div class="field">
					<label>End Time</label>
					<input type="text" name="" id="end-time">
				</div>
			</div>


			<div class="field">
				<label>Patient</label>
				<div class="ui search selection dropdown" id="patientDropdown">
					<input type="hidden">
					<i class="dropdown icon"></i>
					<div class="menu">
						<div class="item" data-value="poop">Sample</div>
					</div>
				</div>
			</div>
		</form>
	</div>

	<div class="actions">
		<button type="button" class="ui red cancel button">Cancel</button>
		<button type="button" class="ui green ok button">
			<i class="check icon"></i>
			Yes
		</button>
	</div>
</div>

<div class="ui small modal" id="modal-view-appointment">

	<div class="header"></div>

	<div class="content"></div>

	<div class="actions">
		<button type="button" class="ui red cancel button">Close</button>
		<a href="#" class="ui green ok button">View in Google Calendar</a>
	</div>

</div>

<div id="calendar">
	
</div>

@stop

@push('js-plugins')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="/fullcalendar/public/moment/moment.js"></script>
<script type="text/javascript" src="/fullcalendar/dist/fullcalendar.min.js"></script>
<script type="text/javascript" src="/fullcalendar/dist/gcal.js"></script>
<script type="text/javascript">
	
$(document).ready(function($) {

	// Cache commonly used selectors
	var 
		$calendar            = $('#calendar'),
		$dropdownPatient     = $('#dropdownPatient'),
		$inputStart          = $('#input-start'),
		$inputEnd            = $('#input-end'),

		$inputStartTime      = $('#start-time'),
		$inputEndTime        = $('#end-time'),

		$modalNewAppointment = $('#modal-new-appointment'),
		$btnNewAppointment   = $('#btn-new-appointment'),

		$modalViewAppointment = $('#modal-view-appointment');

	$btnNewAppointment.on('click', function () {
		$modalNewAppointment.modal('show')
	})

	$modalNewAppointment.modal()

	$modalViewAppointment.find('.ok').on('click', function (event) {
		event.preventDefault()
		event.stopPropagation()
		window.open($(this).attr('href'))
		$modalViewAppointment.modal('hide')
	})

	$calendar.fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'listDay,month,agendaWeek,agendaDay'
		},

		googleCalendarApiKey: 'AIzaSyCAiVX4zIHue6mnsr2su0RTBquPrMx9ElA',

		events: {
			googleCalendarId: 'fa5ioktrrsd0fqisluq54hfs74@group.calendar.google.com',
			editable: true
		},

		eventClick: function (event, jsEvent, view) {
			$modalViewAppointment
				.find('.header')
					.text(event.title)
					.end()
				.find('.content')
					.text(event.description)
					.end()
				.find('.actions .ok')
					.attr('href', event.url)

			$modalViewAppointment.modal('show')

			return false
		},

		defaultView: 'listDay'
	});

});

</script>

@endpush