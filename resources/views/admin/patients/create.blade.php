@extends('layouts.admin.default')

@section('page-header', 'Patient Records')

@section('content')
{{ Form::open([
	'route'  => empty($patient) ? 'patients.store' : ['patients.update', $patient->id],
	'class'  => 'ui custom form',
	'id'     => 'form-patient',
	'method' => empty($patient) ? 'POST' : 'PUT'
]) }}
	@if(count($errors) > 0)
		@include('includes.admin.message_error_list', [ 'errors' => $errors->all() ])
	@endif

	<div class="ui fluid card">
		<div class="content">
			<section id="personal">
				<h2 class="ui dividing header"><i class="fa fa-user"></i> Personal Information</h2>
				<div class="ui centered grid">
					<div class="six wide column">
						<div class="field">
							<label>First Name</label>
							<input
								type="text"
								id="input-firstname"
								name="personal[firstname]"
								value="{{ empty($patient) ? '' : $patient->firstname }}"
								maxlength="255" />
						</div>
						
						<div class="field">
							<label>Middle Name</label>
							<input
								type="text"
								id="input-middlename"
								name="personal[middlename]"
								placeholder="(optional)"
								value="{{ empty($patient) ? '' : $patient->middlename }}"
								maxlength="255" />
						</div>

						<div class="field">
							<label>Last Name</label>
							<input
								type="text"
								id="input-lastname"
								name="personal[lastname]"
								value="{{ empty($patient) ? '' : $patient->lastname }}"
								maxlength="255" />
						</div>

						<div class="twelve wide field">
							<label>Gender</label>
							<div class="two small ui buttons" id="btn-genders">
								@php($male = !empty($patient) && strtolower($patient->gender) === 'male')
		                    	<label class="ui {{ $male ? 'active' : ''}} inverted blue button">
		                    		<i class="male icon"></i> Male
		                    		<input
		                    			type="radio"
		                    			value="male"
		                    			name="personal[gender]"
		                    			{{ $male ? 'checked' : ''}} />
		                    	</label>
		                    	<div class="or"></div>
		                    	@php($female = !empty($patient) && strtolower($patient->gender) === 'female')
		                    	<label class="ui {{ $female ? 'active' : '' }} inverted pink button">
		                    		<i class="female icon"></i> Female
		                    		<input
		                    			type="radio"
		                    			value="female"
		                    			name="personal[gender]"
		                    			{{ $female ? 'checked' : '' }} />
		                    	</label>
		                	</div>
						</div>

						<div class="field">
							<label>Age</label>
							<input
								type="tel"
								id="input-age"
								name="personal[age]"
								maxlength="2"
								style="width: 20%;"
								value="{{ empty($patient) ? '' : $patient->age }}" />
						</div>
					</div>
				</div>
			</section><!--/.personal-->

			<section id="contact">
				<h2 class="ui dividing header"><i class="fa fa-phone"></i> Contact Information</h2>
				<div class="ui centered grid">
					<div class="six wide column">
						<div class="field">
							<label>Phone</label>
							<div
								class="ui selection compact dropdown"
								id="dropdown-prefix"
								tabindex="0"
								style="width: 25%;" >
								@php($phone = empty($patient) ? '0905' : substr($patient->phone, 0, 4))
								<input type="hidden" name="personal[prefix]" value="{{ $phone }}">
							 	<i class="dropdown icon"></i>
							 	<div class="text">{{ $phone }}</div>
							 	<div class="menu" tabindex="-1">
							 		<div class="item" data-value="0905">0905</div>
							 		<div class="item" data-value="0906">0906</div>
							 		<div class="item" data-value="0907">0907</div>
							 		<div class="item" data-value="0908">0908</div>
							 		<div class="item" data-value="0909">0909</div>
							 		<div class="item" data-value="0910">0910</div>
							 		<div class="item" data-value="0912">0912</div>
							 		<div class="item" data-value="0915">0915</div>
							 		<div class="item" data-value="0916">0916</div>
							 		<div class="item" data-value="0917">0917</div>
							 		<div class="item" data-value="0918">0918</div>
							 		<div class="item" data-value="0919">0919</div>
							 		<div class="item" data-value="0920">0920</div>
							 		<div class="item" data-value="0921">0921</div>
							 		<div class="item" data-value="0922">0922</div>
							 		<div class="item" data-value="0923">0923</div>
							 		<div class="item" data-value="0924">0924</div>
							 		<div class="item" data-value="0925">0925</div>
							 		<div class="item" data-value="0926">0926</div>
							 		<div class="item" data-value="0927">0927</div>
							 		<div class="item" data-value="0928">0928</div>
							 		<div class="item" data-value="0929">0929</div>
							 		<div class="item" data-value="0930">0930</div>
							 		<div class="item" data-value="0932">0932</div>
							 		<div class="item" data-value="0933">0933</div>
							 		<div class="item" data-value="0935">0935</div>
							 		<div class="item" data-value="0936">0936</div>
							 		<div class="item" data-value="0937">0937</div>
							 		<div class="item" data-value="0938">0938</div>
							 		<div class="item" data-value="0939">0939</div>
							 		<div class="item" data-value="0942">0942</div>
							 		<div class="item" data-value="0943">0943</div>
							 		<div class="item" data-value="0946">0946</div>
							 		<div class="item" data-value="0947">0947</div>
							 		<div class="item" data-value="0948">0948</div>
							 		<div class="item" data-value="0949">0949</div>
							 		<div class="item" data-value="0950">0950</div>
							 		<div class="item" data-value="0975">0975</div>
							 		<div class="item" data-value="0977">0977</div>
							 		<div class="item" data-value="0989">0989</div>
							 		<div class="item" data-value="0996">0996</div>
							 		<div class="item" data-value="0997">0997</div>
							 		<div class="item" data-value="0998">0998</div>
							 		<div class="item" data-value="0999">0999</div>
							 	</div><!-- end of menu -->
							</div><!-- end of dropdown -->
							<input
								type="tel"
								id="input-phone"
								maxlength="7"
								name="personal[phone]"
								style="width: 30%;"
								value="{{ empty($patient) ? '' : substr($patient->phone, 4, 7) }}" />
						</div>
					</div>
				</div>
			</section><!--/.contact-->

			<section id="address">
				<h2 class="ui dividing header"><i class="fa fa-home"></i> Address Information</h2>
				<div class="ui centered grid">
					<div class="six wide column">
						<div class="field">
							<label>Address Line 1</label>
		                    <input
	                    		name="address[line1]"
	                    		type="text" id="input-address1"
	                    		placeholder="123 Rizal Street, Jose Subdivision"
	                    		value="{{ empty($patient) ? '' : $patient->address_1 }}" />
							<span class="help">House Number, Street Address, Subdivision</span>
						</div>

						<div class="field">
							<label>Address Line 2</label>
		                	<input
		                		name="address[line2]"
		                		type="text"
		                		id="input-address2"
		                		placeholder="Juan's Apartment Floor 3, Unit 5 (optional)"
		                		value="{{ empty($patient) ? '' : $patient->address_2 }}" />
		                  	<span class="help">Apartment, Suite, Unit, Building, Floor, etc (optional)</span>
						</div>

						<div class="field">
							<label>Province</label>
							<div class="ui loading fluid selection search dropdown" id="dropdown-province">
							@php($province = empty($patient) ? '354' : $patient->provCode)
					         	<input class="value" type="hidden" name="address[province]" value="{{ $province }}">
					         	<i class="dropdown icon"></i>
					          	<input class="search" autocomplete="off" tabindex="0">
					          	<div class="text"></div>
					          	<div class="menu" tabindex="-1"></div>
					        </div>
						</div>

						<div class="field">
							<label>City / Municipality</label>
							<div class="ui disabled fluid selection search dropdown" id="dropdown-city">
								@php($city = empty($patient) ? '35401' : $patient->citymunCode)
					         	<input class="value" type="hidden" name="address[city]" value="{{ $city }}">
					         	<i class="dropdown icon"></i>
					          	<input class="search" autocomplete="off" tabindex="0">
					          	<div class="text"></div>
					          	<div class="menu" tabindex="-1"></div>
					        </div>
						</div>

						<div class="field">
							<label>Barangay</label>
							<div class="ui disabled fluid selection search dropdown" id="dropdown-barangay">
								@php($barangay = empty($patient) ? '35401001' : $patient->brgyCode)
					         	<input class="value" type="hidden" name="address[barangay]" value="{{ $barangay }}">
					         	<i class="dropdown icon"></i>
					          	<input class="search" autocomplete="off" tabindex="0">
					          	<div class="text"></div>
					          	<div class="menu" tabindex="-1"></div>
					        </div>
						</div>
					</div>
				</div>
			</section><!--/.address-->

			<section>
				<div class="ui centered grid">
					<div class="six wide column">
						<button type="submit" class="ui blue button">Submit</button>
						<a href="/admin/patients" class="ui basic button">Cancel</a>
					</div>
				</div>
			</section>
		</div>
	</div><!--/.card-->
{{ Form::close() }}
@endsection

@push('js-plugins')
<script src="/js/patients/create.js"></script>
@endpush