@extends('layouts.admin.default')
@section('page-header', 'Patient Records')

@push('css-plugins')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.semanticui.min.css" integrity="sha256-xApYlmdj+dPTG+P7NRNeaxVKwhSuTsa4xQlTKo3V9Vw="crossorigin="anonymous">
@endpush

@section('content-top')
	<a href="/admin/patients/create" class="ui green button">
		<i class="add user icon"></i> Add new patient
	</a>

	<div class="right floated ui buttons">
	    <button type="button" class="ui teal button js-table-mode" data-mode="active" disabled>
	    	<i class="circle check icon"></i> Show Active
	    </button>
	    <div class="or"></div>
	    <button type="button" class="ui red button js-table-mode" data-mode="archived">
	    	<i class="archive icon"></i> Show Archived
	    </button>
	</div>

	@if(session('status') === 'success-store')
		@include('includes.admin.message_success', ['message' => 'Successfully created record'])	
	@elseif(session('status') === 'success-update')
		@include('includes.admin.message_success', ['message' => 'Successfully updated record'])	
	@endif
@stop

@section('content')
	<table class="ui selectable striped table" id="table-patients" data-mode="active">
		<thead>
			<tr>
				<th>ID</th>
				<th>First Name</th>
				<th>Middle Name</th>
				<th>Last Name</th>
				<th>Age</th>
				<th>Phone</th>
				<th class="three wide column">Action</th>
			</tr>
		</thead>

		<tbody></tbody>

		<tfoot>
			<tr>
				<th>ID</th>
				<th>First Name</th>
				<th>Middle Name</th>
				<th>Last Name</th>
				<th>Age</th>
				<th>Phone</th>
				<th class="three wide column">Action</th>
			</tr>
		</tfoot>
	</table>

	<!-- Modal Patient Restore -->
	<div class="ui small modal" id="modal-patient-archive">
		<div class="header">Archive this record</div>

		<div class="content">
			<p>Are you sure you want to archive this record</p>
		</div>

		<div class="actions">
			<button type="button" class="ui cancel button">No</button>
			<button type="button" id="btn-patient-archive" class="ui positive ok button">Yes</button>
		</div>
	</div><!-- Modal Patient Delete -->

	<!-- Modal Patient Restore -->
	<div class="ui small modal" id="modal-patient-restore">
		<div class="header">Restore this record</div>

		<div class="content">
			<p>Are you sure you want to restore this record</p>
		</div>

		<div class="actions">
			<button type="button" class="ui cancel button">No</button>
			<button type="button" id="btn-patient-restore" class="ui positive ok button">Yes</button>
		</div>
	</div><!-- Modal Patient Restore -->

	<!-- Modal Patient View -->
	<div class="ui small patient modal" id="modal-patient-view">
		<div class="close"><i class="close icon"></i></div>
		<div class="header">Viewing Patient Information</div>

		<div class="content">
			<div class="ui huge header js-name"></div>

			<div class="ui buttons">
				<a href="#" class="ui blue button js-prescriptions">
					<i class="fa fa-fw fa-pencil-square-o"></i> Prescriptions
				</a>
				<a href="#" class="ui teal button js-appointments">
					<i class="fa fa-fw fa-calendar"></i> Appointments
				</a>
			</div>

			<table class="ui striped table">
				<tbody>
					<tr>
						<td><strong>Age</strong></td>
						<td class="js-age">19</td>
					</tr>
					<tr>
						<td><strong>Phone</strong></td>
						<td class="js-phone">09365424262</td>
					</tr>
					<tr>
						<td><strong>Address</strong></td>
						<td class="js-address">ADADASDkjwelqjelqejqwlejlqjw</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="actions">
			<a href="#" class="ui yellow button js-edit">Edit</a>
			<button type="button" class="ui cancel button">Close</button>
		</div>
	</div><!-- Modal Patient View -->
@stop

@push('js-plugins')
<!-- Patients DataTables Dependencies  -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.semanticui.min.js"></script>

<script src="/js/patients/index.js"></script>
@endpush