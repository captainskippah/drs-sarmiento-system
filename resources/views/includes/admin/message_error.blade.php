<div class="ui visible error message">
	<div class="header">{{ $message }}</div>
	<i class="close icon"></i>
</div>