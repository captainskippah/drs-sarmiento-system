<div class="ui visible error message">
	<div class="header">Please fix the following errors:</div>
	<i class="close icon"></i>
	<ul class="list">
		@foreach($errors as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>