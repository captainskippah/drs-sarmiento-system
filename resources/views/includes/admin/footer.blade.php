    <!-- jQuery -->
    <script src="/js/jquery.min.js"></script>
    <!-- jQuery Validation -->
    <script src="/js/jquery.validate.min.js"></script>
    <!-- Semantic UI Core Javascript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.3/semantic.min.js"></script>
    <!-- Handlebars -->
    <script src="/js/handlebars-v4.0.5.js"></script>
	<!-- Default Settings -->
	<script src="/js/admin/defaults.js"></script>

    <!-- 3rd-party plugins -->
    @stack('js-plugins')

</body>

</html>
