<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="Clinic Dashboard">
    <meta name="author" content="Lemuel Flores">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') - @yield('page-header')</title>

    <!-- Semantic UI -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.3/semantic.css" rel="stylesheet">

    <!-- Fonts -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" />

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/admin/sb-admin.css">

    @stack('css-plugins')
</head>

<body>

