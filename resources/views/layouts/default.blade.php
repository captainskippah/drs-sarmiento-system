@section('title')
Drs. Sarmiento Clinic
@stop

@push('custom-css')
<link rel="stylesheet" type="text/css" href="/css/default.css">
@endpush

@include('includes.head')


	<nav class="ui huge fixed borderless menu" id="nav">
		<div href="#" class="item">
            <img class="ui tiny image" src="/img/logo.png">
        </div>
        <div class="right menu">
            <a href="#home" class="active item">Home</a>
            <a href="#segment-products" class="item">Products</a>
            <a href="#segment-facilities" class="item">Facilities</a>
            <a href="#segment-aboutus" class="item">About Us</a>
            <a href="#" class="item">
                <button class="ui red button">Schedule an appointment</button>
            </a>
        </div>
	</nav>

	<main>
		@yield('content')
	</main>

    <footer>
        <div class="ui container">
            <div class="ui items">
                <div class="item">
                    <div class="ui medium image">
                        <img src="/img/logo.png">
                    </div>

                    <div class="middle aligned content">
                        <p>
                            <i class="phone icon"></i> +63 906 123 4567
                        </p>
                        <p>
                            <i class="mail icon"></i> drssarmiento@gmail.com
                        </p>
                        <p>
                            <i class="location arrow icon"></i> 123 Rizal Street, Angeles City
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>


@include('includes.footer')
