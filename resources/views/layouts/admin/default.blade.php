@section('title')
Clinic Dashboard
@stop

@include('includes.admin.head')

    <div class="ui fixed large inverted top borderless menu">
        <a href="#" class="brand item">Drs. Sarmiento Clinic</a>

        <div class="right menu">
            <a href="/admin/pos" class="item"><i class="shopping bag icon"></i> POS</a>
            <div class="ui dropdown item" id="dropdownProfile">
                <i class="user icon"></i> Finn The Human <i class="dropdown icon"></i>
                <div class="inverted menu">
                    <a href="{{ url('admin/logout') }}" class="item"><i class="power icon"></i> Logout</a>
                </div>
            </div>
        </div>
    </div>

    <div class="ui visible large inverted vertical sidebar menu" id="main">
        <a
            class="{{ Request::is('admin/dashboard*') ? 'active' : '' }} item"
            href="{{ url('admin/dashboard') }}"
        >
            <i class="fa fa-fw fa-dashboard"></i> Dashboard
        </a>

        <a
            class="{{ Request::is('admin/appointments*') ? 'active' : '' }} item"
            href="{{ url('admin/appointments') }}"
        >
            <i class="fa fa-fw fa-calendar"></i> Appointments
        </a>

        <a
            class="{{ Request::is('admin/products*') ? 'active' : '' }} item"
            href="{{ url('admin/products') }}"
        >
            <i class="fa fa-fw fa-barcode"></i> Products
        </a>

        <a
            class="{{ Request::is('admin/brands*') ? 'active' : '' }} item"
            href="{{ url('admin/brands') }}"
        >
            <i class="fa fa-fw fa-th-list"></i> Brands
        </a>

        <a
            class="{{ Request::is('admin/categories*') ? 'active' : '' }} item"
            href="{{ url('admin/categories') }}"
        >
            <i class="fa fa-fw fa-folder"></i> Categories
        </a>

        <a
            class="{{ Request::is('admin/patients*') ? 'active' : '' }} item"
            href="{{ url('admin/patients') }}"
        >
            <i class="fa fa-fw fa-user"></i> Patients
        </a>

        <a
            class="{{ Request::is('admin/prescriptions*') ? 'active' : '' }} item"
            href="{{ url('admin/prescriptions') }}"
        >
            <i class="fa fa-fw fa-pencil-square-o"></i> Prescriptions
        </a>

        <a
            class="{{ Request::is('admin/treatments*') ? 'active' : '' }} item"
            href="{{ url('admin/treatments') }}"
        >
            <i class="fa fa-fw fa-stethoscope"></i> Treatments
        </a>
    </div>

    <div class="pusher">

        <div id="page-wrapper">

            <!-- Page Heading -->
            <header>
                <h1 class="ui huge header">@yield('page-header')</h1>
                <div class="ui divider"></div>
            </header>

            <!-- Main Content -->
            <main id="content-main">
                <div id="content-top">
                    @yield('content-top')
                </div>

                @yield('content')     
            </main>

        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#pusher -->

@include('includes.admin.footer')