
import Immutable from 'immutable';

export const openModalPatientForm = (patientID = 0) => {
	return {
		type: 'OPEN_MODAL_PATIENT_FORM',
		payload: patientID
	}
}

export const closeModalPatientForm = (state) => {
	return{
		type: 'CLOSE_MODAL_PATIENT_FORM'
	}
}


const requestProvinces = () => {

	return{
		type: 'REQUEST_PROVINCES',
	}

}

const receivedProvinces = (state) => {
	return{
		type: 'RECEIVED_PROVINCES',
		payload: state
	}
}

export const FetchProvinces = (state) => {

	return function(dispatch){

		dispatch(requestProvinces())

		$.ajax({
			url: '/admin/ajax/province',
		})
		.done(function(response) {
			dispatch(receivedProvinces(response));
		})
		.fail(function() {
			console.log("error");
		})
	}
}

const requestCities = () => {
	return{
		type: 'REQUEST_CITIES'
	}
}

const receivedCities = (state) => {
	return{
		type: 'RECEIVED_CITIES',
		payload: state
	}
}

export const FetchCities = (provinceID) => {
	return function(dispatch){

		dispatch(requestCities())

		$.ajax({
			url: '/admin/ajax/province/'+provinceID+'/city'
		})
		.done(function(response) {
			dispatch(receivedCities({
				cities: response,
				provinceID: provinceID
			}));
		})
		.fail(function() {
			console.log("error");
		})		
	}
}

const requestBarangays = () => {
	return{
		type: 'REQUEST_BARANGAYS'
	}
}

const receivedBarangays = (state) => {
	return{
		type: 'RECEIVED_BARANGAYS',
		payload: state
	}
}

export const FetchBarangays = (provinceID, cityID) => {
	return function(dispatch){

		dispatch(requestBarangays())

		$.ajax({
			url: '/admin/ajax/province/'+provinceID+'/city/'+cityID+'/barangay/'
		})
		.done(function(response) {
			dispatch(receivedBarangays({
				barangays: response,
				cityID: cityID
			}));
		})
		.fail(function() {
			console.log("error");
		})
	}
}

const requestPrefixes = () => {
	return{
		type: 'REQUEST_PREFIXES'
	}
}

const receivedPrefixes = (state) => {
	return{
		type: 'RECEIVED_PREFIXES',
		payload: state
	}
}

export const FetchPrefixes = () => {
	return function(dispatch){

		dispatch(requestPrefixes())

		$.ajax({
			url: '/admin/ajax/prefix'
		})
		.done(function(response) {
			dispatch(receivedPrefixes(response));
		})
		.fail(function() {
			console.log("error");
		})		
	}
}

/**************
* Patient App *
***************/

const requestPatient = () => {
	return{
		type: 'REQUEST_PATIENT'
	}
}

const receivedPatient = (state) => {
	return{
		type: 'RECEIVED_PATIENT',
		payload: state
	}
}

export const FetchPatient = (patientID) => {
	return function(dispatch){

		dispatch(requestPatient())

		$.ajax({
			url: '/admin/patients/'+patientID
		})
		.done(function(response){
			dispatch(receivedPatient(response));
		})
		.fail(function(){
			console.log('error');
		});
	}
}

export const searchPatient = (name) => {
	return function(dispatch){

		dispatch(requestPatient())

		$.ajax({
			url: '/admin/patients/search/'+name,
		})
		.done(function(response){
			response.search = true;
			dispatch(receivedPatient(response));
		})
		.fail(function() {
			console.log("error");
		})
	}
}

/*******************
* Prescription App *
********************/

export const openModalPrescription = (prescriptionID = null) => {
	return {
		type: 'OPEN_MODAL_PRESCRIPTION',
		payload: prescriptionID
	}
}

export const closeModalPrescription = () => {
	return{
		type: 'CLOSE_MODAL_PRESCRIPTION'
	}
}

export const modalPrescriptionNext = () => {
	return{
		type: 'MODAL_PRESCRIPTION_NEXT'
	}
}

export const modalPrescriptionPrev = () => {
	return{
		type: 'MODAL_PRESCRIPTION_PREV'
	}
}
/****************
* Treatment App *
*****************/

export const searchTreatment = (name) => {
	return function(dispatch){

		dispatch(requestTreatment());

		$.ajax({
			url: '/admin/treatments/search/'+name,
		})
		.done(function(results) {
			dispatch(receivedTreatment(results));
		})
		.fail(function() {
			console.log("error");
		});
	}
}

const requestTreatment = () => {
	return{
		type: 'REQUEST_TREATMENT'
	}
}

const receivedTreatment = (results) => {
	return{
		type: 'RECEIVED_TREATMENT',
		payload: results
	}
}

/*********************
* Prescriptions Form *
**********************/

export const addMedicineRow = () => {
	return{
		type: 'ADD_MEDICINE_ROW'
	}
}

export const deleteMedicineRow = (rowID) => {
	return{
		type: 'DELETE_MEDICINE_ROW',
		payload: rowID
	}
}

export const submitPrescription = () => {
	return{
		type: 'SUBMIT_PRESCRIPTION'
	}
}

/*********
* Brands *
**********/

export const searchBrand = (brand) => {
	return function(dispatch){
		dispatch(requestBrand());

		$.ajax({
			url: '/admin/brands/search/'+brand
		})
		.done(function(results) {
			results.search = true;
			dispatch(receivedBrand(results));
		})
		.fail(function() {
			console.log("error");
		})		
	}
}

const requestBrand = () => {
	return{
		type: 'REQUEST_BRAND'
	}
}

const receivedBrand = (results) => {
	return{
		type: 'RECEIVED_BRAND',
		payload: results
	}
}

/*************
* Categories *
**************/

export const searchCategory = (category) => {
	return function(dispatch){
		dispatch(requestCategory());

		$.ajax({
			url: '/admin/categories/search/'+category
		})
		.done(function(results){
			results.search = true;
			dispatch(receivedCategory(results));
		})
		.fail(function(){
			console.log('error');
		});
	}
}

const requestCategory = () => {
	return{
		type: 'REQUEST_CATEGORY'
	}
}

const receivedCategory = (results) => {
	return{
		type: 'RECEIVED_CATEGORY',
		payload: results
	}
}