import React, { Component } from 'react';
import { render } from 'react-dom';
import { Provider, connect } from 'react-redux';
import { createStore, combineReducers, applyMiddleware, bindActionCreators } from 'redux';
import thunkMiddleware from 'redux-thunk'
import { Field, reduxForm, reducer as formReducer, formValueSelector } from 'redux-form';

import Sifter from 'sifter';
import classNames from 'classnames';


import Brands from './reducers/Brands.js';
import Categories from './reducers/Categories.js';

import {
	searchBrand,
	searchCategory
} from './actions/index.js';

const reducers = combineReducers({
	brands: Brands,
	categories: Categories,
	form: formReducer
});

const store = createStore(
	reducers,
	applyMiddleware(
		thunkMiddleware
	)
);

class InventoryForm extends Component {

	constructor(){
		super();

		this.state = {
			type: '1',
			imageName: 'No Image Selected',
			imageSize: '0.0',
			imageURL: 'https://placehold.it/260x250'
		}
	}

	componentDidMount(){
		$(this._typeDropdown).dropdown({
			onChange: this.onTypeChange.bind(this)
		});
		$(this._subcategoryDropdown).dropdown();
		$(this._addDropdown).dropdown();
	}

	componentWillReceiveProps(nextProp){
		const { image, type } = nextProp;

		if(image){
			this.loadImage(image[0]);
		}

		if(type == 1){
			$(this._previewField).slideDown();
			$(this._costField).slideDown();
			$(this._alertQtyField).slideDown();
			$(this._stockQtyField).slideDown();

			$(this._addField).slideUp();
			$(this._groupedField).slideUp();
		}else{
			$(this._previewField).slideUp();
			$(this._costField).slideUp();
			$(this._alertQtyField).slideUp();
			$(this._stockQtyField).slideUp();

			$(this._addField).slideDown();
			$(this._groupedField).slideDown();
		}
	}

	loadImage(image){
		let reader = new FileReader();

		reader.onloadend = () => {
			this.setState({
				imageName: image.name,
				imageSize: Math.round(image.size / 1000 * 100) / 100,
				imageURL: reader.result
			})
		}

		reader.readAsDataURL(image);
	}

	onTypeChange(value, text, element){
		this.props.change('type', value);
	}

	render(){
		return (
			<form className="ui form" action='#' id="form-product">
			    <div className="ui centered relaxed grid">
			        <div className="row">
			            <div className="eight wide column" id="column-form-products">
			                <div className="field">
			                    <label>Product Type</label>
			                    <div className="ui fluid selection dropdown" id="input-product-type" ref={(c) => this._typeDropdown = c }>
			                        <Field component="input" type="hidden" name="type" />
			                        <i className="dropdown icon"></i>
			                        <div className="text">Single Product</div>
			                        <div className="menu">
			                            <div className="active selected item" data-value="1">Single Product</div>
			                            <div className="item" data-value="2">Grouped Product</div>
			                        </div>
			                    </div>
			                </div>


			                <div className="field">
			                    <label>Brand</label>
			                    <Dropdown
			                    	fluid
			                    	searchable
			                    	selectable
			                   		name="brand"
			                   		defaultText="Select Brand"
			                   		active={ this.props.brand }
			                   		loading={ this.props.brands.get('fetching') }
			                   		items={ this.props.brands.get('brands') }
			                   		search={ this.props.searchBrand }
			                   		change={ this.props.change }
			                   	/>
			                </div>
			                <div className="field">
			                    <label>Product Name</label>
			                    <Field component="input" type="text" name="name" placeholder="Product Name" />
			                </div>

			                <div className="field">
			                    <label>Category</label>
			                    <Dropdown
			                    	fluid
			                    	searchable
			                    	selectable
			                    	name="category"
			                    	defaultText="Select Category"
			                    	change={ this.props.change }
			                    	search={ this.props.searchCategory }
			                    	loading={ this.props.categories.get('fetching') }
			                    	items={ this.props.categories.get('categories') }
			                    />
			                </div>

			                <div className="field">
			                    <label>Sub-Category</label>

			                    <Dropdown
			                    	disabled={ this.props.category == undefined || this.props.categories.getIn(['categories', this.props.category, 'subcategories']).isEmpty() }
			                    	items={ this.props.category && !this.props.categories.getIn(['categories', this.props.category, 'subcategories']).isEmpty() ? this.props.categories.getIn(['categories', this.props.category, 'subcategories']) : Immutable.List() }
			                    	fluid
			                    	searchable
			                    	selectable
			                    	name="subcategory"
			                    	defaultText="Select Sub-Category"
			                    	change={ this.props.change }
			                    />
			                </div>

			                <div className="field" ref={(c) => this._costField = c}>
			                    <label>Cost ({ String.fromCharCode(8369) })</label>
			                    <Field component="input" type="text" name="cost" placeholder="Cost" />
			                </div>

			                <div className="field">
			                    <label>Price ({ String.fromCharCode(8369) })</label>
			                    <Field component="input" type="text" name="price" placeholder="Price" />
			                </div>

			                <div className="field" ref={(c) => this._stockQtyField = c}>
			                    <label>Stock Qty</label>
			                    <Field component="input" type="number" name="quantity-stock" placeholder="Stock Quantity" />
			                </div>

			                <div className="field" ref={(c) => this._alertQtyField = c}>
			                    <label>Alert Qty</label>
			                    <Field component="input" type="number" name="quantity-alert" placeholder="Alert Quantity" />
			                </div>

			                <div className="field">
			                    <label>Product Details</label>
			                    <Field component="textarea" rows={5} name="details" />
			                </div>
			            </div>
			            <div className="seven wide column">
			                <div className="field" ref={(c)=> this._previewField = c}>
			                    <label>Product Image</label>
			                    <div className="ui fluid card" id="card-img-preview">
			                        <div className="image">
			                            <img src={ this.state.imageURL } />
			                        </div>
			                        <div className="content">
			                            <div className="header">{ this.state.imageName }</div>
			                            <div className="meta">
			                                ({ this.state.imageSize }kB)
			                            </div>
			                        </div>
			                        <div className="extra content">
			                        	<label className="ui fluid blue browse button">
		                                    <i className="fa fa-folder-open"></i> Browse
		                    				<Field component="input" accept="image/png, image/jpeg" type="file" name="image" style={{display:'none'}} />
		                                </label>
			                        </div>
			                    </div>
			                </div>

			                <div className="field" ref={(c)=> this._addField = c} style={{display: 'none'}}>
			                	<label>Add Product</label>
			                	<div className="ui fluid search selection dropdown" ref={(c) => this._addDropdown = c}>
			                		<Field component="input" type="hidden" name="addDropdown" />
			                        <div className="default text">Add Product</div>
			                        <div className="menu">
			                            <div className="item">Product 1</div>
			                            <div className="item">Product 2</div>
			                        </div>
			                	</div>
			                </div>

			                <div className="field" ref={(c)=> this._groupedField = c} style={{display: 'none'}}>
			                	<label>Grouped Products</label>
			                	<table className="ui celled striped table" id="table-grouped-products">
			                		<thead>
			                			<tr className="center aligned">
			                				<td>Product Name</td>
			                				<td>Quantity</td>
			                				<td>Unit Price</td>
			                				<td><i className="trash icon" /></td>
			                			</tr>
			                		</thead>

			                		<tbody></tbody>
			                	</table>
			                </div>
			            </div>
			        </div>
			    </div>
			</form>
		);
	}
}

/***************************************/

class Dropdown extends Component{
	constructor(){
		super()

		this.state = {
			searchValue: false,
			searchTimer: null,
			sifter: new Sifter([])
		}

		this.onChange   = this.onChange.bind(this);
		this.onKeyPress = this.onKeyPress.bind(this);
		this.sifter     = this.sifter.bind(this);
	}

	componentDidMount(){
		$(this._dropdown).dropdown({
			onChange: this.onChange,
			fullTextSearch: true
		});

		if(this.props.search){
			$(this._dropdown).on('keypress', '.search', this.onKeyPress);
		}
	}

	componentWillReceiveProps(nextProp){
		const oldProp = this.props;

		if(!oldProp.items.equals(nextProp.items)){
			this.setState({
				sifter: new Sifter(nextProp.items.toJS())
			});

			$(this._dropdown).find('.menu .message').remove();
		}

		if(nextProp.loading){
			$(this._dropdown).addClass('loading');
			$(this._dropdown).dropdown('hide');
		}
	}

	componentDidUpdate(prevProps, prevState){
		$(this._dropdown).dropdown('refresh');

		if (this.props.active){
			$(this._dropdown).dropdown('set selected', this.props.active);	
		}

		if(!this.props.loading){
			$(this._dropdown).removeClass('loading');
			$(this._dropdown).dropdown('show');
		}
	}

	componentWillUnmount(){
		$(this._dropdown).off('keypress', '.search', this.onKeyPress);
		$(this._dropdown).dropdown('destroy');
	}

	onChange(value){
		this.props.change(this.props.name, value);
	}

	onKeyPress(event){
		if(event.which !== 0 && !event.ctrlKey && !event.metaKey && !event.altKey){
			
			clearTimeout(this.state.searchTimer);

			let searchValue = $(this._dropdown).find('.search').val();
			let results     = this.sifter(searchValue);

			if( results.total == 0 ){
				this.setState({
					searchTimer: setTimeout(() => {
						this.props.search(searchValue)
					}, 200),
					searchValue: searchValue
				});
			}
		}
	}

	sifter(searchValue){
		const sifter  = this.state.sifter;
		const results = sifter.search(searchValue, {
			fields: ['text'],
			sort: [{field: 'text', direction: 'asc'}]
		});

		return results;
	}

	renderItems(items){
		let renderedItems = items.map((value,index) => {
			return <DropdownItem key={value.get('value')} name={value.get('name')} value={value.get('value')} />
		});

		return renderedItems;
	}

	render(){
		const dropdownClass = classNames(
			'ui',
			{
			'fluid': this.props.fluid,
			'search': this.props.searchable,
			'selection': this.props.selectable,
			'disabled': this.props.disabled,
			},
			'dropdown'
		)

		const textClass = classNames({
			'default': !this.props.active && !this.state.searchValue,
			'text': true,
			'filtered': this.state.searchValue
		})

		let items = !this.props.items.isEmpty() ? this.renderItems(this.props.items.toArray()) : null

		return(
			<div className={dropdownClass} id={this.props.id} ref={(c) => this._dropdown = c }>
                <Field component="input" type="hidden" name={this.props.name} />
                <i className="dropdown icon"></i>
                <div className={textClass}>{this.props.defaultText}</div>
                <div className="menu">{ items }</div>
            </div>
		)
	}
}

/**************************************************/

class DropdownItem extends Component{
	constructor(){
		super()
	}

	render(){
		return(
			<div className="item" key={this.props.value} data-value={this.props.value}>{this.props.name}</div>
		)
	}
}

/**************************************************/


const InventoryComponent = reduxForm({
	form: 'inventory'
})(InventoryForm)

const selector = formValueSelector('inventory')

const mapStateToProps = (state) => {
	const { brands, categories } = state;

	const
		type     = selector(state, 'type'),
		brand    = selector(state, 'brand'),
		category = selector(state, 'category'),
		image    = selector(state, 'image');

	return{
		initialValues: {
			type: '1'
		},
		type,
		brand,
		category,
		image,
		name,
		brands,
		categories
	}
}

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		searchBrand,
		searchCategory
	}, dispatch)
}

const InventoryApp = connect(
	mapStateToProps,
	mapDispatchToProps
)(InventoryComponent);

render(
	<Provider store={store}>
		<InventoryApp />
	</Provider>,
	document.getElementById('inventory')
);