import thunkMiddleware from 'redux-thunk'
import {createStore, applyMiddleware} from 'redux';
import allReducers from './reducers/index.js';

const store = createStore(
	allReducers,
	applyMiddleware(
		thunkMiddleware
	)
);