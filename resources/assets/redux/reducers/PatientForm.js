
import Immutable from 'immutable';

const InitialState = Immutable.Map({
	open: false,
	profileID: 0,

	defaultPrefix: '0905',
	defaultProvince: 354,
	defaultCity: 35401
});

export default function PatientForm(state = InitialState, action){
	let {payload} = action;
	switch(action.type){
		case 'OPEN_MODAL_PATIENT_FORM':
			return state.set('open', true).set('profileID', payload);
		case 'CLOSE_MODAL_PATIENT_FORM':
			return state.set('open', false)
		default:
			return state;
	}
}