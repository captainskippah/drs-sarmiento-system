
import Immutable from 'immutable';

const InitialState = Immutable.Map({
	open: false,
	loading: true,
	prescriptionID: 0,
	highestRow: 1
});

export default function PatientForm(state = InitialState, action){
	let {payload} = action;
	switch(action.type){
		case 'ADD_MEDICINE_ROW':
			return state.set('highestRow', state.get('highestRow') + 1);
		case 'SUBMIT_PRESCRIPTION':
			return state.set('loading', true);
		default:
			return state;
	}
}