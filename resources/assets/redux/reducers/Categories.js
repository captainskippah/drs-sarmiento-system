
import Immutable from 'immutable';

const InitialState = Immutable.Map({
	fetching: false,
	categories: Immutable.Map()
});

export default function Categories(state = InitialState, actions){
	let { payload } = actions;

	switch(actions.type){
		case 'REQUEST_CATEGORY':
			return state.set('fetching', true)
			break;
		case 'RECEIVED_CATEGORY':
			if(payload.search){
				if(payload.success){
					payload.results.forEach(function(value, key){
						value.subcategories = Immutable.fromJS(value.subcategories);
						state = state.setIn(['categories', value.value.toString()], Immutable.Map(value))
					});
					return state.set('fetching', false);
				}else{
					return state;
				}
			}
			return state.set('fetching', false);
		default:
			return state;
	}
}