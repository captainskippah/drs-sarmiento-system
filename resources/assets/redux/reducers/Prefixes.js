
import Immutable from 'immutable';

const InitialState = Immutable.Map({
	fetching: false,
	prefixes: Immutable.List()
});

export default function Prefixes(state = InitialState, action){

	let {payload} = action;

	switch (action.type) {
		case 'REQUEST_PREFIXES':
			return state.set('fetching', true);
			break;
		case 'RECEIVED_PREFIXES':
			state = state
				.set('fetching', false)
				.set('prefixes', Immutable.List(payload));

			return state;
		default:
			return state;
			break;
	}

}