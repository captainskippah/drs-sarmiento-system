
import Immutable from 'immutable';

const InitialState = Immutable.Map({
	fetching: false,
	brands: Immutable.List()
});

export default function Brands(state = InitialState, actions){
	let { payload } = actions;

	switch(actions.type){
		case 'REQUEST_BRAND':
			return state.set('fetching', true)
			break;
		case 'RECEIVED_BRAND':
			if(payload.search){
				if(payload.success){
					payload.results.forEach(function(value, key){
						state = state.set('brands', state.get('brands').push(Immutable.Map(value)))
					});
				}
			}
			return state.set('fetching', false);
		default:
			return state;
	}
}