
import Immutable from 'immutable';

const InitialState = Immutable.Map({
	fetching: false,
	cities: Immutable.Map()
});

export default function Cities(state = InitialState, action){

	let {payload} = action;

	switch (action.type) {
		case 'REQUEST_CITIES':
			return state.set('fetching', true);
			break;
		case 'RECEIVED_CITIES':
			state = state
				.set('fetching', false)
				.setIn(['cities', payload.provinceID], payload.cities);
			return state;
		default:
			return state;
			break;
	}

}