
import Immutable from 'immutable';

const InitialState = Immutable.Map({
	fetching: false,
	barangays: Immutable.List()
});

export default function Barangays(state = InitialState, action){

	let {payload} = action;

	switch (action.type) {
		case 'REQUEST_BARANGAYS':
			return state.set('fetching', true);
			break;
		case 'RECEIVED_BARANGAYS':
			state = state
				.set('fetching', false)
				.setIn(['barangays', payload.cityID], payload.barangays);
			return state;
		default:
			return state;
			break;
	}

}