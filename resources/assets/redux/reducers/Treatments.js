
import Immutable from 'immutable';

const InitialState = Immutable.Map({
	fetching: false,
	treatments: Immutable.Map()
});

export default function Treatments(state = InitialState, action){

	let {payload} = action;

	switch(action.type){
		case 'REQUEST_TREATMENT':
			return state.set('fetching', true);
		case 'RECEIVED_TREATMENT':
			if(!payload.success){
				return state;
			}else{
				payload.results.forEach(function(value, key){
					state = state.setIn(['treatments', value.value], Immutable.Map(value))
				});

				return state.set('fetching', false);
			}
		default:
			return state;
	}

}