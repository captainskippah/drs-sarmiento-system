
import {combineReducers} from 'redux';
import Patients from './Patients.js';
import Prefixes from './Prefixes.js';
import Provinces from './Provinces.js';
import Cities from './Cities.js';
import Barangays from './Barangays.js';
import PatientForm from './PatientForm.js';
import Prescriptions from './Prescriptions.js';
import Treatments from './Treatments.js';
import PrescriptionForm from './PrescriptionForm';

export const allReducers = combineReducers({
	Patients,
	Prefixes,
	Provinces,
	Cities,
	Barangays,
	PatientForm
});


export const PrescriptionReducers = combineReducers({
	Prescriptions,
	Treatments,
	Patients,
	PrescriptionForm
});