
import Immutable from 'immutable';

const InitialState = Immutable.Map({
	open: false,
	step: 1,
	prescriptionID: 0,
	fetching: false
});

export default function Prescriptions(state = InitialState, action){

	switch(action.type){
		case 'OPEN_MODAL_PRESCRIPTION':
			return state.set('open', true).set('prescriptionID', action.payload);
		case 'CLOSE_MODAL_PRESCRIPTION':
			return state.set('open', false).set('step', 1);
		case 'MODAL_PRESCRIPTION_NEXT':
			return state.set('step', state.get('step') + 1);
		case 'MODAL_PRESCRIPTION_PREV':
			return state.set('step', state.get('step') - 1);
		default:
			return state;
	}

}