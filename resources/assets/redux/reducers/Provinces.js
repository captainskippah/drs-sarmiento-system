
import Immutable from 'immutable';

const InitialState = Immutable.Map({
	fetching: false,
	provinces: Immutable.List()
});

export default function Provinces(state = InitialState, action){

	let {payload} = action;

	switch (action.type) {
		case 'REQUEST_PROVINCES':
			return state.set('fetching', true);
			break;
		case 'RECEIVED_PROVINCES':
			state = state
				.set('fetching', false)
				.set('provinces', Immutable.List(payload));
			return state;
		default:
			return state;
			break;
	}

}