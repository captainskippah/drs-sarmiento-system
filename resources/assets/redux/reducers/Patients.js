
import Immutable from 'immutable';

const InitialState = Immutable.Map({
	fetching: false,
	patients: Immutable.Map()
});

export default function Patients(state = InitialState, action){

	let {payload} = action

	switch(action.type){
		case 'REQUEST_PATIENT':
			return state.set('fetching', true);
		case 'RECEIVED_PATIENT':
			if(payload.search){
				if(payload.success){
					payload.results.forEach(function(value, key){
						state = state.setIn(['patients', value.value], Immutable.Map(value))
					});
					return state.set('fetching', false);
				}else{
					return state;
				}
			}
		default: return state;
	}

}