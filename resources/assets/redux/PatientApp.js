import thunkMiddleware from 'redux-thunk'
import {createStore, applyMiddleware} from 'redux';
import allReducers from './reducers/index.js';

import {
	openModalPatientForm,
	closeModalPatientForm,
	FetchPrefixes,
	FetchProvinces,
	FetchCities,
	FetchBarangays,
	FetchPatient
} from './actions/index.js';

const store = createStore(
	allReducers,
	applyMiddleware(
		thunkMiddleware
	)
);

var 
	// Cached common selectors
	$table             = $('#table-patients'),
	$addressOneTooltip = $('#addressOneTooltip'),
	$addressTwoTooltip = $('#addressTwoTooltip'),
	$modalPatientForm  = $('#modal-patient-form'),
	$formPatient       = $('#form-patient'),
	$inputs            = $formPatient.find(':input').not(':input.search').not(':input[type="hidden"]').not(':button'),
	$firstname         = $('#firstname'),
	$middlename        = $('#middlename'),
	$lastname          = $('#lastname'),
	$btnNewPatient     = $('#btn-new-patient'),
	$btnGender         = $('#btn-gender'),
	$btnMale           = $('#btn-male'),
	$btnFemale         = $('#btn-female'),
	$age               = $('#age'),
	$phone             = $('#phone'),
	$line1             = $('#line1'),
	$line2             = $('#line2'),
	$prefixDropdown    = $('#prefixDropdown'),
	$provinceDropdown  = $('#provinceDropdown'),
	$cityDropdown      = $('#cityDropdown'),
	$barangayDropdown  = $('#barangayDropdown');

$btnNewPatient.on('click', () => {
	store.dispatch(openModalPatientForm());
});

$modalPatientForm.find('.close').on('click', () => {
	store.dispatch(closeModalPatientForm());
});

$table.DataTable({
	ajax: '/admin/patients',
	processing: true,
	serverSide: true,
	columns: [
		{
			data: 'id',
			name: 'id'
		},
		{
			data: 'firstname',
			name: 'firstname'
		},
		{
			data: 'middlename',
			name: 'middlename',
			defaultContent: '<i>None</i>'
		},
		{
			data: 'lastname',
			name: 'lastname'
		},
		{
			data: 'age',
			name: 'age'
		},
		{
			data: 'phone',
			name: 'phone',
			orderable: false,
			sortable: false
		},
		{
			data: 'action',
			name: 'action',
			orderable: false,
			searchable: false,
			sortable: false
		}
	]
});

$table.on('click','.button[data-action="edit patient"]', (event) => {
	store.dispatch(openModalPatientForm($(event.currentTarget).data('id')));
});

$table.on('click','.button[data-action="view patient"]', (event) => {
});

$modalPatientForm.modal({
	onShow: () => {
		// Tooltips
		$addressOneTooltip.popup({ inline: true });
		$addressTwoTooltip.popup({ inline: true });

		// Dropdowns
		$prefixDropdown.dropdown();
		$provinceDropdown.dropdown({ fullTextSearch: true });
		$cityDropdown.dropdown({ fullTextSearch: true });
		$barangayDropdown.dropdown({ fullTextSearch: true });
	},
	onHide: () => {
		$addressOneTooltip.popup('destroy');
		$addressTwoTooltip.popup('destroy');
		$prefixDropdown.dropdown('destroy');
		$provinceDropdown.dropdown('destroy');
		$cityDropdown.dropdown('destroy');
		$barangayDropdown.dropdown('destroy');
	}
});

store.subscribe( () => {
	let state = store.getState();
	let {
		Patients,
		Prefixes,
		PatientForm,
		Provinces,
		Cities,
		Barangays
	} = state;

	if(PatientForm.get('open') == true && !$modalPatientForm.modal('is active')){
		$modalPatientForm.modal('show');

		$formPatient
		.form('clear')
		.form('destroy')
		.form({
			fields: {
				firstname:{
					identifier: 'personal[firstname]',
					rules: [
						{
							type: 'empty',
							prompt: 'Please enter the patient\'s first name',
						},
						{
							type:'regExp',
							value: /[A-z ]/,
							prompt: 'First name should contain letters only'
						}
					]
				},
				middlename:{
					identifier: 'personal[middlename]',
					rules: [
						{
							type: 'regExp',
							value: /^$|[A-z ]/,
							prompt: 'Middle name should contain letters only'
						}
					]
				},
				lastname:{
					identifier: 'personal[lastname]',
					rules: [
						{
							type: 'empty',
							prompt: 'Please enter the patient\'s last name'
						},
						{
							type: 'regExp',
							value: /[A-z ]/,
							prompt: 'Last name should contain letters only'
						}
					]
				},
				gender:{
					identifier: 'personal[gender]',
					rules: [{
						type: 'checked',
						prompt: 'Please select the patient\'s gender'
					}]
				},
				age:{
					identifier: 'personal[age]',
					rules: [
						{
							type: 'empty',
							prompt: 'Please enter the patient\'s age'
						},
						{
							type: 'integer',
							prompt: 'Age should contain numbers only'
						}
					]
				},
				phone:{
					identifier: 'personal[phone]',
					rules: [
						{
							type: 'empty',
							prompt: 'Please enter the last 7 digit of patient\'s phone number'
						},
						{
							type: 'regExp',
							value: /[0-9]{7,7}/,
							prompt: 'Please enter the last 7 digit of patient\'s phone number'
						}
					]
				},
				address1:{
					identifier: 'address[line1]',
					rules: [
						{
							type: 'empty',
							prompt: 'Please enter the patient\'s address'
						}
					]
				},
				province:{
					identifier: 'address[province]',
					rules: [
						{
							type: 'empty',
							prompt: 'Please select a province'
						}
					]
				},
				city:{
					identifier: 'address[city]',
					rules: [
						{
							type: 'empty',
							prompt: 'Please select a city'
						}
					]
				},
				barangay:{
					identifier: 'address[barangay]',
					rules: [
						{
							type: 'empty',
							prompt: 'Please specify the patient\'s barangay'
						}
					]
				}
			}
		});
	}

	if(PatientForm.get('open') == false && $modalPatientForm.modal('is active')){
		$modalPatientForm.modal('hide');	
	}

	if(PatientForm.get('open')){

		// Common to both default and not-default

		/*
		*	Dropdown Fetching
		*/

		if(Prefixes.get('prefixes').isEmpty() && !Prefixes.get('fetching')){
			store.dispatch(FetchPrefixes())
		}

		if(Provinces.get('provinces').isEmpty() && !Provinces.get('fetching')){
			store.dispatch(FetchProvinces());
		}

		/*
		*	Dropdown Loading State
		*/

		if(Prefixes.get('fetching')){
			$prefixDropdown.addClass('loading');
		}else{
			$prefixDropdown.removeClass('loading');
		}

		if(Provinces.get('fetching')){
			$provinceDropdown.addClass('loading');
		}else{
			$provinceDropdown.removeClass('loading');
		}

		if(Cities.get('fetching')){
			$cityDropdown.addClass('loading');
		}else{
			$cityDropdown.removeClass('loading');
		}

		if(Barangays.get('fetching')){
			$barangayDropdown.addClass('loading');
		}else{
			$barangayDropdown.removeClass('loading');
		}

		// Profile is not default. Check if needs fetching
		if(PatientForm.get('profileID') !== 0){

			if(!Patients.hasIn(['patients', PatientForm.get('profileID')]) && !Patients.get('fetching')){
				store.dispatch(FetchPatient( PatientForm.get('profileID') ));
			}else{
				// Done fetching patient

				if(!Provinces.get('provinces').isEmpty() && Cities.get('cities').isEmpty() && !Cities.get('fetching')){
					let provinceID = Patients.getIn(['patients', PatientForm.get('profileID')]).address.barangay.city.province.provCode
					store.dispatch(FetchCities(provinceID));
				}

				if(!Cities.get('cities').isEmpty() && Barangays.get('barangays').isEmpty() && !Barangays.get('fetching')){
					let provinceID = Patients.getIn(['patients', PatientForm.get('profileID')]).address.barangay.city.province.provCode
					let cityID = Patients.getIn(['patients', PatientForm.get('profileID')]).address.barangay.city.citymunCode
					store.dispatch(FetchBarangays(provinceID, cityID));
				}

				/*
				*	Load dropdown option and set value
				*/

				if(!Prefixes.get('prefixes').isEmpty()){
					let profileData = Patients.getIn(['patients', PatientForm.get('profileID')]);
					let prefixes = formatResponse(Prefixes.get('prefixes').toArray(), 'prefix', 'prefix');
					
					updateDropdown($prefixDropdown, prefixes);
					$prefixDropdown.dropdown('set selected', profileData.phone.substr(0,4))
				}

				if(!Provinces.get('provinces').isEmpty()){
					let profileData = Patients.getIn(['patients', PatientForm.get('profileID')]);
					let provinces = formatResponse(Provinces.get('provinces').toArray(), 'provDesc', 'provCode');
					
					updateDropdown($provinceDropdown, provinces);
					$provinceDropdown.dropdown('set selected', profileData.addres.barangay.city.province.provCode);
				}

				if(!Cities.get('cities').isEmpty()){
					let profileData = Patients.getIn(['patients', PatientForm.get('profileID')]);
					let cities = formatResponse(Cities.getIn(['cities', profileData.address.barangay.city.citymunCode]), 'citymunDesc', 'citymunCode');
					
					updateDropdown($cityDropdown, cities);
					$cityDropdown.dropdown('set selected', profileData.address.barangay.city.citymunCode);
				}

				if(!Barangays.get('barangays').isEmpty()){
					let profileData = Patients.getIn(['patients', PatientForm.get('profileID')]);
					let barangays = formatResponse(Barangays.getIn(['barangays', profileData.address.barangay.brgyCode]), 'brgyDesc', 'brgyCode');
					
					updateDropdown($barangayDropdown, barangays);
					$barangayDropdown.dropdown('set selected', profileData.address.barangay.brgyCode)
				}
			}


		}else{

			// Default Profile

			if(!Provinces.get('provinces').isEmpty() && Cities.get('cities').isEmpty() && !Cities.get('fetching')){
				let provinceID = PatientForm.get('defaultProvince');
				store.dispatch(FetchCities(provinceID));
			}

			if(!Cities.get('cities').isEmpty() && Barangays.get('barangays').isEmpty() && !Barangays.get('fetching')){
				let provinceID = PatientForm.get('defaultProvince');
				let cityID     = PatientForm.get('defaultCity');
				store.dispatch(FetchBarangays(provinceID, cityID));
			}

			/*
			*	Load dropdown and set value
			*/

			if(!Provinces.get('provinces').isEmpty()){
				let provinces = formatResponse(Provinces.get('provinces').toArray());
				updateDropdown($provinceDropdown, provinces);
				$provinceDropdown.dropdown('set selected', PatientForm.get('defaultProvince'));
			}

			if(!Cities.get('cities').isEmpty()){
				let cities = formatResponse(Cities.getIn(['cities', PatientForm.get('defaultProvince')]), 'citymunDesc', 'citymunCode');
				updateDropdown($cityDropdown, cities);
				$cityDropdown.dropdown('set selected', PatientForm.get('defaultCity'));
			}

			if(!Barangays.get('barangays').isEmpty()){
				let array = Barangays.getIn(['barangays', PatientForm.get('defaultCity')]);
				let barangays = formatResponse(array, 'brgyDesc', 'brgyCode');

				updateDropdown($barangayDropdown, barangays);
			}
		}
	}
});

function formatResponse(response, textKey, valueKey){
	var array = [];
	response.map( function(value, key) {
		array[key] = {
			text: value[textKey],
			value: value[valueKey]
		}
	});

	return array;
}

function updateDropdown(element, array){
	let options = arrayToOptions( array );
	appendOptions(element,options);
}

function arrayToOptions(array){
	let options = array.sort( (a,b) => {
		// Sort alphabetically
		var x = a['text'].toLowerCase(), y = b['text'].toLowerCase();
		return x < y ? -1 : x > y ? 1 : 0;
	}).map( (sortedArray) => {
		return renderOptionComponent(sortedArray['value'], sortedArray['text']);
	}).join('');

	return options;
}

function renderOptionComponent(value, text){
	return '<div class="item" data-value="'+value+'">'+text+'</div>';
}

function appendOptions(element, options){
	element.find('.menu').html(options);
	element.dropdown('refresh');
}