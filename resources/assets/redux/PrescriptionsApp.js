import Sifter from 'sifter';
import thunkMiddleware from 'redux-thunk'
import {createStore, applyMiddleware} from 'redux';
import { PrescriptionReducers } from './reducers/index.js';


import {
	openModalPrescription,
	closeModalPrescription,
	modalPrescriptionNext,
	modalPrescriptionPrev,
	searchTreatment,
	searchPatient,
	addMedicineRow,
	deleteMedicineRow,
	submitPrescription
} from './actions/index.js';

var
	// Cache commonly used selectors
	$btnNewPrescription     = $('#btn-new-prescription'),
	$btnAddMedicine         = $('#btn-add-medicine'),
	$rowMedicineClone       = $('#row-clone'),
	$tableMedicines         = $('#table-medicines'),
	$tablePrescriptions     = $('#table-prescriptions'),
	$modalPrescriptionsForm = $('#modal-prescriptions-form'),
	$formPrescriptions      = $('#form-prescriptions'),
	$cancelButton           = $modalPrescriptionsForm.find('.cancel.button'),
	$nextButton             = $modalPrescriptionsForm.find('.next.button'),
	$prevButton             = $modalPrescriptionsForm.find('.prev.button'),
	$saveButton             = $modalPrescriptionsForm.find('.save.button'),
	
	$patientDropdown        = $('#patientDropdown'),
	$inputCase              = $('#input-case'),
	$treatmentDropdown      = $('#treatmentDropdown'),
	$inputNotes             = $('#input-notes');


const store = createStore(
	PrescriptionReducers,
	applyMiddleware(
		thunkMiddleware
	)
);

var oldState = {
	Prescriptions: Immutable.Map({
		open: false,
		step: 1
	}),
	Treatments: Immutable.Map(),
	Patients: Immutable.Map(),
	Form: Immutable.Map({
		highestRow: 1
	})
}

var patientSifter   = new Sifter([]);
var treatmentSifter = new Sifter([]);

var patientTimer;
var treatmentTimer;

$tablePrescriptions.DataTable({
	ajax: '/admin/prescriptions',
	processing: true,
	serverSide: true,
	columns:[
		{
			data: 'patient.name',
			name: 'patient.name'
		},
		{
			data: 'case',
			name: 'case'
		},
		{
			data: 'treatment.name',
			name: 'treatment.name',
			defaultContent: '<i>None</i>'
		},
		{
			data: 'notes',
			name: 'notes'
		},
		{
			data: 'created_at',
			name: 'created_at'
		},
		{
			data: 'action',
			sortable: false,
			orderable: false,
			searchable: false
		}
	]
});

$patientDropdown.dropdown({
	fullTextSearch: true
});

$treatmentDropdown.dropdown({
	fullTextSearch: true
});

$patientDropdown.on('keypress', '.search', function(e){
	if(e.which !== 0 || e.which !== null){

		var input = $(this);

		clearTimeout(patientTimer);

		patientTimer = setTimeout(function(){

			let results = patientSifter.search(input.val(), {
				fields: ['text'],
				sort: [{field: 'text', direction: 'asc'}]
			});

			if(results.total === 0){
				store.dispatch(searchPatient( input.val() ));
			}

		}, 200)
	}
})

$treatmentDropdown.on('keypress', '.search', function(e){
	if (e.which !== 0 || e.which !== null) {  

		var input = $(this);  

		clearTimeout(treatmentTimer);

		treatmentTimer = setTimeout(function(){
			let results = treatmentSifter.search(input.val(), {
				fields: ['text'],
				sort: [{field: 'text', direction: 'asc'}]
			});

			if(results.total === 0){
				store.dispatch(searchTreatment( input.val() ));
			}
		},200);
	}
})

$btnNewPrescription.on('click', function(){
	store.dispatch(openModalPrescription());
})

$btnAddMedicine.on('click', function(){
	store.dispatch(addMedicineRow());
})

$tableMedicines.on('click', '.delete', function(){
	$(this).closest('tr')
		.remove();
})

$tableMedicines.on('keypress change', '.field :input:not(:button)', function(e){
	if (e.which !== 0 || e.which !== null) {
		let rowID = $(this).closest('tr').data('id');
		setTimeout(function(){
			validateMedicineRow(rowID);
		},250)
	}
})

$nextButton.on('click', function(event){
	event.preventDefault();
		if(oldState.Prescriptions.get('step') < 2){
			if($formPrescriptions.form('is valid')){
				store.dispatch(modalPrescriptionNext());
			}
		}
})

$saveButton.on('click', function(event){
	event.preventDefault();

	validateMedicineTable();

	if(isValid()){
		$formPrescriptions
			.find(':button')
				.prop('disabled', true)
				.end()
			.find(':input').not(':button')
				.prop('readonly', true)
				.trigger('blur');

		$(this).prop('disabled', true);

		showLoader(true);

		$formPrescriptions.ajaxSubmit({
			url: '/admin/prescriptions',
			success: function(response){
				store.dispatch(closeModalPrescription());
			},
			error: function(response){

			},
			complete: function(){
				showLoader(false);
			}
		})
	}
})

$prevButton.on('click', function(){
	if(oldState.Prescriptions.get('step') > 1)
		store.dispatch(modalPrescriptionPrev());
})

$cancelButton.on('click', function(event){
	event.stopPropagation();
	store.dispatch(closeModalPrescription());
})


store.subscribe( () => {

	const state = store.getState();

	const {
		Prescriptions,
		Treatments,
		Patients,
		PrescriptionForm
	} = state;


	if(Prescriptions.get('open') && !oldState.Prescriptions.get('open')){
		oldState.Prescriptions = oldState.Prescriptions.set('open', true);
		$modalPrescriptionsForm.modal('show');

		$formPrescriptions
			.find(':button')
				.removeProp('disabled')
				.end()
			.find(':input').not(':button')
				.removeProp('readonly')

		$saveButton.removeProp('disabled');
	}

	if(!Prescriptions.get('open') && oldState.Prescriptions.get('open')){
		oldState.Prescriptions = oldState.Prescriptions.set('open', false);
		$modalPrescriptionsForm.modal('hide');
	}

	oldState.Prescriptions = oldState.Prescriptions.set('step', Prescriptions.get('step'));

	if(Prescriptions.get('open')){

		if(Prescriptions.get('fetching')){
			showLoader(true);
		}else{
			showLoader(false);
		}

		showStep(Prescriptions.get('step'));

		// All page not 1
		if(Prescriptions.get('step') > 1){
			$prevButton.css('display', 'inline-block');
		}

		// On Page 1
		if(Prescriptions.get('step') == 1){
			$prevButton.css('display', 'none');
			$saveButton.css('display', 'none');
			$nextButton.css('display', 'inline-block');

			$formPrescriptions
			.form('clear')
			.form({
				fields:{
					patient:{
						identifier: 'input-patient',
						rules: [
							{
								type: 'empty',
								prompt: 'Please select a patient for this prescription'
							}
						]
					},
					case:{
						identifier: 'input-case',
						rules:[
							{
								type: 'empty',
								prompt: 'Please enter the case of the patient'
							}
						]
					}
				}
			});
		}

		// Last page.. which is 2
		if(Prescriptions.get('step') == 2){
			$nextButton.css('display', 'none');
			$saveButton.css('display', 'inline-block');
			$formPrescriptions.form('destroy');
		}

		if(!Patients.get('patients').isEmpty()){

			if(!Patients.get('patients').equals(oldState.Patients)){

				let oldOptions = oldState.Patients;
				let newOptions = Patients.get('patients');

				patientSifter = new Sifter(newOptions.toJS());

				// Filter old values;
				let options = newOptions.toArray().filter(function(val){
					return oldOptions.includes(val) == false;
				});

				$patientDropdown.find('.menu .message').remove();

				$patientDropdown.find('.menu').append(
					options.map(function(value, key){
						return makeOption(value.get('text'), value.get('value'));
					})
				);

				$patientDropdown.dropdown('refresh');

				oldState.Patients = Patients.get('patients');
			}

		}

		if(!Treatments.get('treatments').isEmpty()){

			if( !Treatments.get('treatments').equals(oldState.Treatments) ){

				let oldOptions = oldState.Treatments;
				let newOptions = Treatments.get('treatments');

				treatmentSifter = new Sifter(newOptions.toJS());

				// Filter old values.
				let options = newOptions.toArray().filter(function(val){
					return oldOptions.includes(val) == false
				});

				$treatmentDropdown.find('.menu .message').remove();

				$treatmentDropdown.find('.menu').append(
					options.map(function(value,key){
						return makeOption(value.get('text'), value.get('value'));
					})
				);

				$treatmentDropdown.dropdown('refresh');

				oldState.Treatments = Treatments.get('treatments');
			}
		}

		if(Patients.get('fetching')){
			$patientDropdown
				.addClass('loading');
		}else{
			$patientDropdown
				.removeClass('loading');
		}

		if(Treatments.get('fetching')){
			$treatmentDropdown
				.addClass('loading');
				
		}else{
			$treatmentDropdown
				.removeClass('loading');
		}

		if(PrescriptionForm.get('highestRow') !== oldState.Form.get('highestRow')){
			oldState.Form.set('highestRow', PrescriptionForm.get('highestRow'));
			$(makeMedicineRow(PrescriptionForm.get('highestRow'))).insertBefore($btnAddMedicine.closest('tr'));
		}

	}
});

function showStep(activeStep){
	$modalPrescriptionsForm
		.find('.form.step')
		.css('display', 'block')
		.filter(function(){
			return $(this).data('step') !== activeStep
		})
		.css('display', 'none');

	$modalPrescriptionsForm
		.find('.steps .step')
		.addClass('active')
		.filter(function(){
			return $(this).data('step') !== activeStep
		})
		.removeClass('active');	
}

function showLoader(state){

	if(state){
		$modalPrescriptionsForm
			.find('.loader').addClass('active').removeClass('disabled')
			.end().find('#form-prescriptions').css('visibility', 'hidden');
	}else{
		$modalPrescriptionsForm
			.find('.loader').removeClass('active').addClass('disabled')
			.end().find('#form-prescriptions').css('visibility', 'visible');
	}

}

function makeOption(text, value){
	return '<div class="item" data-value="'+value+'">'+text+'</div>';
}

function makeMedicineRow(rowNumber){
	return '<tr data-id="'+rowNumber+'">'+
		'<td>'+
			'<div class="field">'+
				'<input type="text" placeholder="Biogesic" data-validate="medicine-'+rowNumber+'" name="medicine[]">'+
			'</div>'+
		'</td>'+
		'<td>'+
			'<div class="field">'+
				'<input type="number" placeholder="1" data-validate="frequency-'+rowNumber+'" name="frequency[]" min="1">'+
			'</div>'+
		'</td>'+
		'<td>'+
			'<div class="field">'+
				'<input type="number" placeholder="2" data-validate="days-'+rowNumber+'" name="days[]" min="1">'+
			'</div>'+
		'</td>'+
		'<td>'+
			'<div class="field">'+
				'<textarea rows="2" placeholder="Take 2 tablets every 10 AM etc..." data-validate="instruction-'+rowNumber+'" name="instruction[]"></textarea>'+
			'</div>'+
		'</td>'+
		'<td>'+
			'<button class="ui delete button" type="button">Delete</button>'+
		'</td>'+
	'</tr>'
}

function validateMedicineTable(){	
	let rows = $tableMedicines.find('tbody tr[data-id]');

	$.each(rows, function(index, row) {
		let rowID = $(row).data('id');
		validateMedicineRow(rowID);
	});
}

function validateMedicineRow(rowID){
	let row = $tableMedicines.find('tr[data-id="'+rowID+'"]');
	let inputs = row.find(':input').not(':button');
	let required = false;

	$.each(inputs, function(index, input) {
		if($(input).val() !== ''){
			required = true
		}
	});

	if(required){
		$(inputs)
		.closest('.field')
			.removeClass('error')
			.end()
		.filter(function(){
			return $(this).val() == ''
		})
		.closest('.field')
		.addClass('error')
	}else{
		$.each(inputs, function(index, input) {
			 $(input).filter(function(){
			 	return $(this).val() == ''
			 })
			 .closest('.field')
			 .removeClass('error')
		});
	}
}

function isValid(){
	return $formPrescriptions.find('.field.error').length == 0
}