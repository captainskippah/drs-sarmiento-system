import React, { Component } from 'react';
import { render } from 'react-dom';
import { Provider, connect } from 'react-redux';
import { createStore, combineReducers } from 'redux';
import { Field, reduxForm, reducer as formReducer, formValueSelector } from 'redux-form';
import { Griddle } from 'griddle-react';



class NewCategoryButton extends Component{
	constructor(){
		super()
	}

	open(){
		this.props.open();
	}

	render(){
		return(
			<div className="row">
		        <div className="four wide column">
		            <button onClick={this.open.bind(this)} className="ui green button" id="btn-new-category"><i className="plus icon"></i> Add New Category</button>
		        </div>
		    </div>
		)
	}
}

class NewCategoryModal extends Component{
	constructor(){
		super()
	}

	componentDidMount(){
		$(this._newCategoryModal)
			.modal({detachable: false})
			.modal('show');
	}

	componentWillUnmount(){
		$(this._newCategoryModal).modal('hide').modal('destroy');
	}

	close(){
		this.props.close();
	}

	render(){
		return(
			<div className="ui modal" ref={(c) => this._newCategoryModal = c}>
			  <i className="close icon" onClick={this.close.bind(this)}></i>
			  <div className="header">
			    Profile Picture
			  </div>
			  <div className="image content">
			    <div className="ui medium image">
			      <img src="/images/avatar/large/chris.jpg" />
			    </div>
			    <div className="description">
			      <div className="ui header">We've auto-chosen a profile image for you.</div>
			      <p>We've grabbed the following image from the <a href="https://www.gravatar.com" target="_blank">gravatar</a> image associated with your registered e-mail address.</p>
			      <p>Is it okay to use this photo?</p>
			    </div>
			  </div>
			  <div className="actions">
			    <div className="ui black deny button">
			      Nope
			    </div>
			    <div className="ui positive right labeled icon button">
			      Yep, that's me
			      <i className="checkmark icon"></i>
			    </div>
			  </div>
			</div>
		)
	}
}

class CategoryTable extends Component{
	constructor(){
		super()
	}

	render(){
		return null;
	}

}


class CategoryApp extends Component{
	constructor(){
		super()

		this.state = {
			open: false,
		}
	}

	openModal(){
		this.setState((oldState) => {
			return {
				open: !oldState.open
			}
		});
	}



	render(){
		console.log(this.state.open)
		return(
			<div>
				<div id="content-top">
					<NewCategoryButton open={this.openModal.bind(this)} />
				</div>

				{this.state.open ? <NewCategoryModal close={this.openModal.bind(this)} /> : '' }

			</div>
		)
	}
}


render(
	<CategoryApp />,
	document.getElementById('content-main')
);