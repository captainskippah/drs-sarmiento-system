import alt from '../alt.js';
import Immutable from 'immutable';
import immutable from 'alt-utils/lib/ImmutableUtil';
import PrefixActions from '../actions/PrefixActions.js';

class PrefixStore{

	constructor(){

		this.prefixes = Immutable.List();
		this.fetching = false;

		this.bindListeners({
		  handleFetchPrefixes: PrefixActions.fetchPrefixes,
		  handleUpdatePrefixes: PrefixActions.updatePrefixes
		});

	}

	handleFetchPrefixes() {
		this.fetching = true;
	}

	handleUpdatePrefixes(prefixes){
		let array = this.formatResponse(prefixes, 'prefix', 'prefix');
		this.prefixes = Immutable.List(array);
		this.fetching = false;
	}

	formatResponse(response, textKey, valueKey){
		var array = [];
		response.map( function(value, key) {
			array[key] = {
				text: value[textKey],
				value: value[valueKey]
			}
		});

		return array;
	}

}

export default alt.createStore(immutable(PrefixStore), 'PrefixStore');