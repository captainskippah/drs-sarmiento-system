import alt from '../alt.js';
import Immutable from 'immutable';
import immutable from 'alt-utils/lib/ImmutableUtil';

import PatientActions from '../actions/PatientActions.js';
import PatientModalViewActions from '../actions/PatientModalViewActions.js';

import PatientModalFormStore from '../stores/PatientModalFormStore.js';
import PatientModalFormActions from '../actions/PatientModalFormActions.js';


class PatientStore{

	constructor(){

		this.state = Immutable.Map();

		this.bindListeners({
		  handleOnFetchProfile: PatientActions.onFetchProfile
		});

	}

	handleOnFetchProfile(response){
		let object = this.formatResponse(response);

		this.setState((oldState)=>{
    		console.log('patient received. saving in PatientStore')
			return oldState.set(response.id, object);
		});
	}

	formatResponse(response){
		let object = {
			activeProvince: response.address.barangay.city.province.provCode,
			activeCity: response.address.barangay.city.citymunCode,
			activeBarangay: response.address.barangay.brgyCode,

			firstname: response.firstname,
			middlename: response.middlename,
			lastname: response.lastname,

			gender: response.gender.toLowerCase(),
			age: response.age,
			activePrefix: response.phone.substr(0,4),
			phone: response.phone.substr(4),

			line1: response.address.address_1,
			line2: response.address.address_2,

			province: response.address.barangay.city.province.name,
			city: response.address.barangay.city.name,
			barangay: response.address.barangay.name,
		};

		return object;
	}

}

export default alt.createStore(immutable(PatientStore), 'PatientStore');