import alt from '../alt.js';
import Immutable from 'immutable';
import immutable from 'alt-utils/lib/ImmutableUtil';

import PatientStore from './PatientStore.js';
import ProvinceStore from './ProvinceStore.js';

import PatientActions from '../actions/PatientActions.js';
import ProvinceActions from '../actions/ProvinceActions.js';

import PatientModalFormActions from '../actions/PatientModalFormActions.js';

class PatientModalFormStore{

	constructor(){

		this.state = Immutable.Map({

			open: false,
			profileData: {},
			profileID: 0,

			provinces: [],
			cities: [],
			barangays: []
		});

		this.bindListeners({
			handleOpenModal: PatientModalFormActions.openModal,
			handleOpenModalEdit: PatientModalFormActions.openModalEdit,
			handleCloseModal: PatientModalFormActions.closeModal,

			handleSetProfile: PatientModalFormActions.setProfile,
			handleSetProvinces: PatientModalFormActions.setProvinces
		});
	}

	handleOpenModalEdit(patientID){

		this.setState( (oldState) => {
			return oldState
				.set('open', true)
				.set('profileID', patientID)
		});

	}

	handleSetProfile(profileData){
		this.setState( (oldState) => {
			return oldState
				.set('profileData', profileData)
		})
	}

	handleSetProvinces(provinces){
		this.setState( (oldState) => {
			return oldState
				.set('provinces', provinces)
		})
	}

	handleOpenModal(patientID = 0){

		var loadProfile = patientID == 0 ? false : true;
		var profileData = false;

		if(patientID !== 0){

			profileData = this._getFromPatientStore(patientID);
			loadProfile = profileData ? false : true;

			this.setState((oldState) => {
				return oldState.set('profileID', patientID);
			})
		}

		this.setState((oldState) => {
			return oldState
				.set('open', true)
		});
	}

	_getFromPatientStore(patientID){
		return PatientStore.getState().getIn(['patients', patientID]);
	}

	handleCloseModal(){
		this.setState((oldState) => {
			return oldState
				.set('open', false);
		});
	}

}

export default alt.createStore(immutable(PatientModalFormStore), 'AllStoresA');