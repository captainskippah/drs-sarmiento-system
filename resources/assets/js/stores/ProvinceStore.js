import alt from '../alt.js';
import Immutable from 'immutable';
import immutable from 'alt-utils/lib/ImmutableUtil';

import PatientStore from './PatientStore.js';
import PatientModalFormStore from './PatientModalFormStore.js';

import ProvinceActions from '../actions/ProvinceActions.js';
import PatientActions from '../actions/PatientActions.js';
import PatientModalFormActions from '../actions/PatientModalFormActions.js';

class ProvinceStore{

	constructor(){

		this.state = Immutable.List();

		this.bindListeners({
		  handleOnFetchProvinces: ProvinceActions.onFetchProvinces
		});

	}

	handleOnFetchProvinces(provinces){
		console.log('provinces received. saving in ProvinceStore.')
		let array = this.formatResponse(provinces, 'provDesc', 'provCode');
		this.state = Immutable.List(array);
	}

	formatResponse(response, textKey, valueKey){
		var array = [];
		response.map( function(value, key) {
			array[key] = {
				text: value[textKey],
				value: value[valueKey]
			}
		});

		return array;
	}
}

export default alt.createStore(immutable(ProvinceStore), 'ProvinceStore');