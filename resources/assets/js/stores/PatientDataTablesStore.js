import alt from '../alt.js';
import Immutable from 'immutable';
import immutable from 'alt-utils/lib/ImmutableUtil';
import PatientDataTablesActions from '../actions/PatientDataTablesActions.js';

class PatientDataTablesStore{

	constructor(){

		this.state = Immutable.Map({
			openForm: false,
			openView: false
		});

		this.bindListeners({
		  handleOpenForm: PatientDataTablesActions.openForm,
		  handleOpenView: PatientDataTablesActions.openView
		});

	}

	handleOpenForm(state){
		this.state = this.state.set('openForm', state);
	}

	handleOpenView(state){
		this.state = this.state.set('openView', state);
	}


}

export default alt.createStore(immutable(PatientDataTablesStore), 'PatientDataTablesStore');