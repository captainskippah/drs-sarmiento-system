import alt from '../alt.js';
import Immutable from 'immutable';
import immutable from 'alt-utils/lib/ImmutableUtil';
import BarangayActions from '../actions/BarangayActions.js';

class BarangayStore{

	constructor(){

		this.barangays = Immutable.Map();
		this.cityID = 0;
		this.fetching = false;

		this.bindListeners({
		  handleFetchBarangays: BarangayActions.fetchBarangays,
		  handleUpdateBarangays: BarangayActions.updateBarangays
		});

	}

	handleFetchBarangays(cityID) {
		this.cityID = cityID;
		this.fetching = true;
	}

	handleUpdateBarangays(barangays){
		let array = this.formatResponse(barangays, 'brgyDesc', 'brgyCode');
		this.barangays = this.barangays.set(this.cityID, array);
		this.fetching = false;
	}

	formatResponse(response, textKey, valueKey){
		var array = [];
		response.map( function(value, key) {
			array[key] = {
				text: value[textKey],
				value: value[valueKey]
			}
		});

		return array;
	}

}

export default alt.createStore(immutable(BarangayStore), 'AllStores');