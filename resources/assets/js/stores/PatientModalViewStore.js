import alt from '../alt.js';
import Immutable from 'immutable';
import immutable from 'alt-utils/lib/ImmutableUtil';

import PatientStore from './PatientStore.js';
import PatientModalViewActions from '../actions/PatientModalViewActions.js';

class PatientModalViewStore{

	constructor(){

		this.state = Immutable.Map({
			open: false,
			fetching: false,
			profileData: false,
			profileID: 0,
		});

		this.bindListeners({
			handleOpenModal: PatientModalViewActions.openModal,
			handleCloseModal: PatientModalViewActions.closeModal,
			handleFetchProfile: PatientModalViewActions.fetchProfile,
			handleOnFetchProfile: PatientModalViewActions.onFetchProfile
		});

	}

	handleOpenModal(patientID = 0){
		var loadProfile = patientID == 0 ? false : true;
		var profileData = false;

		if(loadProfile == true){

			profileData = this._getFromPatientStore(patientID);
			loadProfile = profileData ? false : true;

			this.setState((oldState) => {
				return oldState.set('profileID', patientID);
			})
		}

		this.setState((oldState) => {
			return oldState
				.set('open', true)
				.set('profileData', profileData)
				.set('fetching', loadProfile); // while dispatch while fetching doesn't work... base state here
		});
	}

	handleCloseModal(){
		this.setState((oldState) => {
			return oldState
				.set('open', false);
		});
	}

	_getFromPatientStore(patientID){
		return PatientStore.getState().getIn(['patients', patientID]);
	}

	/*
	*	Supposed to be handling the loading state
	*	Left until dispatching from here works
	*/
	handleFetchProfile(){
		this.setState((oldState) => {
			return oldState.set('fetching', true);
		});
	}

	handleOnFetchProfile(patient){
		this.setState((oldState) => {
			return oldState
				.set('fetching', false)
				.set('profileData', this._getFromPatientStore(this.state.get('profileID')));
		});
	}

}

export default alt.createStore(immutable(PatientModalViewStore), 'PatientModalViewStore');