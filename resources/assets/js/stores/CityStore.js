import alt from '../alt.js';
import Immutable from 'immutable';
import immutable from 'alt-utils/lib/ImmutableUtil';

import CityActions from '../actions/CityActions.js';
import ProvinceActions from '../actions/ProvinceActions.js';
import PatientModalFormActions from '../actions/PatientModalFormActions.js';


class CityStore{

	constructor(){

		this.state = Immutable.Map({
			cities: Immutable.List(),
			fetching: false,
			active: false,
			province: 354
		});

		this.bindListeners({
			//handleOnCloseModal: PatientModalFormActions.closeModal,

			//handleOpenModalEdit: PatientModalFormActions.openModalEdit,
			handleOnFetchCitiesOf: CityActions.onfetchCitiesOf,

		});

	}

	handleOpenModalEdit(){
		if (this.state.get('provinces').isEmpty()){
			console.log('Province is empty anyway. Do not wait for modalForm data. update now. - ProvinceStore');

			this.setState((oldState) => {
				return oldState.set('fetching', true);
			})

			return true;
		}
		console.log('Wait for modalForm to update before getting profileData - ProvinceStore');
		this.waitFor(PatientModalFormStore.dispatchToken);

		if (!PatientModalFormStore.getState().hasIn(['profileData', 'activeProvince'])){
			console.log('patient not in store. Wait for fetching - ProvinceStore');
			return false;
		}

		console.log('patient in store. No waiting - Province Store');
		console.log(PatientModalFormStore.getState().toObject())
	}

	handleOnCloseModal(){
		this.setState((oldState) => {
			return oldState.set('active', 35401);
		})
	}

	handleOnFetchCitiesOf(cities){
		this.setState((oldState) => {
			return oldState
				.set('cities', Immutable.List(array))
				.set('fetching', false);
		});
	}

	formatResponse(response, textKey, valueKey){
		var array = [];
		response.map( function(value, key) {
			array[key] = {
				text: value[textKey],
				value: value[valueKey]
			}
		});

		return array;
	}
}

export default alt.createStore(immutable(CityStore), 'AllStores');