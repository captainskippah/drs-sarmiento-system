
class CitiesSource{

	fetch(provinceID){
		return $.ajax({
	    	url: '/admin/ajax/province/'+provinceID+'/city'
	    })
	}
	
}

export default new CitiesSource()