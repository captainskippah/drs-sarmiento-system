
class PatientSource{

	fetch(patientID){
		return $.ajax({
	    	url: '/admin/patients/'+patientID
	    })
	}
	
}

export default new PatientSource()