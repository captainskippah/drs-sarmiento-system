import PatientModalViewActions from '../actions/PatientModalViewActions.js';
import PatientModalViewStore from '../stores/PatientModalViewStore.js';

import PatientActions from '../actions/PatientActions.js';
import PatientStore from '../stores/PatientStore.js';

var
	// Cache common selectors
	$modalPatientView = $('#modal-patient-view'),
	$patientName      = $('#patient-name'),
	$patientGender    = $('#patient-gender'),
	$patientAge       = $('#patient-age'),
	$patientPhone     = $('#patient-phone'),
	$patientAddress   = $('#patient-address'),
	$patientBarangay  = $('#patient-barangay'),
	$patientCity      = $('#patient-city'),
	$patientProvince  = $('#patient-province');

class PatientModalView{

	constructor(){

		this.state = Immutable.Map({
			profileID: 0,
			patients: PatientStore.getState().patients
		});

		PatientModalViewStore.listen(this.onPatientModalViewStoreChange.bind(this));
		
		/*
		*	Initialize Stuffs
		*/

		$modalPatientView.modal();

		// Closes modal form
		$modalPatientView.find('.close').on('click', function(event) {
			PatientModalViewActions.closeModal();
		});
	}

	onPatientModalViewStoreChange(state){
		if (state.get('open') == true && !$modalPatientView.modal('is active')){
			$modalPatientView.modal('show');
		}

		if (state.get('open') == false && $modalPatientView.modal('is active')){
			$modalPatientView.modal('hide');
		}

		if (state.get('open') == true){
			this.setProfile( state.get('profileData') );

			if ( !state.get('profileData') ){
				PatientModalViewActions.fetchProfile(state.get('profileID'));
			}
			this.setLoading( state.get('fetching') );
		}
	}

	setProfile(profileData){

		if (!profileData){
			return false;
		}

		$patientName.text(profileData.firstname+' '+profileData.middlename+' '+profileData.lastname);
		$patientGender.text(profileData.gender);
		$patientAge.text(profileData.gender);
		$patientPhone.text(profileData.phone);

		if(profileData.line2){
			$patientAddress.html('<p>'+profileData.line1+'</p>'+'<p>'+profileData.line2+'</p>');
		} else{
			$patientAddress.text(profileData.line1);
		}

		$patientBarangay.text(profileData.barangay);
		$patientCity.text(profileData.city);
		$patientProvince.text(profileData.province);
	}

	setLoading(state){
		if(state == true){
			$modalPatientView
			.find('.loader').addClass('active').removeClass('disabled')
			.end().find('#patient-profile').css('visibility', 'hidden');
		}
		
		if(state == false){
			$modalPatientView
			.find('.loader').removeClass('active').addClass('disabled')
			.end().find('#patient-profile').css('visibility', 'visible');
		}
	}

}

export default new PatientModalView();