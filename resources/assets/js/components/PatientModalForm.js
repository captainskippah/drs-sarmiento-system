import alt from '../alt.js';

import PatientActions from '../actions/PatientActions.js';
import PatientStore from '../stores/PatientStore.js';

import PrefixActions from '../actions/PrefixActions.js';
import PrefixStore from '../stores/PrefixStore.js';

import ProvinceActions from '../actions/ProvinceActions.js';
import ProvinceStore from '../stores/ProvinceStore.js';

import CityActions from '../actions/CityActions.js';
import CityStore from '../stores/CityStore.js';

import BarangayActions from '../actions/BarangayActions.js';
import BarangayStore from '../stores/BarangayStore.js';

import PatientModalFormActions from '../actions/PatientModalFormActions.js';
import PatientModalFormStore from '../stores/PatientModalFormStore.js';

var 
	// Cached common selectors
	$addressOneTooltip = $('#addressOneTooltip'),
	$addressTwoTooltip = $('#addressTwoTooltip'),
	$modalPatientForm  = $('#modal-patient-form'),
	$formPatient       = $('#form-patient'),
	$inputs            = $formPatient.find(':input').not(':input.search').not(':input[type="hidden"]').not(':button'),
	$firstname         = $('#firstname'),
	$middlename        = $('#middlename'),
	$lastname          = $('#lastname'),
	$btnNewPatient     = $('#btn-new-patient'),
	$btnGender         = $('#btn-gender'),
	$btnMale           = $('#btn-male'),
	$btnFemale         = $('#btn-female'),
	$age               = $('#age'),
	$phone             = $('#phone'),
	$line1             = $('#line1'),
	$line2             = $('#line2'),
	$prefixDropdown    = $('#prefixDropdown'),
	$provinceDropdown  = $('#provinceDropdown'),
	$cityDropdown      = $('#cityDropdown'),
	$barangayDropdown  = $('#barangayDropdown');


class PatientModalForm{

	constructor(){

		this.prefixes  = PrefixStore.getState().prefixes;
		this.provinces = ProvinceStore.getState().provinces;
		this.cities    = CityStore.getState().cities;
		this.barangays = BarangayStore.getState().barangays;

		this.state = Immutable.Map({
			activePrefix: '0905',
			activeProvince: '354',
			activeCity: '35401',
			activeBarangay: '',
		});

		/*
		*	The Module's Stores
		*/

		PrefixStore.listen(this.onPrefixStoreChange.bind(this));;

		ProvinceStore.listen(this.onProvinceStoreChange.bind(this));
		
		CityStore.listen(this.onCityStoreChange.bind(this));
		
		BarangayStore.listen(this.onBarangayStoreChange.bind(this));

		PatientModalFormStore.listen(this.onPatientModalFormStoreChange.bind(this));

		PatientStore.listen(this.onPatientStoreChange.bind(this));

		//////////////////////////
		//						//
		//	    DOM STUFFS 		//
		//						//
		//////////////////////////

			/*
			*	Initialize Plugins
			*/

			$modalPatientForm.modal({
				onShow: this.onModalShow.bind(this),
				onHide: this.onModalHide.bind(this)
			});


			/*
			*	Event Listener Section
			*/

			// Closes modal form
			$modalPatientForm.find('.close').on('click', function(event) {
				PatientModalFormActions.closeModal();
			});

			$btnNewPatient.on('click', function(event) {
				//PatientModalFormActions.openModal();
				console.log(alt.stores.AllStores);
				console.log(alt.stores.AllStores.getState());
			});

			// Set Active Class on Gender Button
			$btnGender.on('click keydown reset', '.button', function(event) {
				if (event.type == 'keydown'){
					if (event.which === 13 || event.which === 32){
						$(this).trigger('click');
						event.preventDefault();
					}
				}

				if(event.type == 'click'){
					$(this).addClass('active').siblings('.button').removeClass('active');
				}

				if(event.type == 'reset'){
					$(this).removeClass('active');
				}
			});

			// Dropdown Events. Replace options using array provided. //
			$provinceDropdown.on('update', this.updateDropdown.bind(this));
			$cityDropdown.on('update', this.updateDropdown.bind(this));
			$barangayDropdown.on('update', this.updateDropdown.bind(this));
			$prefixDropdown.on('update', this.updateDropdown.bind(this));

	} // END OF CONSTRUCTOR

	/*
	*	Methods Section
	*/

	setLoading(value){
		if(value == true){
			$modalPatientForm
			.find('.loader').addClass('active').removeClass('disabled')
			.end().find('#form-patient').css('visibility', 'hidden');
		}

		if(value == false){
			$modalPatientForm
			.find('.loader').removeClass('active').addClass('disabled')
			.end().find('#form-patient').css('visibility', 'visible');
		}
	}

	updateDropdown(element, array){
		let options = this.arrayToOptions( array );
		this.appendOptions(element,options);
	}

	arrayToOptions(array){
		let options = array.sort( (a,b) => {
			// Sort alphabetically
			var x = a['text'].toLowerCase(), y = b['text'].toLowerCase();
			return x < y ? -1 : x > y ? 1 : 0;
		}).map( (sortedArray) => {
			return this.renderOptionComponent(sortedArray['value'], sortedArray['text']);
		}).join('');

		return options;
	}

	renderOptionComponent(value, text){
		return '<div class="item" data-value="'+value+'">'+text+'</div>';
	}

	appendOptions(element, options){
		element.find('.menu').html(options);
		element.dropdown('refresh');
	}

	onModalShow(){
		// Tooltips
		$addressOneTooltip.popup({ inline: true });
		$addressTwoTooltip.popup({ inline: true });

		// Dropdowns
		$prefixDropdown.dropdown();
		$provinceDropdown.dropdown({ fullTextSearch: true });
		$cityDropdown.dropdown({ fullTextSearch: true });
		$barangayDropdown.dropdown({ fullTextSearch: true });
	}

	onModalHide(){
		$addressOneTooltip.popup('destroy');
		$addressTwoTooltip.popup('destroy');
		$prefixDropdown.dropdown('destroy');
		$provinceDropdown.dropdown('destroy');
		$cityDropdown.dropdown('destroy');
		$barangayDropdown.dropdown('destroy');
		$formPatient.form('clear');
		$btnMale.trigger('reset');
		$btnFemale.trigger('reset');
	}

	/*
	*	Store Changes Listener
	*/

	onPrefixStoreChange(state){
		console.log(state);
	}



	setDropdownLoading(element, state){
		if (state == true){
			element.addClass('loading disabled');
		}else{
			element.removeClass('loading disabled');
		}
	}

	onCityStoreChange(state){
		this.setDropdownLoading( $cityDropdown, state.get('fetching') );

		if (  state.get('fetching') == true ){
			CityActions.fetchCitiesOf( state.get('provinceID') );
		}else{
			this.updateDropdown($cityDropdown, state.get('cities').toArray());
			$cityDropdown.dropdown('set selected', state.get('active'));
		}
	}

	onBarangayStoreChange(state){
		console.log(state);
	}

	/*
	*	Main State for this component.
	*	Use for re-rendering parts only.
	*	Handles what to do with the data.
	*/
	onPatientModalFormStoreChange(state){
		if (state.get('open') == true && !$modalPatientForm.modal('is active')){
			console.log('modalForm open')
			$modalPatientForm.modal('show');
		}

		if (state.get('open') == false && $modalPatientForm.modal('is active')){
			console.log('modalForm close')
			$modalPatientForm.modal('hide');
		}
		if (!$.isEmptyObject(state.get('profileData'))){
			console.log('profileData has value.')
			console.log(state.get('provinces'))
			if ( state.get('provinces').length == 0 ){
				console.log('province store is empty. I will fetch provinces')
				ProvinceActions.fetchProvinces();
				this.setDropdownLoading( $provinceDropdown, true);
				return false;
			} else{
				console.log('Update province dropdown values')
				this.updateDropdown($provinceDropdown,
				PatientModalFormStore
						.getState()
						.get('provinces')
				);
				this.setDropdownLoading( $provinceDropdown, false);
			}

			this.setFormValues( state.get('profileData') );
			this.setLoading( false );
		} else{
			console.log('profileData is empty. I will fetch patient')
			PatientActions.fetchProfile( state.get('profileID') )
			this.setLoading( true );
		}
	}

	onPatientStoreChange(state){
		console.log('I fetched a new Patient. I will pass it to the state');
		let profileID   = PatientModalFormStore.getState().get('profileID');
		let profileData = state.get(profileID);
		PatientModalFormActions.setProfile( profileData )
	}

	onProvinceStoreChange(state){
		console.log('I fetched provinces. I will pass it to the state');
		PatientModalFormActions.setProvinces(state.toArray())
	}

	setFormValues(profileData){
		if (!profileData){
			return false;
		}

		$formPatient.form('set values', {
			'firstname': profileData.firstname,
			'middlename': profileData.middlename,
			'lastname': profileData.lastname,

			'age': profileData.age,
			'phone': profileData.phone,

			'line1': profileData.line1,
			'line2': profileData.line2
		});

		switch (profileData.gender) {
			case 'male':
				$btnMale.trigger('click');
				break;
			case 'female':
				$btnFemale.trigger('click');
				break;
		}

		$provinceDropdown
			.dropdown('set selected', profileData.activeProvince);

		$cityDropdown
			.dropdown('set selected', profileData.activeCity);

		$barangayDropdown
			.dropdown('set selected', profileData.activeBarangay);
	}


}

export default new PatientModalForm();