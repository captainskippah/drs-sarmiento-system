import PatientDataTableActions from '../actions/PatientDataTablesActions.js';
import PatientDataTablesStore from '../stores/PatientDataTablesStore.js';

import PatientModalViewActions from '../actions/PatientModalViewActions.js';
import PatientModalViewStore from '../stores/PatientModalViewStore.js';

import PatientModalFormActions from '../actions/PatientModalFormActions.js';
import PatientModalFormStore from '../stores/PatientModalFormStore.js';

import PatientStore from '../stores/PatientStore.js';
import PatientActions from '../actions/PatientActions.js';

var
	// Cache commonly used selectors
	$table = $('#table-patients');

class PatientDataTables{

	constructor(){
		
		//////////////////////////
		//						//
		//	    DOM STUFFS 		//
		//						//
		//////////////////////////

		$table.DataTable({
			ajax: '/admin/patients',
			processing: true,
			serverSide: true,
			columns: [
				{
					data: 'id',
					name: 'id'
				},
				{
					data: 'firstname',
					name: 'firstname'
				},
				{
					data: 'middlename',
					name: 'middlename',
					defaultContent: '<i>None</i>'
				},
				{
					data: 'lastname',
					name: 'lastname'
				},
				{
					data: 'age',
					name: 'age'
				},
				{
					data: 'phone',
					name: 'phone',
					orderable: false,
					sortable: false
				},
				{
					data: 'action',
					name: 'action',
					orderable: false,
					searchable: false,
					sortable: false
				}
			]
		});

		/*
		*	Event Listeners
		*/

		$table.on('click','.button[data-action="edit patient"]', this.onEditClick.bind(this) );
		$table.on('click','.button[data-action="view patient"]', this.onViewClick.bind(this) )
	}

	onCreateClick( e, dt, node, config ){

	}

	onViewClick(event){
		var patientID = $(event.currentTarget).data('id');
		PatientModalViewActions.openModal(patientID);
	}

	onEditClick(event){
		let patientID = $(event.currentTarget).data('id');
		PatientModalFormActions.openModalEdit(patientID);
	}

}

export default new PatientDataTables();