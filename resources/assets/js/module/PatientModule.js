import Immutable from 'immutable';

import PatientStore from '../stores/PatientStore.js';
import PatientActions from '../actions/PatientActions.js';

export default class PatientModule{

	constructor(patientData){
		this.patients = patientData ? patientData : PatientStore.getState().patients;

		PatientStore.listen(this.onPatientStoreChange.bind(this));
	}

	onPatientStoreChange(state){
		this.patients = state.patients;
	}

}
