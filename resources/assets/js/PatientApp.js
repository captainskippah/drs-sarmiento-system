import PatientModalForm from './components/PatientModalForm.js';
import PatientModalView from './components/PatientModalView.js';
import PatientDataTables from './components/PatientDataTables.js';

// Patient's API
$.fn.api.settings.api['create patient']   = '/admin/patients';
$.fn.api.settings.api['edit patient']   = '/admin/patients/{id}';
$.fn.api.settings.api['view patient']   = '/admin/patients/{id}';
$.fn.api.settings.api['delete patient'] = '/admin/patients/{id}';