import alt from '../alt.js';

class PrefixActions {

  fetchPrefixes(){

    $.ajax({
      
      url: '/admin/ajax/prefix',
      success: this.updatePrefixes,
      error: () => {
        console.error('Fetching prefixes failed. Please API response.');
      }

    });

    return true; 
  }

  updatePrefixes(prefixes){
    return prefixes;
  }

}

export default alt.createActions(PrefixActions);