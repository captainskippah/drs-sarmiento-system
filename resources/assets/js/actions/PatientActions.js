import alt from '../alt.js';
import PatientSource from '../sources/PatientSource.js';

class PatientActions {

  fetchProfile(patientID){

    return(dispatch) => {
      PatientSource.fetch(patientID).then( (patient) => {
        this.onFetchProfile(patient)
      })
    }

  }

  onFetchProfile(patient){
    return patient;
  }

}

export default alt.createActions(PatientActions);