import alt from '../alt.js';
import PatientSource from '../sources/PatientSource.js';

class PatientModalViewActions{

	fetchProfile(patientID){

		return(dispatch) => {
			PatientSource.fetch(patientID).done( (patient) => {
				this.onFetchProfile(patient)
			})
		}

	}

	onFetchProfile(patient){
		return patient;
	}

	openModal(patientID){
		return patientID;
	}

	closeModal(){
		return(dispatch) => {
			dispatch();
		}
	}

}

export default alt.createActions(PatientModalViewActions);