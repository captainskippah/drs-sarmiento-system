import alt from '../alt.js';
import ProvincesSource from '../sources/ProvincesSource.js';

class ProvinceActions {

    fetchProvinces(){

        return(dispatch) => {
            ProvincesSource.fetch().done( (provinces) => {
                this.onFetchProvinces(provinces);
            })
        }

    }

    onFetchProvinces(provinces){
        return provinces;
    }

    provinceSelect(provinceID){
        return provinceID;
    }
}

export default alt.createActions(ProvinceActions);