import alt from '../alt.js';
import ProvincesSource from '../sources/ProvincesSource.js';

class CityActions {

  fetchCitiesOf( provinceID ){

   return(dispatch) => {
        CitiesSource.fetch(provinceID).done( (cities) => {
            this.onFetchCities(cities);
        })
    }

  }

  onfetchCitiesOf(cities){
    return cities;
  }

}

export default alt.createActions(CityActions);