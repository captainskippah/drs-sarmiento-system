import alt from '../alt.js';
import PatientSource from '../sources/PatientSource.js';

class PatientModalFormActions{

	openModal(){
		return true;
	}

	openModalEdit(patientID){
		return patientID;
	}

	closeModal(){
		return (dispatch) => {
			dispatch();
		}
	}

	provinceSelect(provinceID){
        return provinceID;
    }

    setProfile(profileData){
    	return profileData;
    }

    setProvinces(provinces){
    	return provinces;
    }
}

export default alt.createActions(PatientModalFormActions);