import alt from '../alt.js';

class BarangayActions {

  fetchBarangays( provinceID, cityID ){

    $.ajax({

      url: '/admin/ajax/'+provinceID+'/city/'+cityID+'/barangay/',
      success: this.updateBarangays,
      error: () => {
        console.error('Fetching barangays failed. Please check API response.');
      }

    });

    return cityID;
  }

  updateBarangays(barangays){
    return barangays;
  }

}

export default alt.createActions(BarangayActions);