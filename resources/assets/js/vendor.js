$(document).ready(function() {

	// Patient's API
	$.fn.api.settings.api['create patient']   = '/admin/patients';
	$.fn.api.settings.api['edit patient']   = '/admin/patients/{id}';
	$.fn.api.settings.api['view patient']   = '/admin/patients/{id}';
	$.fn.api.settings.api['delete patient'] = '/admin/patients/{id}';

	var 
		// Cache common selectors
		$addressOneTooltip = $('#addressOneTooltip'),
		$addressTwoTooltip = $('#addressTwoTooltip'),
		$modalPatientForm  = $('#modal-patient-form'),
		$modalPatientView  = $('#modal-patient-view'),
		$formPatient       = $('#form-patient'),
		$inputs            = $formPatient.find(':input').not(':input.search').not(':input[type="hidden"]').not(':button'),
		$firstname         = $('#firstname'),
		$middlename        = $('#middlename'),
		$lastname          = $('#lastname'),
		$btnGender         = $('#btn-gender'),
		$btnMale           = $('#btn-male'),
		$btnFemale         = $('#btn-female'),
		$age               = $('#age'),
		$phone             = $('#phone'),
		$line1             = $('#line1'),
		$line2             = $('#line2'),
		$prefixDropdown    = $('#prefixDropdown'),
		$provinceDropdown  = $('#provinceDropdown'),
		$cityDropdown      = $('#cityDropdown'),
		$barangayDropdown  = $('#barangayDropdown'),
		$table             = $('#table-patients'),
		
		$patientName       = $('#patient-name'),
		$patientGender     = $('#patient-gender'),
		$patientAge        = $('#patient-age'),
		$patientPhone      = $('#patient-phone'),
		$patientAddress    = $('#patient-address'),
		$patientBarangay   = $('#patient-barangay'),
		$patientCity       = $('#patient-city'),
		$patientProvince   = $('#patient-province'),
		
		// Stores
		provinceStore      = Immutable.List(),
		cityStore          = Immutable.Map(),
		barangayStore      = Immutable.Map(),
		prefixStore        = Immutable.List(),
		patientStore       = Immutable.Map(),

		state = Immutable.Map({
			lazyLoad: 'false',
			activePrefix: '0905',
			activeProvince: '354',
			activeCity: '35401',
			activeBarangay: ''
		}),

		formRules = Immutable.Map({
			fields: {
				firstname:{
					identifier: 'personal[firstname]',
					rules: [
						{
							type: 'empty',
							prompt: 'Please enter the patient\'s first name',
						},
						{
							type:'regExp',
							value: /[A-z ]/,
							prompt: 'First name should contain letters only'
						}
					]
				},
				middlename:{
					identifier: 'personal[middlename]',
					rules: [
						{
							type: 'regExp',
							value: /^$|[A-z ]/,
							prompt: 'Middle name should contain letters only'
						}
					]
				},
				lastname:{
					identifier: 'personal[lastname]',
					rules: [
						{
							type: 'empty',
							prompt: 'Please enter the patient\'s last name'
						},
						{
							type: 'regExp',
							value: /[A-z ]/,
							prompt: 'Last name should contain letters only'
						}
					]
				},
				gender:{
					identifier: 'personal[gender]',
					rules: [{
						type: 'checked',
						prompt: 'Please select the patient\'s gender'
					}]
				},
				age:{
					identifier: 'personal[age]',
					rules: [
						{
							type: 'empty',
							prompt: 'Please enter the patient\'s age'
						},
						{
							type: 'integer',
							prompt: 'Age should contain numbers only'
						}
					]
				},
				phone:{
					identifier: 'personal[phone]',
					rules: [
						{
							type: 'empty',
							prompt: 'Please enter the last 7 digit of patient\'s phone number'
						},
						{
							type: 'regExp',
							value: /[0-9]{7,7}/,
							prompt: 'Please enter the last 7 digit of patient\'s phone number'
						}
					]
				},
				address1:{
					identifier: 'address[line1]',
					rules: [
						{
							type: 'empty',
							prompt: 'Please enter the patient\'s address'
						}
					]
				},
				province:{
					identifier: 'address[province]',
					rules: [
						{
							type: 'empty',
							prompt: 'Please select a province'
						}
					]
				},
				city:{
					identifier: 'address[city]',
					rules: [
						{
							type: 'empty',
							prompt: 'Please select a city'
						}
					]
				},
				barangay:{
					identifier: 'address[barangay]',
					rules: [
						{
							type: 'empty',
							prompt: 'Please specify the patient\'s barangay'
						}
					]
				}
			}
		});

	/*
	*	Methods
	*/

	function setPatientProfile(values){
		$patientName.text(values.firstname+' '+values.middlename+' '+values.lastname);
		$patientGender.text(values.gender);
		$patientAge.text(values.gender);
		$patientPhone.text(values.phone);

		if(values.line2){
			$patientAddress.html('<p>'+values.line1+'</p>'+'<p>'+values.line2+'</p>');
		} else{
			$patientAddress.text(values.address_1);
		}

		$patientBarangay.text(values.address.barangay.name);
		$patientCity.text(values.address.barangay.city.name);
		$patientProvince.text(values.address.barangay.city.province.name);
	}

	function setFormValues(values){

		$formPatient.form('set values', {
			'firstname': values.firstname,
			'middlename': values.middlename,
			'lastname': values.lastname,

			'age': values.age,
			'phone': values.phone,

			'line1': values.line1,
			'line2': values.line2
		});

		switch (values.gender) {
			case 'male':
				$btnMale.trigger('click');
				break;
			case 'female':
				$btnFemale.trigger('click');
				break;
			default:
				$btnMale.trigger('reset');
				$btnFemale.trigger('reset');
				break;
		}

		/*
		*	To set Dropdown values, just set the state and trigger the fetching.
		*	Fetching will be cancelled when data is on Store and will do the normal routine.
		*/

		state = state
		.set('activePrefix', values.activePrefix)
		.set('activeProvince', values.activeProvince)
		.set('activeCity', values.activeCity)
		.set('activeBarangay', values.activeBarangay);

		if (state.get('lazyLoad') == 'true'){
			fetchPrefixes();
			fetchProvinces();
		}
			
		return this;
	}

	function openModalPatientForm(){
		$modalPatientForm.modal('show');
	}

	function closeModalForm(){
		$modalPatientForm.modal('hide');
	}

	function openModalPatientView(){
		$modalPatientView.modal('show');
	}

	function closeModalPatientView(){
		$modalPatientView.modal('hide');
	}

	function setLoading(value){
		if(value == 'true'){
			$modalPatientForm
			.find('.loader').addClass('active').removeClass('disabled')
			.end().find('#form-patient').css('visibility', 'hidden');
		}
		else{
			$modalPatientForm
			.find('.loader').removeClass('active').addClass('disabled')
			.end().find('#form-patient').css('visibility', 'visible');
		}
	}

	function setLazyLoad(value){
		state = state.set('lazyLoad', value);
	}


	function formatResponse(response, textKey, valueKey){
		var array = [];
		response.map( function(value, key) {
			array[key] = {
				text: value[textKey],
				value: value[valueKey]
			}
		});

		return array;
	}

	// Creates an <option> for each pair of the array
	function arrayToOptions(array){
		let options = array.sort(function(a,b){
			// Sort alphabetically
			var x = a['text'].toLowerCase(), y = b['text'].toLowerCase();
			return x < y ? -1 : x > y ? 1 : 0;
		}).map( function(sortedArray){
			return renderOptionComponent(sortedArray['value'], sortedArray['text']);
		}).join('');

		return options;
	}

	// Receives an object of the element to be updated
	// Replaces current options with a new options
	// options is a string of <option> elements
	function appendOptions(element,options){
		element.find('.menu').html(options);
		element.dropdown('refresh');
	}

	// Creates an <option> element
	function renderOptionComponent(value, text){
		return '<div class="item" data-value="'+value+'">'+text+'</div>';
	}


	/*
	*	Callbacks / Events
	*/

	// Modal Close
	$modalPatientForm.find('.close').on('click', function(event) {
		closeModalForm();
	});

	// Adds active class to the Gender Radion Buttons inside the Form.
	$btnGender.on('click keydown reset', '.button', function(event) {
		if (event.type == 'keydown'){
			if (event.which === 13 || event.which === 32){
				$(this).trigger('click');
				event.preventDefault();
			}
		}

		if(event.type == 'click'){
			$(this).addClass('active').siblings('.button').removeClass('active');
		}

		if(event.type == 'reset'){
			$(this).removeClass('active');
		}
	});

	$provinceDropdown.on('update', function(event, data) {
		let element = $(this);
		let options = arrayToOptions( data );
		appendOptions(element,options);
	});

	$cityDropdown.on('update', function(event, data) {
		let element = $(this);
		let options = arrayToOptions( data );
		appendOptions(element,options);
	});

	$barangayDropdown.on('update', function(event, data) {
		let element = $(this);
		let options = arrayToOptions( data )
		appendOptions(element,options);
	});

	$prefixDropdown.on('update', function(event, data){
		let element = $(this);
		let options = arrayToOptions( data );
		appendOptions(element, options);
	});

	$table.on('click','.button[data-action="view patient"]', function(event){
		var patientID = $(this).data('id'); // ID of the selected patient from the row

		if (patientStore.has(patientID)){
			// Get Patient Data from STORE and set it to the form.
			setPatientProfile( patientStore.get(patientID) );
			openModalPatientForm();
		}else{



		}
	});


	$table.on('click','.button[data-action="edit patient"]', function(event){
					
		var patientID = $(this).data('id'); // ID of the selected patient from the row

		if (patientStore.has(patientID)){
			// Get Patient Data from STORE and set it to the form.
			setFormValues( patientStore.get(patientID) );
			openModalPatientForm();
		}else{

			// Makes the Modal Wait for All AJAX response to complete.
			setLazyLoad('true');
			setLoading('true');
			openModalPatientForm();

			$(this).api({
				action: 'view patient', // change the action 'view patient' to get the data for pre-filling form
				urlData:{ id: patientID },
				on: 'now',
				onSuccess: function(response){
					let patientData = {
						activeProvince: response.address.barangay.city.province.provCode,
						activeCity: response.address.barangay.city.citymunCode,
						activeBarangay: response.address.barangay.brgyCode,

						firstname: response.firstname,
						middlename: response.middlename,
						lastname: response.lastname,

						gender: response.gender.toLowerCase(),
						age: response.age,
						activePrefix: response.phone.substr(0,4),
						phone: response.phone.substr(4),

						line1: response.address.address_1,
						line2: response.address.address_2
					};

					setFormValues(patientData);
					setLoading('false');
					setLazyLoad('false');

					// Put the response to Patients STORE.
					patientStore = patientStore.set(patientID, patientData);
				},
				onFailure: function(response){
					alert('error');
					return false;
				}
			})
		}

	}); // END OF EDIT CLICK EVENT

	function fetchPrefixes(){
		$.ajax({
			url: '/admin/ajax/prefix',
			beforeSend: (settings) => {
				if( !prefixStore.isEmpty() ){
					onPrefixFetch(prefixStore.toArray());
					return false;
				}
			}
		})
		.done(function(response) {
			let array = formatResponse(response, 'prefix', 'prefix');
			prefixStore = Immutable.List(array);
			onPrefixFetch(array);
		})
	}

	function onPrefixFetch(array){
		$prefixDropdown.trigger('update', [array]);
		$prefixDropdown.dropdown('set selected', state.get('activePrefix'));
	}

	function fetchProvinces(){
		$.ajax({
			url: '/admin/ajax/province',
			beforeSend: (settings) => {
				if ( !provinceStore.isEmpty() ){
					onProvinceFetch(provinceStore.toArray());
					return false;
				}
			}
		})
		.done(function(response){
			let array = formatResponse(response, 'provDesc', 'provCode');
			provinceStore = Immutable.List(array);
			onProvinceFetch(array);
		});
	}

	function onProvinceFetch(array){
		$provinceDropdown.trigger('update', [array]);
		$provinceDropdown.dropdown('set selected', state.get('activeProvince'));
	}

	function onProvinceSelect(provinceID){
		if(provinceID){
			state = state.set('activeProvince', provinceID);
			fetchCities(provinceID);
		}else{
			$cityDropdown.addClass('disabled').dropdown('clear');
			$cityDropdown.find(':input').prop('disabled', true);
		}
	}

	function fetchCities(provinceID){
		$.ajax({
			url: '/admin/ajax/province/'+provinceID+'/city',
			beforeSend: (settings) => {
				if (cityStore.get(provinceID) !== undefined){
					onCityFetch(cityStore.get(provinceID));
					return false;
				}
			}
		})
		.done(function(response){
			let array = formatResponse(response, 'citymunDesc', 'citymunCode');
			cityStore = cityStore.set(provinceID, array);
			onCityFetch(array);
		});
	}

	function onCityFetch(array){
		$cityDropdown.removeClass('disabled').dropdown('clear');
		$cityDropdown.find(':input').removeProp('disabled');
		$cityDropdown.trigger('update', [array]);
		$cityDropdown.dropdown('set selected', state.get('activeCity'));	
	}

	function onCitySelect(cityID){
		if(cityID){
			state = state.set('activeCity', cityID)
			fetchBarangays(cityID);
		} else{
			$barangayDropdown.addClass('disabled').dropdown('clear');
			$barangayDropdown.find(':input').prop('disabled', true);
		}
	}

	function fetchBarangays(cityID){
		$.ajax({
			url: '/admin/ajax/province/'+state.get('activeProvince')+'/city/'+cityID+'/barangay',
			beforeSend: (settings) => {
				if(barangayStore.get(cityID) !== undefined){
					onBarangayFetch(barangayStore.get(cityID));
					return false;
				}
			}
		})
		.done(function(response) {
			let array     = formatResponse(response, 'brgyDesc', 'brgyCode');
			barangayStore = barangayStore.set(cityID, array);
			onBarangayFetch(array);
		});
	}

	function onBarangayFetch(array){
		$barangayDropdown.removeClass('disabled').dropdown('clear');
		$barangayDropdown.find(':input').removeProp('disabled');
		$barangayDropdown.trigger('update', [array]);
		$barangayDropdown.dropdown('set selected', state.get('activeBarangay'));
	}

	function onBarangaySelect(barangayID){
		state = state.set('activeBarangay', barangayID)
	}

	/*
	*	Initialize Stuffs
	*/

	$formPatient.form({
		fields: formRules.get('fields'),
		on: 'blur'
	});

	$modalPatientForm.modal({
		onShow: () => {
			$addressOneTooltip.popup({ inline: true });
			$addressTwoTooltip.popup({ inline: true });
			$prefixDropdown.dropdown();
			$provinceDropdown.dropdown({ fullTextSearch: true, onChange: onProvinceSelect });
			$cityDropdown.dropdown({ fullTextSearch: true, onChange: onCitySelect });
			$barangayDropdown.dropdown({ fullTextSearch: true, onChange: onBarangaySelect });

			if(state.get('lazyLoad') == 'false'){
				fetchProvinces();
				fetchPrefixes();
			}
		},
		onHide: () => {
			$addressOneTooltip.popup('destroy');
			$addressTwoTooltip.popup('destroy');
			$prefixDropdown.dropdown('destroy');
			$provinceDropdown.dropdown('destroy');
			$cityDropdown.dropdown('destroy');
			$barangayDropdown.dropdown('destroy');
			$formPatient.form('clear');
			$btnMale.trigger('reset');
			$btnFemale.trigger('reset');

			// Set dropdowns to default values
			state = state
			.set('activeProvince', '354')
			.set('activeCity', '35401')
			.set('activeBarangay', '')
			.set('activePrefix', '0905')
			.set('lazyLoad', 'false');		
		}
	});

	$modalPatientView.modal();

	$table.DataTable({
		dom: '<"#table-actions"Bf>trip',
		buttons:{
			dom:{
				container:{
					className:'ui buttons'
				},
				button:{
					className:'ui button',
					tag: 'button'
				}
			},
			buttons:[
				{
					text:'<i class="add user icon"></i> Add new patient',
					className: 'green',
					action: function ( e, dt, node, config ) {
						openModalPatientForm();
					}
				}
			]
		},
		ajax: '/admin/patients',
		processing: true,
		serverSide: true,
		columns: [
			{
				data: 'id',
				name: 'id'
			},
			{
				data: 'firstname',
				name: 'firstname'
			},
			{
				data: 'middlename',
				name: 'middlename',
				defaultContent: '<i>None</i>'
			},
			{
				data: 'lastname',
				name: 'lastname'
			},
			{
				data: 'age',
				name: 'age'
			},
			{
				data: 'phone',
				name: 'phone',
				orderable: false,
				sortable: false
			},
			{
				data: 'action',
				name: 'action',
				orderable: false,
				searchable: false,
				sortable: false
			}
		]
	});

});