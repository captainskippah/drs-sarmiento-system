<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
/*
|--------------------------------------------------------------------------
| Administrator Pages Route
|--------------------------------------------------------------------------
|
*/

Route::get('admin', function(){
	return redirect('admin/dashboard');
});

Route::group(['prefix' => 'admin'], function(){

	Route::get('logout', 'Auth\LoginController@logout');
	Auth::routes();

	// Authentication Required Routes
	Route::group(['middleware' => ['auth'] ], function () {

		// Dashboard Page.
		Route::get('dashboard', function(){
			return view('admin.dashboard.index');
		});

		// POS
		Route::get('pos', 'Admin\PosController@index');
		Route::post('pos', 'Admin\PosController@store');

		// Inventory
		Route::get('products', 'Admin\ProductController@index');

		// Brands
		Route::get('brands/search/{query?}', 'Admin\BrandController@search');
		Route::get('brands/table', 'Admin\BrandController@table');
		Route::resource('brands', 'Admin\BrandController');

		// Appoinment
		Route::get('appointments', 'Admin\AppointmentController@index');

		/* CATEGORIES API */

			// Get Categories with Products only.		
			Route::get('categories/menu', 'Admin\CategoryController@showMenu');

			// Get All products belong under category
			Route::get('categories/{id}/products', 'Admin\CategoryController@showProducts');

			Route::get('categories/search{query?}', 'Admin\CategoryController@search');

			// Get Table Data of categories
			Route::get('categories/table', 'Admin\CategoryController@showTable');

			// CRUD Operations of categories
			Route::resource('categories', 'Admin\CategoryController');

		/* END OF CATEGORIES API */

		/* PRODUCTS API */

			// Restore archived product
			Route::put('products/{id}/restore', 'Admin\ProductController@restore');

			// Get Latest products
			Route::get('products/latest', 'Admin\ProductController@showLatest');

			// Get Table Data of products
			Route::get('products/table', 'Admin\ProductController@showTable');

			// CRUD Operations of categories
			Route::resource('products', 'Admin\ProductController');

		/* END OF PRODUCTS API */
		
		Route::group(['prefix' => 'patients'], function() {
			Route::get('table', 'Admin\PatientController@getTable');
			Route::get('search/{query?}', 'Admin\PatientController@search');

			Route::put('{id}/restore', 'Admin\PatientController@restore');
			Route::delete('{id}/archive', 'Admin\PatientController@archive');

			Route::get('{id}/prescriptions', ['as' => 'patients'], 'Admin\PatientController@prescriptions');
		});
		
		Route::resource('patients', 'Admin\PatientController', ['except' => ['show','destroy']]);
		
		// Prescriptions
		Route::resource('prescriptions', 'Admin\PrescriptionController');

		// Treatments
		Route::get('treatments/table', 'Admin\TreatmentController@getTable');
		Route::get('treatments/search', 'Admin\TreatmentController@search');
		Route::resource('treatments', 'Admin\TreatmentController');
	});

});
