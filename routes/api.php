<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::group(['prefix' => 'v1'], function() {
	Route::get('provinces/{province?}', 'LocationApiController@getProvince');
	Route::get('provinces/{province}/cities/{city?}', 'LocationApiController@getCity');
	Route::get('provinces/{province}/cities/{city}/barangays/{barangay?}', 'LocationApiController@getBarangay');

	Route::group(['prefix' => 'admin'], function() {
		
		Route::group(['prefix' => 'prescriptions'], function() {
			Route::get('table', 'Admin\PrescriptionController@getTable');
		});

	});
});