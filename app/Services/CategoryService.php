<?php

namespace App\Services;

use App\Models\Category;

class CategoryService
{

	protected $category;

	public function __construct(Category $category)
	{
		$this->category = $category;
	}

	public function save(array $data, $id = null)
	{
		$category              = empty($id) ? $this->category : $this->find($id);
		$category->name        = $data['name'];
		$category->category_id = $data['parent'];
		$category->save();
	}

	public function isUnique($name, $parent = null, $id = null)
	{
		$where['name']        = $name;
		$where['category_id'] = !empty($parent) ? $parent : null;

		if (! empty($id) ) {
			$where[] = ['id', '<>', $id];
		}

		return $this->category->where($where)->count() == 0;
	}

	public function subcategoriesCount($id)
	{
		return $this->category
			->with('subcategoriesCount')
			->where('id', $id)
			->first()
			->subcategoriesCount;
	}

	public function delete($id)
	{
		$this->find($id)->delete();
	}

	public function all($relationship = null, $orderBy = 'ASC', $orderColumn = 'name')
	{
		$query = $this->category->with('parent');

		if(!empty($relationship)){
			$query = $query->has($relationship);
		}

		return $query->orderBy($orderColumn, $orderBy)->get();
	}

	public function find($id)
	{
		return $this->category->find($id);
	}

	public function findByName($name)
	{
		return $this->category->where('name', 'LIKE', '%'.$name.'%')->get();
	}

	public function getById($id)
	{
		return $this->category->find($id);
	}

	public function getProducts($id, $detailed = false)
	{
		$query = $this->category
			->where('id', $id);

		if ($detailed) {
			$query = $query->with([
				'products',
				'products.brand'
			]);
		}

		return $query->first()->products;
	}

	public function getMenu($orderBy = 'ASC')
	{
		$query = $this->category
			->with('parent')
			->has('products')
			->orderBy('name', $orderBy)
			->get();

		return $query;
	}

	public function withParent()
	{
		return $this->category->with('parent');
	}

	public function withSubcategories()
	{
		return $this->category->with('subcategories');
	}

	public function searchCategoryDropdown($query)
	{
		return $this->category
			->with('parent')
            ->where('name', 'LIKE', '%'.$query.'%')
            ->get()
            ->toArray();
	}

	public function select(array $columns)
	{
		return $this->category->select($columns)->get();
	}
}