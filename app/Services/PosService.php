<?php

namespace App\Services;

use App\Services\ProductService;
use App\Services\StockService;

class PosService
{

	protected $products;
	protected $stocks;
	protected $transactions;

	public function __construct(ProductService $products, StockService $stocks, TransactionService $transactions)
	{
		$this->products     = $products;
		$this->stocks       = $stocks;
		$this->transactions = $transactions;
	}

	public function save(array $data)
	{
		foreach($data as $key => $product)
		{
			$stocks   = $this->products->getStocks($product['id'])->toArray();
			$shortage = false;

			foreach($stocks as $key => $stock)
			{
				// Current stock is enough. Don't iterate anymore.
				if($shortage === 0) { break; }

				$transactions['stock_id'][$key]    = $stock['id'];
				$transactions['description'][$key] = 'Purchased';

				// There's a shortage. Subtract what's left only.
				if ($shortage !== false) {
					$new_quantity = $stock['quantity'] - $shortage;
				} else {
					$new_quantity = $stock['quantity'] - $product['quantity'];
				}

				// If new quantity is negative, set to 0 and get the rest from next stocks
				// Else, record the new quantity.
				$transactions['quantity'][$key] = $new_quantity < 0 ? 0 : $new_quantity;
				$shortage = $new_quantity >= 0 ? 0 : $new_quantity * -1;
			}

			$this->stocks->save($this->transactions, $product['id'], $transactions);
		}
	}

}