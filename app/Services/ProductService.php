<?php

namespace App\Services;

use App\Models\Product;
use App\Services\StockService;
use App\Services\TransactionService;

class ProductService
{
	protected $product;

	public function __construct(Product $product)
	{
		$this->product = $product;
	}

	public function archive($id)
	{
		return $this->findById($id)->delete();	
	}

	public function restore($id)
	{
		return $this->product
			->onlyTrashed()
			->find($id)
			->restore();
	}

	public function save(StockService $stock, array $data, $id = null)
	{
        \DB::transaction(function() use ($stock, $data, $id){
			$product = empty($id) ? $this->product : $this->findById($id);
			$product->name        = ucwords($data['name']);
			$product->details     = $data['details'];
			$product->category_id = $data['category'];

			$product->brand_id   = $data['brand'];
			$product->alert_qty  = $data['alert'];
			$product->image      = empty($data['image']) ? $product->image : $data['image'];
			$product->price      = $data['price'];
			$product->cost       = $data['cost'];
			$product->unit       = $data['unit'];

			$product->save();

			$stock->save(app('TransactionService'), $product->id, $data['stocks']);
		});
	}

	public function all()
	{
		return $this->product->all();
	}

	public function getLatest($limit = 8, $detailed = false)
	{
		$query = $this->product
			->orderBy('id', 'DESC')
			->take($limit);
		
		if ($detailed) {
			$query = $query->with([
				'brand',
				'category'
			]);
		}

		return $query->get();
	}

	public function getByCategory($categoryID, $detailed = false)
	{
		$query = $this->product->where('category_id', $categoryID);

		if ($detailed) {
			$query = $query->with('stocks', 'brand', 'category');
		}

		return $query->get();
	}

	public function getStocks($productID)
	{
		return $this->product->find($productID)
			->stocks()
			->where('quantity', '<>', 0)
			->orderBy('expiration', 'ASC')
			->get();
	}

	public function findById($id, $detailed = false)
	{
		$query = $this->product->withTrashed();

		if ($detailed) {
			$query = $query->with('stocks', 'brand', 'category');
		}

		return $query->find($id);
	}

	public function select(array $columns)
	{
		return $this->product->select($columns);
	}

	public function with(array $relationship)
	{
		return $this->product->with($relationship);
	}

	public function where(array $criteria)
	{
		return $this->product->where($criteria);
	}

	public function tableData(array $columns = array(), $archived = false)
	{
		$query = $this->product
			->select($columns)
			->with('stocks', 'brand', 'category');

		if ($archived){
			$query = $query->onlyTrashed();
		}

		return app('datatables')->eloquent( $query )
            ->addColumn('action', function($product) use($archived){
                $view = '<a href="/admin/products/'.$product->id.'" class="item"><i class="search icon"></i> View</a>';

                $edit = '<a href="/admin/products/'.$product->id.'/edit" class="item"><i class="pencil icon"></i> Edit</a>';

                $delete = '<div class="item" data-action="delete product" data-id="'.$product->id.'"><i class="archive icon"></i> Archive</div>';

                $restore = '<div class="item" data-action="restore product" data-id="'.$product->id.'"><i class="upload icon"></i> Restore</div>';

                if($archived){
                    return '<div class="ui fluid selection dropdown"><div class="text">Actions</div><i class="dropdown icon"></i><div class="menu">'.$view.$edit.$restore.'</div></div>';
                }

                return '<div class="ui fluid selection dropdown"><div class="text">Actions</div><i class="dropdown icon"></i><div class="menu">'.$view.$edit.$delete.'</div></div>';
            }, false)
            ->addColumn('category', function($product){
                return $product->category->name;
            })
            ->addColumn('brand', function($product){
                return $product->brand ? $product->brand->name : '<i>Not Available</i>';
            })
            ->editColumn('image', function($product){
                return '<img src="/img/products/'.$product->image.'" class="ui tiny image">';
            }, false)
            ->editColumn('stocks.quantity', function($product){
                return $product->stocks->sum('quantity');
            })
            ->make(true);
	}
}