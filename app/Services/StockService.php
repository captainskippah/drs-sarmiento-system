<?php

namespace App\Services;

use App\Models\Stock;
use App\Services\TransactionService;

use Carbon\Carbon;

class StockService
{

	protected $stock;

	public function __construct(Stock $stock)
	{
		$this->stock = $stock;
	}

	public function save(TransactionService $transaction, $productID, array $data)
	{
		$stocksCount = count($data['quantity']);

		// Loop at each row of stocks
		for($x = 0; $x < $stocksCount; $x++)
		{
			// Update existing stock.
			if(!empty($data['stock_id'][$x])){
				$oldStock              = $this->stock->find($data['stock_id'][$x]);
				$oldStock_product_id   = $productID;
				$oldStock_quantity     = $oldStock->quantity;


				// No `manufactured` index when purchasing
				if (!empty($data['manufactured'])) {
					// Current stock has older manufacturing date (can't be greater as max is the current date on that day).
					// Current stock has newer expiration date (can't be older as min is the current date on that day).
					if($this->compareDates($oldStock->transactions()->first()->created_at, '>', $data['manufactured'][$x])){				
						$oldStock->manufactured = empty($data['manufactured'][$x]) ? null : Carbon::parse($data['manufactured'][$x])->format('Y-m-d');
					}
				}

				// No `expiration` index when purchasing
				if (!empty($data['expiration'])) {
					if($this->compareDates($oldStock->transactions()->first()->created_at, '<', $data['expiration'][$x])){
						$oldStock->expiration   = empty($data['expiration'][$x]) ? null : Carbon::parse($data['expiration'][$x])->format('Y-m-d');
					}
				}

				// Current stock has new value.
				if($oldStock_quantity != $data['quantity'][$x]){

					// Pre-aggregated Data / Quick sum of all transactions
					$oldStock->quantity = $data['quantity'][$x];

					// Record how much was changed to the current stock.
					$transactions[$x] = [
						'stock_id'    => $oldStock->id,
						'value'       => $data['quantity'][$x] - $oldStock_quantity,
						'description' => empty($data['description']) ? 'Edited Value Manually' : $data['description'][$x]
					];
				}

				$oldStock->save();

			}else{

				// Save new stock data.
				$this->stock->quantity     = $data['quantity'][$x];
				$this->stock->product_id   = $productID;
				$this->stock->manufactured = empty($data['manufactured'][$x]) ? null : Carbon::parse($data['manufactured'][$x])->format('Y-m-d');
				$this->stock->expiration   = empty($data['expiration'][$x]) ? null : Carbon::parse($data['expiration'][$x])->format('Y-m-d');
				$this->stock->save();

				// Initial value of the new stock
				$transactions[$x] = [
					'stock_id'    => $this->stock->id ,
					'value'       => $data['quantity'][$x],
					'description' => 'Initial Value'
				];
			}
		}

		// Save all transactions that happened if there was one
		if(!empty($transactions))
		{
			$transaction->save($transactions);
		}
	}

	private function compareDates($old, $operator, $new)
	{
		$old = empty($old) ? Carbon::now() : $old;
		$new = empty($new) ? Carbon::now() : Carbon::parse($new);

		switch ($operator) {
			case '<':
				return $old->lt($new);
			case '>':
				return $old->gt($new);
			case '<=':
				return $old->lte($new);
			case '>=':
				return $old->gte($new);
			case '=':
				return $old->eq($new);
			case '!=':
				return $old->ne($new);
			default:
				return false;
		}
	}

	public function getStocks($productID)
	{
		return $this->stock->where('product_id', $productID)
			->where('quantity', '<>', 0)
			->orWhere('quantity', '<>', NULL)
			->get();
	}

	public function getOldestStock($productID)
	{
		return $this->stock->where('product_id', $productID)
			->where('quantity', '<>', 0)
			->orWhere('quantity', '<>', NULL)
			->take(1)
			->first();
	}

}