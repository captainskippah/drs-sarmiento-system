<?php

namespace App\Services;

use App\Models\Brand;

class BrandService
{

	protected $brand;

	public function __construct(Brand $brand)
	{
		$this->brand = $brand;
	}

	public function save(array $data, $id = null)
	{
		if($this->brand->where('name', $data['name'])->first()){
			return true;
		}

		$brand = empty($id) ? $this->brand : $this->find($id);
		$brand->name = $data['name'];
		return $brand->save();
	}

	public function all()
	{
		return $this->brand->all();
	}

	public function find($id)
	{
		return $this->brand->find($id);
	}

	public function findByName($name)
	{
		return $this->brand
			->where('name', 'LIKE', '%'.$name.'%')
			->get();
	}

	public function delete($id)
	{
		$this->find($id)->delete();
	}

	public function select(array $columns)
	{
		return $this->brand->select($columns)->get();
	}


}