<?php

namespace App\Services;

use App\Models\Transaction;
use Carbon\Carbon;

class TransactionService
{

	protected $transaction;

	public function __construct(Transaction $transaction)
	{
		$this->transaction = $transaction;
	}

	public function save(array $data)
	{
		foreach($data as $key => $value)
		{
			$data[$key]['created_at'] = Carbon::now()->toDateTimeString();
			$data[$key]['updated_at'] = Carbon::now()->toDateTimeString();
		}

		$this->transaction->insert($data);
	}

}