<?php

namespace App\Http\Requests;

use App\Models\Patient;
use Illuminate\Foundation\Http\FormRequest;

class PatientRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            // Patients table fields
            'personal.firstname'  => 'required|string|max:255',
            'personal.middlename' => 'string|max:255',
            'personal.lastname'   => 'required|string|max:255',
            'personal.gender'     => 'required|in:male,female',
            'personal.age'        => 'required|integer',
            'personal.prefix'     => 'required|digits:4',
            'personal.phone'      => 'required|digits:7',

            // Addresses table fields
            'address.line1'       => 'required|max:255',
            'address.line2'       => 'max:255',
            'address.barangay'    => 'required|exists:barangays,brgyCode'
        ];
    }

    public function attributes()
    {
        return [
            // Patients table fields
            'personal.firstname'  => 'First name',
            'personal.middlename' => 'Middle name',
            'personal.lastname'   => 'Last name',
            'personal.gender'     => 'Gender',
            'personal.age'        => 'Age',
            'personal.prefix'     => 'Phone prefix',
            'personal.phone'      => 'Phone',

            // Addresses table fields
            'address.line1'       => 'Address line 1',
            'address.line2'       => 'Address line 2',
            'address.barangay'    => 'Barangay'
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function($validator){
            $patient = Patient::findByName(
                $this->input('personal.firstname'),
                $this->input('personal.lastname'),
                $this->input('personal.middlename')
            );

            // ID currently updating
            $patient_id = (int) $this->route('patient');

            if(!empty($patient) && $patient->id !== $patient_id) {
                $validator->errors()->add('', 'A record with the same name already exists.');
            }
        });
    }
}
