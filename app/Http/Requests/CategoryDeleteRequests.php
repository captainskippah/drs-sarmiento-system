<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class CategoryDeleteRequests extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }

    public function validator($factory)
    {
        $validator = $factory->make(
            $this->validationData(), $this->container->call([$this, 'rules']), $this->messages(), $this->attributes()
        );

        $validator->after(function($validator){

            $category = app('CategoryService');

            if( $category->subcategoriesCount($this->category) > 0){
                $validator->errors()->add('', 'Category is currently a parent of other categories.');
            }
        });

        return $validator;
    }
}
