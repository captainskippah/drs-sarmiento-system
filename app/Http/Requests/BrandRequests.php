<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class BrandRequests extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:brands,name,'.$this->route('brand')
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Brand name'
        ];
    }

    public function messages()
    {
        return [
            'name.unique' => ':attribute already exists'
        ];
    }

    public function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }
}
