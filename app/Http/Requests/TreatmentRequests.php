<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TreatmentRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required',
                'price' => [
                'required',
                'regex:/^([1-9][0-9]{,2}(,[0-9]{3})*|[0-9]+)(\.[0-9]{1,9})?$/'
            ]
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Treatment name',
            'price' => 'Price'
        ];
    }

    public function messages()
    {
        return [
            'price.regex' => 'Please enter a valid price'
        ];
    }
}
