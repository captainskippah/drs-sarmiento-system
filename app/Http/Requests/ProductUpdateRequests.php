<?php

namespace App\Http\Requests;

use App\Http\Requests\ProductRequests;

class ProductUpdateRequests extends ProductRequests
{
    public function rules()
    {
        return [
            // Required Fields
            'name'             => 'required',
            'category'         => 'required',            
            'price'            => 'required|numeric',
            'quantity-stock'   => 'required',
            'quantity-stock.*' => 'required|integer',
            'unit'             => 'required',

            // Optional fields
            'image'            => 'image|size:5000',
            'cost'             => 'numeric',
            'quantity-alert'   => 'integer',

            // Optional fields on stocks
            'manufactured.*' => 'date_format:m/d/Y',
            'expiration.*'   => 'date_format:m/d/Y'
        ];
    }
}
