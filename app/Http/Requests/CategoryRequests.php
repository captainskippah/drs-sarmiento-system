<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class CategoryRequests extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'   => 'required|string|max:50',
            'parent' => 'integer'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Category name'
        ];
    }

    public function messages()
    {
        return [
            'name.unique' => ':attribute already exists'
        ];
    }

    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }

    public function validator($factory)
    {
        $validator = $factory->make(
            $this->validationData(), $this->container->call([$this, 'rules']), $this->messages(), $this->attributes()
        );

        $validator->after(function($validator){
            $category = app('CategoryService');

            if( !$category->isUnique($this->name, $this->parent, $this->category) ){
                $validator->errors()->add('', 'Category with the same name and parent already exist');
            }
        });

        return $validator;
    }
}
