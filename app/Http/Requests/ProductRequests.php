<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequests extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            // Required Fields
            'name'             => 'required',
            'category'         => 'required',            
            'image'            => 'required|image|max:5000',
            'price'            => 'required|numeric',
            'quantity-stock'   => 'required',
            'quantity-stock.*' => 'required|integer',
            'unit'             => 'required',

            // Optional fields
            'cost'             => 'numeric',
            'quantity-alert'   => 'integer',

            // Optional fields on stocks
            'manufactured.*' => 'date_format:m/d/Y',
            'expiration.*'   => 'date_format:m/d/Y'
        ];
    }

    public function attributes()
    {
        return [
            'name'             => 'Product name',
            'category'         => 'Product category',
            'image'            => 'Product image',
            'price'            => 'Product price',
            'cost'             => 'Product cost',
            'quantity-alert'   => 'Alert quantity',
            'quantity-stock'   => 'Stock',
            'quantity-stock.*' => 'Stock quantity',
            'expiration.*'     => 'Expiration date',
            'manufactured.*'   => 'Manufacturing date',
            'unit'             => 'Unit'
        ];
    }

    public function messages()
    {
        return [
            'manufactured.*.date_format' => ':attribute should be in the format of MM/DD/YYYY',
            'expiration.*.date_format' => ':attribute should be in the format of MM/DD/YYYY',
            'image.size' => 'Maximum image size is 5MB only'
        ];
    }
}
