<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Google_Client;
use Google_Service_Calendar;

class AppointmentController extends Controller
{
	protected $calendar;

	public function __construct(Google_Service_Calendar $calendar)
	{
		$this->calendar = $calendar;
	}

    public function index()
    {
    	return view('admin.appointments.index');
    }

    public function store(Request $request)
    {
		$calendarID  = env('GOOGLE_CALENDAR_ID');
		$key_name    = env('GOOGLE_SERVICE_KEY');
		$key_dir     = storage_path('app/keys/');

    	$client = new Google_Client();
    	$client->setApplicationName('Sarmiento_Calendar');
    	$client->setScopes(Google_Service_Calendar::CALENDAR);
    	$client->setAuthConfig(storage_path('app/keys/').env('GOOGLE_SERVICE_KEY'));

    	$service = new Google_Service_Calendar($client);

		$event = new Google_Service_Calendar_Event([
			'start' => ['dateTime' => Carbon::now()->toIso8601String(), 'timeZone' => 'Asia/Manila'],
			'end' => ['dateTime' => Carbon::now()->addHours(3)->toIso8601String(), 'timeZone' => 'Asia/Manila'],
			'description' => 'Test event from Artisan description',
			'summary' => 'Test event from artisan summary'
		]);

    }
}
