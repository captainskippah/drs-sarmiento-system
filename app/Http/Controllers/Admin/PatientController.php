<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Datatables;
use App\Models\Patient;
use App\Models\Address;
use App\Http\Requests\PatientRequest;

class PatientController extends Controller
{
    private $patient;
    private $address;

    public function __construct(Patient $patient, Address $address)
    {
        $this->patient = $patient;
        $this->address = $address;
    }

    public function index()
    {
        return view('admin.patients.index');
    }

    public function create()
    {
        return view('admin.patients.create');
    }

    public function edit(Request $request, $id)
    {
        $patient = $this->patient->getFullInfo()
                        ->withTrashed()
                        ->where('patients.id', $id)
                        ->first();

        // Create and Edit form is same
        return view('admin.patients.create')
            ->with(['patient' => $patient]);
    }

    public function getTable(Request $request)
    {
        $patients = $this->patient
            ->getFullInfo();

        if ($request->input('status') === 'archived') {
            $patients = $patients->onlyTrashed();
        }

        return Datatables::eloquent( $patients )->make(true);
    }

    public function store(PatientRequest $request)
    {
        \DB::transaction(function() use ($request) {
            $this->patient->firstname  = $request->input('personal.firstname');
            $this->patient->middlename = $request->input('personal.middlename');
            $this->patient->lastname   = $request->input('personal.lastname');
            $this->patient->gender     = $request->input('personal.gender');
            $this->patient->age        = $request->input('personal.age');
            $this->patient->phone      = $request->input('personal.prefix') . $request->input('personal.phone');
            $this->patient->save();

            $this->address->address1 = $request->input('address.line1');
            $this->address->address2 = $request->input('address.line2');
            $this->address->brgy_id  = $request->input('address.barangay');
            $this->address->patient_id = $this->patient->id;
            $this->address->save();
        });

        return redirect('/admin/patients')
                ->with('status', 'success-store');
    }

    public function update(PatientRequest $request, $id)
    {
        \DB::transaction(function() use ($request, $id) {
            $this->patient             = $this->patient->find($id);
            $this->patient->firstname  = $request->input('personal.firstname');
            $this->patient->middlename = $request->input('personal.middlename');
            $this->patient->lastname   = $request->input('personal.lastname');
            $this->patient->gender     = $request->input('personal.gender');
            $this->patient->age        = $request->input('personal.age');
            $this->patient->phone      = $request->input('personal.prefix') . $request->input('personal.phone');

            $this->patient->address->address1 = $request->input('address.line1');
            $this->patient->address->address2 = $request->input('address.line2');
            $this->patient->address->brgy_id  = $request->input('address.barangay');
            
            $this->patient->push();
        });

        return redirect('/admin/patients')
                ->with('status', 'success-update');
    }

    public function archive(Request $request, $id)
    {
        $patient = Patient::findOrFail($id);
        $patient->delete();

        return response('',204);
    }

    public function restore(Request $request, $id)
    {
        Patient::onlyTrashed()
            ->find($id)
            ->restore();

        return response('', 204);
    }

    /*
    * Methods used in processing data.
    * In Logical Order.
    */

    public function search($query = null)
    {

        if(empty($query)){
            return response()->json([
                'success' => false
            ]);
        }

        $result = Patient::whereRaw('
            replace(concat(firstname, " ", middlename, " ", lastname), " ", " ") LIKE "%'.$query.'%"
        ')
        ->get();

        if($result == null){
            return response()->json([
                'success' => false
            ]);
        }

        return response()->json([
            'success' => true,
            'results' => $result->map(function($item, $key){
                return [
                    'name'  => $item->firstname.' '.$item->middlename.' '.$item->lastname,
                    'value' => $item->id,
                    'text'  => $item->firstname.' '.$item->middlename.' '.$item->lastname
                ];
            })
        ]);

    }
}
