<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\BrandRequests;

use App\Services\BrandService;
use Datatables;

class BrandController extends Controller
{
	protected $brands;

	public function __construct(BrandService $brands)
	{
		$this->brands = $brands;
	}

    public function index()
    {
    	return view('admin.brands.index');
    }

    public function table()
    {
    	$brands = $this->brands->all();

    	return Datatables::of($brands)
    		->addColumn('action', function($brand){
				$edit   = '<button type="button" class="ui yellow button" data-action="edit brand" data-id="'.$brand->id.'"><i class="pencil icon"></i> Edit</button>';
                $delete = '<button type="button" class="ui red button" data-action="delete brand" data-id="'.$brand->id.'"><i class="trash icon"></i> Delete</button>';

                return '<div class="two small ui buttons">'.$edit.$delete.'</div>';
    		}, false)
    		->make(true);
    }

    public function search($query = null)
    {
        if(empty($query)){
            return response()->json([
                'success' => false
            ]);
        }

        $results = $this->brands->findByName($query);

        if($results == null){
            return response()->json([
                'success' => false
            ]);
        }

        return response()->json([
            'success' => true,
            'results' => $results->map(function($item, $key){
                return [
                    'name'  => $item->name,
                    'value' => $item->id,
                    'text'  => $item->name
                ];
            })
        ]);
    }

    public function store(BrandRequests $request)
    {
    	$this->brands->save([
    		'name' => $request->name
    	]);

    	return response('', 204);
    }

    public function update(BrandRequests $request, $id)
    {
    	$this->brands->save([
    		'name' => $request->name
    	], $id);

    	return response('', 204);
    }

    public function destroy($id)
    {
    	$this->brands->delete($id);
    	return response('', 204);
    }
}


