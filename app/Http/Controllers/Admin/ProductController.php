<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\ProductRequests;
use App\Http\Requests\ProductUpdateRequests;

use App\Services\ProductService;

use Log;

class ProductController extends Controller
{

    protected $product;
    private $uploadPath;

    public function __construct(ProductService $product)
    {
        $this->product    = $product;
        $this->uploadPath = storage_path('app/public/uploads/products');
    }

    public function index(Request $request)
    {
        if($request->ajax()){

            $products = $request->has('category') ?
                $this->product->getByCategory($request->category) :
                $this->product->getLatest();

            return response()->json($products);
        }

        return view('admin.products.index');
    }

    public function show($id)
    {
        $product = $this->product->findById($id, true);

        return view('admin.products.show')
            ->with('product', $product);
    }

    public function showLatest(Request $request)
    {
        if ($request->ajax()) {
            $products = $this->product->getLatest(
                $request->input('limit', 8),
                $request->input('detailed', false)
            );
            return response()->json($products);
        }
    }

    public function showTable(Request $request)
    {
        $columns = [
            'products.image',
            'products.name',
            'products.price',
            'products.cost',
            'products.category_id',
            'products.brand_id',
            'products.alert_qty',
            'products.id'
        ];

        return $this->product->tableData($columns, $archived = $request->archived == 'true');
    }

    public function create()
    {
        return view('admin.products.create');
    }

    public function store(ProductRequests $request)
    {
        try{
            $this->product->save(app('StockService'), [
                // Common Fields
                'name'       => $request->name,
                'category'   => $request->has('subcategory') ? $request->subcategory : $request->category,
                'details'    => $request->details,
                
                // Product Fields
                'brand'      => $request->has('brand') ? $request->brand : null,
                'image'      => $filename = $this->upload($request->image),
                'price'      => $request->price,
                'cost'       => $request->cost,
                'alert'      => $request->input('quantity-alert'),
                'unit'       => $request->unit,

                // All stocks of the incoming product
                'stocks'     => [
                    'quantity'     => $request->input('quantity-stock'),
                    'expiration'   => $request->expiration,
                    'manufactured' => $request->manufactured
                ]
            ]);

            return redirect('/admin/inventory')
                ->with('status', 'success');

        }catch(\Illuminate\Database\QueryException $e){
            $this->deleteImage($filename);

            dd($e->getMessage());

            if($request->ajax()){
                return response(['There was a problem while adding new item. Please contact the Administrator/Developer'], 500);
            }

            return redirect('/admin/product/create')
                ->with('status', 'error');                
        }
    }

    private function upload($image)
    {
        $tinify    = resolve('tinify');
        $source      = $tinify->fromFile( $image->path() );
        $extension   = $image->guessExtension();
        $filename    = uniqid('img_').'.'.$extension;
        $properties  = array(
            "method" => "fit",
            "width"  => 300,
            "height" => 300
        );

        // Set Tinify's Log path
        Log::useDailyFiles(storage_path().'/logs/tinify.log');

        try {
            $optimized = $source->resize($properties);
            $optimized->toFile($this->uploadPath.'/'.$filename);
            Log::info('Tinify Compression Count: '.$tinify->compressionCount());
        } catch(AccountException $e) {
            Log::alert($e->getMessage());
            return abort(503,['Account used for the image service has an error. Please contact the administrator/developer.']);
        } catch(ClientException $e) {
            Log::debug($e->getMessage());
            return abort(422,['Image service denied the uploaded photo. Usually because the image is not jpg or png']);
        } catch(ServerException $e) {
            Log::error($e->getMessage());
            return abort(504, ['Temporary Server issue with the image optimization service.']);
        } catch(ConnectionException $e) {
            Log::debug($e->getMessage());
            return abort(408,['Optimizing failed. Please try again.']);
        } catch(Exception $e) {
            Log::error($e->getMessage());
            return abort(500);
        }

        return $filename;
    }

    private function deleteImage($filename)
    {
        return unlink($this->uploadPath.'/'.$filename);
    }

    public function edit($id)
    {
        $product = $this->product->findById($id, true);

        return view('admin.products.create')
            ->with('product', $product);
    }

    public function update(ProductUpdateRequests $request, $id)
    {
        try{
            $this->product->save(app('StockService'), [
                // Common Fields
                'name'       => $request->name,
                'category'   => $request->has('subcategory') ? $request->subcategory : $request->category,
                'details'    => $request->details,
                
                // Product Fields
                'brand'      => $request->has('brand') ? $request->brand : null,
                'image'      => $request->hasFile('image') ? $filename = $this->upload($request->image) : null,
                'price'      => $request->price,
                'cost'       => $request->cost,
                'alert'      => $request->input('quantity-alert'),
                'unit'       => $request->unit,

                // All stocks of the incoming product
                'stocks'     => [
                    'stock_id'     => $request->input('stock_id'),
                    'quantity'     => $request->input('quantity-stock'),
                    'expiration'   => $request->expiration,
                    'manufactured' => $request->manufactured
                ]
            ], $id);

            return redirect('/admin/inventory')
                ->with('status', 'edited');

        }catch(\Illuminate\Database\QueryException $e){
            dd($e->getMessage());
            
            $this->deleteImage($filename);
            
            if($request->ajax()){
                return response(['There was a problem while adding new item. Please contact the Administrator/Developer'], 500);
            }

            return redirect('/admin/product/create')
                ->with('status', 'error');                
        }
    }

    public function destroy($id)
    {
        $this->product->archive($id);

        return response('',204);
    }

    public function restore($id)
    {
        $this->product->restore($id);

        return response('', 204);
    }
}