<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Datatables;

use App\Models\Prescription;
use App\Models\Patient;
use App\Models\Treatment;
use App\Models\Medication;

class PrescriptionController extends Controller
{
    private $prescriptions;

    public function __construct(Prescription $prescriptions)
    {
        $this->prescriptions = $prescriptions;
    }

    public function index()
    {
        return view('admin.prescriptions.index');
    }

    public function create()
    {
        return view('admin.prescriptions.create');
    }

    public function show(Request $request, $id)
    {
        return view('admin.prescriptions.show');
    }

    public function getTable(Request $request)
    {
        $prescriptions = $this->prescriptions->getRelated();
        
        return Datatables::eloquent($prescriptions)->make(true);
    }

    public function store(Request $request)
    {

    }


    public function update(Request $request, $id)
    {
        
    }

    public function destroy($id)
    {
        
    }

    private function save(array $data, $id = NULL)
    {
        \DB::transaction(function() use ($data, $id){

            // Empty $id means new entry, else, edit is wanted.
            $prescription               = empty($id) ? new Prescription : Prescription::find($id);
            $prescription->patient_id   = $data['input-patient'];
            $prescription->case         = $data['input-case'];
            $prescription->treatment_id = $data['input-treatment'];
            $prescription->notes        = $data['input-notes'];
            
            $prescription->save();

            foreach ($data as $column => $values) {
                $currentRow = 0;
                // Skips inputs not related to medication table
                if(
                    $column == 'medicine' ||
                    $column == 'frequency' ||
                    $column == 'days' ||
                    $column == 'instruction'
                ){
                    // Store value of current $column of all rows
                    foreach ($values as $value) {
                        $medications[$currentRow][$column] = $values[$currentRow];
                        $medications[$currentRow]['prescription_id'] = $prescription->id;
                        $currentRow++;
                    }
                }       
            }

            if(!empty($medications)){
                Medication::insert($medications);
            }
        });
    }
}
