<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Datatables;

use App\Models\Province;
use App\Models\City;
use App\Models\Barangay;
use App\Models\PhonePrefix;


use App\Models\Treatment;


class Ajax extends Controller
{
    public function queryPrefix()
    {
        return PhonePrefix::select('prefix')->get();
    }

    public function queryLocation(Request $request, $province = NULL, $city = NULL, $barangay = NULL)
    {
        if ( !empty($province) )
        {
            if (!empty($city) )
            {
                if ( !empty($barangay) )
                {
                    return $this->formatResults( $this->getBarangay($province, $city, $barangay)->toArray() );
                }

                if ( $request->is('admin/ajax/province/*/city/*/barangay') )
                {
                    return $this->formatResults( $this->getBarangay($province, $city, $barangay)->toArray() );
                }

                return $this->formatResults( $this->getCity($province, $city)->toArray() );
            }

            if ( $request->is('admin/ajax/province/*/city') )
            {
                return $this->formatResults( $this->getCity($province, $city)->toArray() );
            }

            return $this->formatResults( $this->getProvince($province)->toArray() );
        }

        return $this->formatResults( $this->getProvince($province)->toArray() );
    }

    private function getProvince($province = NULL)
    {
    	if ( empty($province) )
    	{
            $results = Province::select('provCode', 'provDesc')
                ->orderBy('provDesc', 'asc')
                ->get();

            return $results;
    	}

    	return Province::select('provCode', 'provDesc')->findOrFail($province);
    }

    private function getCity($province, $city = NULL)
    {
    	if ( empty($city) )
    	{
            $results = City::orderBy('citymunDesc', 'asc')
                ->where('provCode', $province)
                ->get();

    		return $results;
    	}

    	return City::select('citymunCode', 'citymunDesc')->findOrFail($city);
    }

    private function getBarangay($province, $city, $barangay = NULL)
    {
    	if ( empty($barangay) )
    	{
            $results = Barangay::select('brgyCode', 'brgyDesc')
                ->orderBy('brgyDesc', 'asc')
                ->where('citymunCode', $city)
                ->get();

            return $results;
    	}

		return Barangay::select('brgyCode', 'brgyDesc')->findOrFail($barangay);
    }

    private function formatResults($results)
    {
        // Not single array.
        if ($this->findArrayKey($results) === FALSE)
        {
            $result_key = $this->findArrayKey($results[0]);

            foreach ($results as $key => $result)
            {
                $results[$key][$result_key] = ucwords( strtolower($result[$result_key]) );      
            }

            return $results;
        }

        $result_key = $this->findArrayKey($results);
        $results[$result_key] = ucwords( strtolower($results[$result_key]) );

        return $results;
    }

    private function findArrayKey($array)
    {
        if (array_key_exists('provDesc', $array))
        {
            return 'provDesc';
        }

        if (array_key_exists('citymunDesc', $array))
        {
            return 'citymunDesc';
        }

        if (array_key_exists('brgyDesc', $array))
        {
            return 'brgyDesc';
        }

        return FALSE;
    }
}
