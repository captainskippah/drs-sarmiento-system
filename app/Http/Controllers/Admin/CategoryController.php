<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CategoryRequests;
use App\Http\Requests\CategoryDeleteRequests;

use App\Services\CategoryService;

use Datatables;

class CategoryController extends Controller
{
	protected $categories;

	public function __construct(CategoryService $categories)
	{
		$this->categories = $categories;
	}

    public function index(Request $request)
    {
        if($request->ajax()){
            return response()->json($this->categories->all());
        }

    	return view('admin.categories');
    }

    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            $category = $this->categories->getById($id);

            return response()->json($category);
        }
    }

    public function showMenu(Request $request)
    {
        $category = $this->categories->getMenu($request->input('order', 'ASC'));

        if ($request->ajax()) {
            return response()->json($category);
        }
    }

    public function showProducts(Request $request, $id)
    {
        if ($request->ajax()) {
            $products = $this->categories->getProducts($id, $request->input('detailed', false));

            return response()->json($products);
        }
    }

    public function showTable()
    {
    	$categories = $this->categories->withParent();
    	
    	return Datatables::of($categories)
    		->addColumn('action', function($category){
				$edit   = '<button type="button" class="ui yellow button" data-action="edit category" data-id="'.$category->id.'"><i class="pencil icon"></i> Edit</button>';
                $delete = '<button type="button" class="ui red button" data-action="delete category" data-id="'.$category->id.'"><i class="trash icon"></i> Delete</button>';

                return '<div class="two small ui buttons">'.$edit.$delete.'</div>';
    		}, false)
    		->make(true);
    }

    public function search(Request $request)
    {
    	if(!$request->has('q'))
    	{
    		return response()->json([
    			'success' => false
    		]);
    	}

    	$results = $this->categories->searchCategoryDropdown($request->input('q'));

    	if(empty($results))
    	{
    		return response()->json([
    			'success' => false
    		]);
    	}

    	return response()->json([
            'success' => true,
            'results' => $this->mapArray($results)
        ]);
    }

    private function mapArray($array)
    {
        return array_map(array($this, 'formatToDropdown'), $array);
    }

    private function formatToDropdown($value)
    {
        return [
            'name'          =>  empty($value['parent']) ? $value['name'] : '<small class="right floated"><i>('.$value['parent']['name'].')</i></small>'.$value['name'],
            'value'         => $value['id'],
            'text'          => $value['name']
        ];
    }

    public function store(CategoryRequests $request)
    {
    	$this->categories->save([
			'name'   => $request->name,
			'parent' => $request->parent
    	]);

    	return response('',204);
    }

    public function update(CategoryRequests $request, $id)
    {
    	$this->categories->save([
    		'name'   => $request->name,
			'parent' => $request->parent
    	], $id);

    	return response('',204);
    }

    public function destroy(CategoryDeleteRequests $request, $id)
    {
    	$this->categories->delete($id);
    	return response('',204);	
    }
}
