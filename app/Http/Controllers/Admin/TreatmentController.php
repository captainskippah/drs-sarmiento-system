<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Datatables;
use App\Models\Treatment;
use App\Http\Controllers\Controller;
use App\Http\Requests\TreatmentRequests;


class TreatmentController extends Controller
{
    private $treatments;

    public function __construct(Treatment $treatment)
    {
        $this->treatments = $treatment;
    }

    public function index()
    {
        return view('admin.treatments.index');
    }

    public function create()
    {
        return view('admin.treatments.create');
    }

    public function getTable(Request $request)
    {
        $results = $this->treatments->select(['id', 'name', 'price']);
        return Datatables::eloquent($results)->make(true);
    }

    public function show(Request $request, $id)
    {
        // Currently, Table shows enough data.
    }

    public function edit(Request $request, $id)
    {
        $treatment = $this->treatments->find($id, ['id', 'name', 'price']);

        return view('admin.treatments.create')
                ->with('treatment', $treatment);
    }

    public function store(TreatmentRequests $request)
    {
        $this->treatments->name = $request->input('name');
        $this->treatments->price = $request->input('price');
        $this->treatments->save();

        return redirect('/admin/treatments')
                ->with('status', 'success-create');
    }

    public function update(TreatmentRequests $request, $id)
    {
        $this->treatments = $this->treatments->find($id);
        $this->treatments->name = $request->input('name');
        $this->treatments->price = $request->input('price');
        $this->treatments->save();

        return redirect('/admin/treatments')
                ->with('status', 'success-update');
    }

    public function destroy(Request $request, $id)
    {
        $this->treatments->find($id)->delete();

        if ($request->ajax() || $request->wantsJSON()) {
            return response()->json();
        }

        return redirect('/admin/treatments')
                ->with('status', 'success-delete');
    }

    public function search(Request $request)
    {
        $results = '';

        if ($request->has('q')) {
            $results = $this->treatments
                        ->where('name', 'like', "%{$request->input('q')}%")
                        ->orWhere('price', $request->input('q'))
                        ->get();
        }

        return response()->json($results);
    }
}
