<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PosRequests;

use App\Http\Controllers\Controller;

use App\Services\PosService;

class PosController extends Controller
{
	protected $pos;

	public function __construct(PosService $pos)
	{
		$this->pos = $pos;
	}

    public function index(Request $request)
    {
    	return view('admin.pos');
    }

    public function store(PosRequests $request)
    {
    	$this->pos->save($request->all());
    }
}
