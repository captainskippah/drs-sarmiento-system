<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Province;
use App\Models\City;
use App\Models\Barangay;

class LocationApiController extends Controller
{
    public function getProvince(Request $request)
    {
        $query = Province::orderBy('provDesc', 'Asc');

 		$results = !empty($request->route('province'))
 			? $query->where('provCode', $request->route('province'))->get()
 			: $query->get();

    	return response()->json($results);
    }

    public function getCity(Request $request)
    {
    	$query = City::where('provCode', $request->route('province'))
                        ->orderBy('citymunDesc', 'Asc');

    	$results = !empty($request->route('city'))
 			? $query->where('citymunCode', $request->route('city'))->get()
 			: $query->get();

    	return response()->json($results);
    }
    public function getBarangay(Request $request)
    {
    	$query = Barangay::where('citymunCode', $request->route('city'))
                            ->orderBy('brgyDesc', 'Asc');

    	$results = !empty($request->route('barangay'))
    		? $query = $query->where('brgyCode', $request->route('barangay'))->get()
    		: $query->get();

    	return response()->json($results);
    }
}
