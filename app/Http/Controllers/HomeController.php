<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Services\CategoryService;
use App\Services\ProductService;

use App\Http\Requests;

class HomeController extends Controller
{
    public function index()
    {
    	return view('home')
    		->with('categories', app('CategoryService')->getMenu())
    		->with('products', app('ProductService')->all());
    }
}
