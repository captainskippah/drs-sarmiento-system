<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
   public $timestamps = false;
	protected $primaryKey = 'citymunCode';

	public function barangays()
	{
	return $this->hasMany('App\Models\Barangay', 'citymunCode');
	}

	public function province()
	{
		return $this->belongsTo('App\Models\Province', 'provCode');
	}

   public function getNameAttribute()
   {
      return ucwords( strtolower($this->attributes['citymunDesc']) );
   }
}
