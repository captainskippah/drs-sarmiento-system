<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhonePrefix extends Model
{
    public $timestamps = false;
}
