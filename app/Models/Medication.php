<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Medication extends Model
{
	protected $guarded = [];
    public $timestamps = false;

    public function prescription()
    {
    	return $this->belongsTo('App\Models\Prescription');
    }
}
