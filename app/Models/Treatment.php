<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Treatment extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function prescriptions()
    {
    	return $this->hasMany('App\Models\Prescription');
    }

    public function setNameAttribute($value)
    {
    	$this->attributes['name'] = ucwords(strtolower($value));
    }
}
