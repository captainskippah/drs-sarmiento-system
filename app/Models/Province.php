<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
   public $timestamps    = false;
   protected $primaryKey = 'provCode';

	public function cities()
	{
	  return $this->hasMany('App\Models\City', 'provCode');
	}

	public function barangays()
	{
		return $this->hasManyThrough(
			'App\Models\Barangay', 'App\Models\City',
			'provCode', 'citymunCode', 'citymunCode'
		);
	}
}
