<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    public $timestamps = false;
    protected $guarded = [];


    public function product()
    {
    	return $this->belongsTo('App\Models\Product');
    }

    public function transactions()
    {
    	return $this->hasMany('App\Models\Transaction');
    }
}
