<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
	public $timestamps = false;
    protected $guarded = [];


    /*
    *   Relationships sections
    */

    public function patient()
    {
    	return $this->belongsTo('App\Models\Patient', 'patient_id');
    }

    public function barangay()
    {
    	return $this->belongsTo('App\Models\Barangay', 'brgy_id');
    }

    /*
    *   Accessors Section
    */

    public function getAddress2Attribute($value)
    {
        return empty($this->attributes['address_2']) ? 'None' : $this->attributes['address_2'];
    }

    /*
    *   Mutators Section
    */

    public function setAddress1Attribute($value)
    {
        return $this->attributes['address_1'] = ucwords($value);
    }

    public function setAddress2Attribute($value)
    {
        return $this->attributes['address_2'] = ucwords($value);
    }
}
