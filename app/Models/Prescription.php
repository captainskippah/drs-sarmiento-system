<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{
    protected $guarded = [];
    //public $timestamps = false;

    protected function validation_rules()
    {
        $rules = array(
            // Prescriptions table fields
            'input-case'      => 'required|max:255',
            'input-patient'   => 'required|integer',
            'input-treatment' => 'integer',
            'input-notes'     => 'max:255',

            // Medications table fields
            'medicine.*'     => 'bail|required_with:days.*,frequency.*,instructions.*|max:255|',
            'frequency.*'    => 'bail|required_with:days.*,instructions.*,medicine.*|integer|',
            'days.*'         => 'bail|required_with:frequency.*,instructions.*,medicine.*|integer|',
            'instructions.*' => 'bail|required_with:days.*,frequency.*,medicine.*|max:255|',
        );

        return $rules;
    }


    protected function inputNames()
    {
        return [
            // Prescriptions table fields
            'input-case'      => 'Case history',
            'input-patient'   => 'Patient name',
            'input-treatment' => 'Treatment',
            'input-notes'     => 'Notes',

            // Medications table fields
            'medicine.*'    => 'Medicine(s)',
            'frequency.*'   => 'Times per day',
            'days.*'        => 'Days',
            'instruction.*' => 'Instructions'
        ];
    }

    public function getRelated()
    {
        return $this->select([
                'prescriptions.id',
                'prescriptions.case',
                'prescriptions.notes',

                'patients.firstname',
                'patients.middlename',
                'patients.lastname',

                'treatments.id',
                'treatments.name'
            ])
            ->join('patients', 'prescriptions.patient_id', '=', 'patients.id')
            ->join('treatments', 'prescriptions.treatment_id', '=', 'treatments.id');
    }

    public function patient()
    {
        return $this->belongsTo('App\Models\Patient');
    }

    public function treatment()
    {
    	return $this->belongsTo('App\Models\Treatment');
    }

    public function medications()
    {
        return $this->hasMany('App\Models\Medication');
    }

    public function setCaseAttribute($value)
    {
    	return $this->attributes['case'] = ucwords(strtolower($value), '.');
    }

    public function setTreatmentIdAttribute($value)
    {
        return $this->attributes['treatment_id'] = empty($value) ? NULL : $value;
    }

    public function setInstructionsAttribute($value)
    {
        return $this->attributes['instructions'] = ucwords(strtolower($value), '.');
    }

    public function getTreatmentAttribute()
    {
        return empty($this->attributes['treatment']) ? 'None' : $this->attributes['treatment'];
    }

}
