<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;

    public function subcategories()
    {
    	return $this->hasMany('App\Models\Category');
    }

    public function parent()
    {
    	return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function subcategoriesCount()
    {
        return $this->hasOne('App\Models\Category')
            ->selectRaw('category_id, count(*) as aggregate')
            ->groupBy('category_id');
    }

    public function getSubcategoriesCountAttribute()
    {
        if( !array_key_exists('subcategoriesCount', $this->relations))
            $this->load('subcategoriesCount');

        $related = $this->getRelation('subcategoriesCount');

        return !empty($related) ? (int) $related->aggregate : 0;
    }

    public function setCategoryIdAttribute($value)
    {
    	return $this->attributes['category_id'] = empty($value) ? null : $value;
    }

    public function setNameAttribute($value)
    {
        return $this->attributes['name'] = ucwords( strtolower($value) );
    }
}
