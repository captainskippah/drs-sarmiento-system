<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Patient extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $hidden  = ['deleted_at'];
    protected $dates   = ['deleted_at'];

    public static function findByName($firstname, $lastname, $middlename = null)
    {
        $query = self::withTrashed()
                    ->where('firstname', $firstname)
                    ->where('lastname', $lastname);

        if (!empty($middlename)) {
            $query = $query->where('middlename', $middlename);
        }
    
        return $query->first();
    }

    public function getFullInfo()
    {
        return $this->select([
                'patients.id',
                'patients.firstname',
                'patients.middlename',
                'patients.lastname',
                'patients.gender',
                'patients.age',
                'patients.phone',

                'addresses.address_1',
                'addresses.address_2',

                'barangays.brgyCode',
                'cities.citymunCode',
                'provinces.provCode',

                'barangays.brgyDesc',
                'cities.citymunDesc',
                'provinces.provDesc'
            ])
            ->join('addresses', 'patients.id', '=', 'addresses.patient_id')
            ->join('barangays', 'addresses.brgy_id', '=', 'barangays.brgyCode')
            ->join('cities', 'barangays.citymunCode', '=', 'cities.citymunCode')
            ->join('provinces', 'cities.provCode', 'provinces.provCode');
    }

    /*
    *   Relationships Section
    */

    public function address()
    {
    	return $this->hasOne('App\Models\Address');
    }

    public function prescriptions()
    {
    	return $this->hasMany('App\Models\Prescription');
    }

    /*
    *   Accessors Section
    */

    public function getGenderAttribute()
    {
        return ucfirst(strtolower($this->attributes['gender']));
    }

    /*
    *   Mutators Section
    */

    public function setFirstnameAttribute($value)
    {
        return $this->attributes['firstname'] = ucwords(strtolower($value));
    }

    public function setMiddlenameAttribute($value)
    {
        return $this->attributes['middlename'] = ucwords(strtolower($value));
    }

    public function setLastnameAttribute($value)
    {
        return $this->attributes['lastname'] = ucwords(strtolower($value));
    }

    public function setGenderAttribute($value)
    {
        return $this->attributes['gender'] = ucfirst($value);
    }

}
