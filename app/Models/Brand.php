<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    public $timestamps = false;


    public function setNameAttribute($value)
    {
    	return $this->attributes['name'] = ucwords( strtolower($value) );
    }
}
