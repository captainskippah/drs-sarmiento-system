<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Product extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $appends = ['total_stocks'];

    public $timestamps = false;

    public function category()
    {
    	return $this->belongsTo('App\Models\Category');
    }

    public function brand()
    {
    	return $this->belongsTo('App\Models\Brand', 'brand_id');
    }

    public function stocks()
    {
    	return $this->hasMany('App\Models\Stock', 'product_id', 'id');
    }

    public function getTotalStocksAttribute()
    {
        return $this->stocks->sum('quantity');
    }

    public function transactions()
    {
        return $this->hasManyThrough('App\Models\Transaction', 'App\Models\Stock');
    }

    public function setImageAttribute($value)
    {
        return $this->attributes['image'] = empty($value) ? $this->image : $value;
    }
}
