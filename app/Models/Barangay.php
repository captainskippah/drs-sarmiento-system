<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Barangay extends Model
{
	public $timestamps = false;
	protected $primaryKey = 'brgyCode';

	public function city()
	{
		return $this->belongsTo('App\Models\City', 'citymunCode');
	}

	public function addresses()
	{
		return $this->hasMany('App\Models\Address');
	}

	public function getNameAttribute()
	{
		return ucwords( strtolower($this->attributes['brgyDesc']) );
	}
    
}
