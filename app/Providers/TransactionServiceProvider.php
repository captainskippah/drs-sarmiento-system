<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Services\TransactionService;
use App\Models\Transaction;

class TransactionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('TransactionService', function($app){
            return new TransactionService( new Transaction );
        });
    }
}
