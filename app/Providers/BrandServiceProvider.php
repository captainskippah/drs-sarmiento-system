<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Services\BrandService;

class BrandServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('BrandService', function(){
            return new BrandService();
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
