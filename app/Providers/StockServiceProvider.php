<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Services\StockService;
use App\Models\Stock;

class StockServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('StockService', function(){
            return new StockService( new Stock );
        });
    }
}
