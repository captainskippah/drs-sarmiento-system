/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

 var
	elixir       = require('laravel-elixir'),
	gulp         = require('gulp'),
	jsAssetsPath = './vue-projects';

require('laravel-elixir-webpack-ex');

elixir.extend('buildVueProject', function(mix, projectName, entryPath, configPath) {

	var project          = {}
	project[projectName] = entryPath
	mix.webpack(project, require(configPath), elixir.config.publicPath);

});

elixir(function(mix) {
    if(elixir.config.production) {
        mix.sass('app.scss')
        .buildVueProject(
            mix,
            'pos',
            '/pos/src/main.js',
            jsAssetsPath + '/pos/build/webpack.prod.conf.js'
        );

        // Let's let elixer take care of the hashing
        mix.version([
            'public/css/my-vue-project.css',
            'public/js/pos.js'
        ])
    } else {
        mix.sass('app.scss')
        .browserSync({
            proxy           : "local.clinic.ph/",
            logPrefix       : "POS App",
            logConnections  : false,
            reloadOnRestart : false,
            notify          : false
        });
    }
});